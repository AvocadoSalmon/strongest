//**************************************************************************************************
//																									
//		基本シェーダー		
//
//**************************************************************************************************

//------------------------------------------------------
//		環境関連
//------------------------------------------------------
float4x4 TransMatrix;	//	変換行列
float4x4 matView;		//	変換行列
float4x4 Projection;	//	変換行列

float3	ViewPos;
float   eventGray;	//	追加

//------------------------------------------------------
//		テクスチャサンプラー	
//------------------------------------------------------
texture Texture;	//	デカールテクスチャ
sampler DecaleSamp = sampler_state
{
    Texture = <Texture>;
    MinFilter = LINEAR;
    MagFilter = LINEAR;
    MipFilter = POINT;

    AddressU = Wrap;
    AddressV = Wrap;
};

texture NormalMap;	//	法線マップテクスチャ
sampler NormalSamp = sampler_state
{
    Texture = <NormalMap>;
    MinFilter = LINEAR;
    MagFilter = LINEAR;
    MipFilter = POINT;

    AddressU = Wrap;
    AddressV = Wrap;
};

texture SpecularMap;	//	スペキュラマップテクスチャ
sampler SpecularSamp = sampler_state
{
    Texture = <SpecularMap>;
    MinFilter = LINEAR;
    MagFilter = LINEAR;
    MipFilter = POINT;

    AddressU = Wrap;
    AddressV = Wrap;
};

texture HeightMap;		//	高さマップテクスチャ
sampler HeightSamp = sampler_state
{
    Texture = <HeightMap>;
    MinFilter = LINEAR;
    MagFilter = LINEAR;
    MipFilter = POINT;

    AddressU = Wrap;
    AddressV = Wrap;
};

//**************************************************************************************************
//		頂点フォーマット
//**************************************************************************************************
struct VS_BASIC
{
    float4 Pos    : POSITION;
    float4 Color  : COLOR0;
    float2 Tex	  : TEXCOORD0;
};

struct VS_OUTPUT
{
    float4 Pos		: POSITION;
    float4 Color	: COLOR0;
    float2 Tex		: TEXCOORD0;
    float3 Normal	: TEXCOORD1;

    float4 Ambient	: COLOR1;
	float4 light	: COLOR2;
	float3 vLight	: TEXCOORD2;
	float3 vE		: TEXCOORD3;
	float  specular	: TEXCOORD4;
};

struct VS_INPUTL
{
    float4 Pos    : POSITION;
    float3 Normal : NORMAL;
    float4 Color  : COLOR0;
    float2 Tex	  : TEXCOORD0;
};

struct VS_OUTPUT_FOG
{
    float4 Pos		: POSITION;
    float4 Color	: COLOR0;
    float2 Tex		: TEXCOORD0;

	float  Fog		: TEXCOORD1;
	
    float4 GlobalPos    : TEXCOORD2;	//	グローバル座標
    float3 GlobalNormal : TEXCOORD3;	//	グローバル法線
    
    float4 light	: COLOR1;
	float3 vLight	: TEXCOORD4;
	float3 vE		: TEXCOORD5;
	float  Level	: TEXCOORD6;
};

//**************************************************************************************************
//
//		ライティング
//
//**************************************************************************************************

//**************************************************************************************************
//	半球ライティング
//**************************************************************************************************
//float3 SkyColor = { 0.1f, 0.1f, 0.1f };
//float3 GroundColor = { 0.1f, 0.1f, 0.1f };

float3 SkyColor = { 0.25f, 0.2f, 0.1f };
float3 GroundColor = { 0.25f, 0.2f, 0.1f };

inline float4 HemiLight( float3 normal )
{
	float4 color;
	float rate = (normal.y*0.5f) + 0.5f;
	color.rgb = SkyColor * rate;
	color.rgb += GroundColor * (1-rate);
	color.a = 1.0f;

	return color;
}

//**************************************************************************************************
//	平行光
//**************************************************************************************************
float3 LightDir = { 1.0f, -1.0f, 0.0f };
float3 DirLightColor = { 0.6f, 0.6f, 0.6f };

inline float3 DirLight( float3 dir, float3 normal )
{
	float3 light;
	float rate = max( 0.0f, dot( -dir, normal ) );
	light = DirLightColor * rate;
	
	return light;
}

//**************************************************************************************************
//	スペキュラ
//**************************************************************************************************
inline float Specular(float3 pos, float3 normal)
{
    float   sp;

    float3	H = normalize(ViewPos - pos);
    H = normalize(H - LightDir);
    
	sp = dot(normal, H);
	sp = max( 0, sp );
	sp = pow(sp, 10);

    return sp;
}

//------------------------------------------------------
//		フォグ関連
//------------------------------------------------------
float	FogNear	= -100.0f;
float	FogFar  = 600.0f;
float3	FogColor = { 0.55f, 0.5f, 0.55f };

//**************************************************************************************************
//
//		基本シェーダー
//
//**************************************************************************************************

//------------------------------------------------------
//	頂点シェーダー
//------------------------------------------------------
VS_BASIC VS_Basic( VS_INPUTL In )
{
	VS_BASIC Out = (VS_BASIC)0;

	float3 P = mul(In.Pos, TransMatrix);

	float3x3	mat = TransMatrix;
	float3 N = mul(In.Normal, mat);
	N = normalize(N);

	Out.Pos   = mul(In.Pos, Projection);
	Out.Color.rgb = DirLight( LightDir, N ) + HemiLight( N );
	Out.Color.a = 1.0f;
	Out.Tex   = In.Tex;

	return Out;
}

//------------------------------------------------------
//	頂点カラー付シェーダー
//------------------------------------------------------
VS_BASIC VS_Basic2( VS_INPUTL In )
{
    VS_BASIC Out = (VS_BASIC)0;
	
	float3 P = mul( In.Pos, TransMatrix );

	float3x3	mat = TransMatrix;
	float3 N = mul(In.Normal, mat);
	N = normalize(N);

	Out.Pos   = mul(In.Pos, Projection);
	Out.Color.rgb = (DirLight( LightDir, N ) + HemiLight( N ));
	Out.Color.a = In.Color.a;
	Out.Tex   = In.Tex;

    return Out;
}

//------------------------------------------------------
//	ピクセルシェーダー	
//------------------------------------------------------
float4 PS_Basic( VS_BASIC In) : COLOR
{   
	float4	OUT;
	//	ピクセル色決定
	OUT = In.Color * tex2D( DecaleSamp, In.Tex );

	return OUT;
}

float4 PS_BasicGray( VS_BASIC In) : COLOR
{   
	float4	OUT;
	//	ピクセル色決定
	OUT = In.Color * tex2D( DecaleSamp, In.Tex );
	float ave = (OUT.r + OUT.g + OUT.b)/3.0f;
	OUT.r = ave*eventGray + OUT.r*(1-eventGray);
	OUT.g = ave*eventGray + OUT.g*(1-eventGray);
	OUT.b = ave*eventGray + OUT.b*(1-eventGray);

	return OUT;
}

//------------------------------------------------------
//	テクニック
//------------------------------------------------------
technique copy
{
    pass P0
    {
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		ZWriteEnable     = true;

		VertexShader = compile vs_3_0 VS_Basic();
		PixelShader  = compile ps_3_0 PS_Basic();
    }
}

technique lcopy
{
    pass P0
    {
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		ZWriteEnable     = true;

		VertexShader = compile vs_2_0 VS_Basic2();
		PixelShader  = compile ps_2_0 PS_Basic();
    }
}

technique copyGray
{
    pass P0
    {
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		ZWriteEnable     = true;

		VertexShader = compile vs_3_0 VS_Basic();
		PixelShader  = compile ps_3_0 PS_BasicGray();
    }
}

technique add
{
    pass P0
    {
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = One;
		ZWriteEnable     = false;

		VertexShader = compile vs_3_0 VS_Basic();
		PixelShader  = compile ps_3_0 PS_Basic();
    }
}

//**************************************************************************************************
//
//		フルエフェクト
//
//**************************************************************************************************
//------------------------------------------------------
//	頂点シェーダ
//------------------------------------------------------
VS_OUTPUT VS_FullFX( VS_INPUTL In )
{
    VS_OUTPUT Out = (VS_OUTPUT)0;
	
	float4 P = mul(In.Pos,    TransMatrix);

	float3x3	mat = TransMatrix;
	float3 N = mul(In.Normal, mat);
	N = normalize(N);
	Out.Normal = N;
	Out.Ambient.rgb = HemiLight(N);

	Out.Pos   = mul(In.Pos, Projection);
	Out.Color = 1.0f;
	Out.Tex   = In.Tex;

	//	ライトベクトル補正
	float3	vx;
	float3	vy = { 0.0f, -1, 0.01f };

	vx = cross( N, vy );
	vx = normalize( vx );
	
	vy = cross( vx, N );
	vy = normalize( vy );

	//	ライトベクトル補正
	Out.vLight.x = dot(vx, LightDir);
	Out.vLight.y = dot(vy, LightDir);
	Out.vLight.z = dot(N, LightDir);
	Out.vLight = normalize( Out.vLight );

	// 視線ベクトル補正
	float3 E = (P - ViewPos);   // 視線ベクトル
	Out.vE.x = -dot(vx, E);
	Out.vE.y = dot(vy, E);
	Out.vE.z = dot(N, E);
	Out.vE = normalize( Out.vE );

	Out.light = 0;//PointLight( P, N );

	//	フォグ計算
	Out.Ambient.a = (FogFar-Out.Pos.z) / (FogFar-FogNear);
	Out.Ambient.a = saturate(Out.Ambient.a);

	return Out;
}

//------------------------------------------------------
//	頂点カラー付シェーダ
//------------------------------------------------------
VS_OUTPUT VS_FullFX2( VS_INPUTL In )
{
	VS_OUTPUT Out = (VS_OUTPUT)0;

	float4 P = mul(In.Pos, TransMatrix);

	float3x3	mat = TransMatrix;
	float3 N = mul(In.Normal, mat);
	N = normalize(N);
	Out.Normal = N;

	Out.Ambient.rgb = HemiLight(N);

	Out.Pos   = mul(In.Pos, Projection);
	Out.Color = In.Color;
	Out.Tex   = In.Tex;

	//	ライトベクトル補正
	float3	vx;
	float3	vy = { 0, 1, 0.01f };

	vx = cross( N, vy );
	vx = normalize( vx );

	vy = cross( N, vx );
	vy = normalize( vy );

	//	ライトベクトル補正
	Out.vLight.x = dot(vx, LightDir);
	Out.vLight.y = dot(vy, LightDir);
	Out.vLight.z = dot(N, LightDir);

	// 視線ベクトル補正
	float3 E = (P - ViewPos);   // 視線ベクトル
	Out.vE.x = dot(vx, E);
	Out.vE.y = dot(vy, E);
	Out.vE.z = dot(N, E);

	Out.light = 0;//PointLight( P, N );

	//	フォグ計算
	Out.Ambient.a = (FogFar-Out.Pos.z) / (FogFar-FogNear);
	Out.Ambient.a = saturate(Out.Ambient.a);

	return Out;
}

//------------------------------------------------------
//		ピクセルシェーダー	
//------------------------------------------------------
float4 PS_FullFX( VS_OUTPUT In) : COLOR
{   
	float4	OUT;
	float2 Tex = In.Tex;

	//	パララックスディスプレースメント
	float h = tex2D( HeightSamp, Tex ).r-0.5f;// 高さマップのサンプリング
	float3 E = normalize(In.vE);
	In.vLight = normalize(In.vLight);
	
	Tex -= 0.05f * h * E.xy;
	float3 N = tex2D( NormalSamp, Tex ).xyz * 2.0f - 1.0f;

	//	ライト計算
	float3 light = DirLight(In.vLight, N);
	//	ピクセル色決定
	OUT = In.Color * tex2D( DecaleSamp, Tex );
	OUT.rgb = (OUT.rgb*(light+In.Ambient.rgb));


	//	スペキュラ
	float3  R = normalize( reflect( E, N ) );
	OUT.rgb += pow( max( 0, dot(R, -In.vLight) ), 10 ) * tex2D( SpecularSamp, Tex );
	//	フォグ採用
	OUT.rgb = (OUT.rgb * In.Ambient.a) + (FogColor * (1-In.Ambient.a));

	return OUT;
}

//------------------------------------------------------
//		ピクセルシェーダー	
//------------------------------------------------------
float4 PS_FullFX2( VS_OUTPUT In) : COLOR
{   
	float4	OUT;
	float2 Tex = In.Tex;

	//	パララックスディスプレースメント
	float h = tex2D( HeightSamp, Tex ).r-0.5f;// 高さマップのサンプリング
	float3 E = normalize(In.vE);
	In.vLight = normalize(In.vLight);
	
	float3 N = tex2D( NormalSamp, Tex ).xyz * 2.0f - 1.0f;

	//	ライト計算
	float3 light = DirLight(In.vLight, N);
	//	ピクセル色決定
	OUT = In.Color * tex2D( DecaleSamp, Tex );
	OUT.rgb = (OUT.rgb*(light+In.Ambient.rgb));


	//	スペキュラ
	float3  R = normalize( reflect( E, N ) );
	OUT.rgb += pow( max( 0, dot(R, -In.vLight) ), 10 ) * tex2D( SpecularSamp, Tex );
	//	フォグ採用
	//OUT.rgb = (OUT.rgb * In.Ambient.a) + (FogColor * (1-In.Ambient.a));

	return OUT;
}

//------------------------------------------------------
//		合成なし	
//------------------------------------------------------
technique copy_fx
{
    pass P0
    {
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		ZWriteEnable     = true;

		VertexShader = compile vs_3_0 VS_FullFX();
		PixelShader  = compile ps_3_0 PS_FullFX();
    }
}

technique lcopy_fx
{
    pass P0
    {
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		ZWriteEnable     = true;
		VertexShader = compile vs_3_0 VS_FullFX2();
		PixelShader  = compile ps_3_0 PS_FullFX();
    }
}

technique copy_fx2
{
    pass P0
    {
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		ZWriteEnable     = true;

		VertexShader = compile vs_3_0 VS_FullFX();
		PixelShader  = compile ps_3_0 PS_FullFX2();
    }
}

technique lcopy_fx2
{
    pass P0
    {
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		ZWriteEnable     = true;
		VertexShader = compile vs_3_0 VS_FullFX2();
		PixelShader  = compile ps_3_0 PS_FullFX2();
    }
}

//**************************************************************************************************
//
//		環境マップ
//
//**************************************************************************************************

float EnvParam = 0.5f;	//	合成の割合

//**************************************************************************************************
//	環境マップ用
//**************************************************************************************************
texture EnvMap;		//	環境テクスチャ

sampler EnvSamp = sampler_state
{
    Texture = <EnvMap>;
    MinFilter = LINEAR;
    MagFilter = LINEAR;
    MipFilter = NONE;

    AddressU = Wrap;
    AddressV = Wrap;
};

texture LightMap;	//	ライトマップテクスチャ
sampler LightSamp = sampler_state
{
    Texture = <LightMap>;
    MinFilter = LINEAR;
    MagFilter = LINEAR;
    MipFilter = NONE;

    AddressU = Clamp;
    AddressV = Clamp;
};

inline float4 Environment( float3 normal )
{
	float4	color;
	
	float2	uv;
	uv.x = normal.x*0.5f + 0.5f;
	uv.y = -normal.y*0.5f + 0.5f;

	color = tex2D( EnvSamp, uv );
	return color;
}

//------------------------------------------------------
//		頂点シェーダー	
//------------------------------------------------------
 VS_OUTPUT_FOG VS_Lighting2( VS_INPUTL In )
{

   VS_OUTPUT_FOG Out = (VS_OUTPUT_FOG)0;

    Out.Pos = mul(In.Pos, Projection);
	Out.Tex = In.Tex;
	Out.Color.a = 1;
	
	//	法線変換	
	float3x3 mat = TransMatrix;
	float3 N = mul( In.Normal, mat );
	N = normalize(N);
	
	//	半球ライティング適用
	Out.Color.rgb = HemiLight(N);
	//	平行光適用
	Out.Color.rgb += DirLight( LightDir,N );
	
	//	グローバルポジション&法線
	Out.GlobalPos = mul(In.Pos, TransMatrix );
	float3x3 m = (float3x3)TransMatrix;
	Out.GlobalNormal = mul(In.Normal, m );	
	
	//	ライトマップ
	float3 v = normalize( LightDir );
	Out.Level = dot(v,-N) * 0.5f + 0.5f;

	//	フォグ
	Out.Fog = ( FogFar - Out.Pos.z ) / ( FogFar-FogNear );
	Out.Fog = saturate( Out.Fog );


	
    return Out;
}

//------------------------------------------------------
//		環境マップ用ピクセルシェーダー	
//------------------------------------------------------
float4 PS_Env( VS_OUTPUT_FOG In) : COLOR
{   
	float4	OUT;
	float2 Tex = In.Tex;
	
	//	パララックスディスプレースメント
	float h = tex2D( HeightSamp, Tex ).r-0.5f;// 高さマップのサンプリング
	float3 E = normalize(In.vE);
	In.vLight = normalize(In.vLight);
	
	float3 N = tex2D( NormalSamp, Tex ).xyz * 2.0f - 1.0f;

	//	ライト計算
	float3 light = DirLight(In.vLight, N);
	//	ピクセル色決定
	OUT = In.Color * tex2D( DecaleSamp, Tex );
	
	In.GlobalNormal = normalize(In.GlobalNormal);

	//	スペキュラ
	float3  R = normalize( reflect( In.vE, N ) );
	OUT.rgb +=  pow( max( 0, dot(R, -In.vLight) ), 50 ) * tex2D( SpecularSamp, Tex ) + Specular( N, In.GlobalNormal );
	
	//	環境マップ
	float4 Env = Environment( In.GlobalNormal );
	OUT = OUT * ( 1 - EnvParam ) + Env * EnvParam;
	
	//	フォグ採用
	//OUT.rgb = (OUT.rgb * In.Fog ) + (FogColor * (1-In.Fog));
	
	return OUT;
}

//------------------------------------------------------
//		環境マッピング
//------------------------------------------------------
technique environment
{
    pass P0
    {
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		CullMode         = CCW;
		ZEnable          = true;
		ZWriteEnable     = true;

		VertexShader = compile vs_3_0 VS_Lighting2();
		PixelShader  = compile ps_3_0 PS_Env();
    }
}

//********************************************************************
//
//		トゥーンシェーディング		
//
//********************************************************************

//------------------------------------------------------
//		ライトマップ
//------------------------------------------------------
//texture LightMap;
/*
sampler LightSamp = sampler_state
{
    Texture = <LightMap>;
    MinFilter = LINEAR;
    MagFilter = LINEAR;
    MipFilter = NONE;

    AddressU = Clamp;
    AddressV = Clamp;
};
*/

struct VS_OUTPUT_TOON
{
    float4 Pos		: POSITION;
    float4 Color	: COLOR0;
    float2 Tex		: TEXCOORD0;
    
    float Level		: TEXCOORD1;
    
};

float OutlineSize = 0.2f;

//------------------------------------------------------
//		アウトライン頂点シェーダー	
//------------------------------------------------------
VS_OUTPUT VS_Outline( VS_INPUTL In )
{
	VS_OUTPUT Out = (VS_OUTPUT)0;
	
	//	法線方向に拡大
	In.Normal = normalize( In.Normal );
	In.Pos.xyz += In.Normal *  OutlineSize;
	
	Out.Pos = mul( In.Pos,Projection );
	Out.Tex = In.Tex;
	
	float3 color = { 0.0f,0.0f,0.0f };
	Out.Color.rgb = color;
	Out.Color.a = 1;
	
	return Out;
};

//------------------------------------------------------
//		アウトラインピクセルシェーダー	
//------------------------------------------------------
float4 PS_Outline( VS_OUTPUT In ) : COLOR
{
	float4 OUT;
	OUT = In.Color;
	return OUT;
}

//------------------------------------------------------
//		トゥーン頂点シェーダー	
//------------------------------------------------------
float ToonLevel = 0.5f;
float ToonShadow = 0.4f;

VS_OUTPUT_TOON VS_Toon( VS_INPUTL In )
{
	VS_OUTPUT_TOON Out = (VS_OUTPUT_TOON)0;
	
	Out.Pos = mul( In.Pos,Projection );
	Out.Tex = In.Tex;
	Out.Color = 1;
	
	float3x3 mat = TransMatrix;
	float3 N = mul( In.Normal,mat );
	N = normalize(N);
	
	float3 v = normalize( LightDir );
	Out.Level = dot(v,-N) * 0.5f + 0.5f;
	
	return Out;
};

//------------------------------------------------------
//		薄膜頂点シェーダー	
//------------------------------------------------------
VS_OUTPUT_TOON VS_Film( VS_INPUTL In )
{
	VS_OUTPUT_TOON Out = (VS_OUTPUT_TOON)0;
	
	Out.Pos = mul( In.Pos,Projection );
	Out.Tex = In.Tex;
	Out.Color = In.Color;
	
	float3x3 mat = TransMatrix;
	float3 N = mul( In.Normal,mat );
	N = normalize(N);
	
	float3 vE = normalize( ViewPos - In.Pos );
	float value = max( 0, dot( vE,N ) );
	
	Out.Level = 1-value;
	
	return Out;
};

//------------------------------------------------------
//		トゥーンピクセルシェーダー	
//------------------------------------------------------
float4 PS_Toon( VS_OUTPUT_TOON In ) : COLOR
{
	float4 OUT;
	OUT = tex2D( DecaleSamp,In.Tex );
	
	if( In.Level < ToonLevel )OUT.rgb *= ToonShadow;
	
	return OUT;
}

//------------------------------------------------------
//		ライトマップピクセルシェーダー	
//------------------------------------------------------
float4 PS_LightMap( VS_OUTPUT_TOON In ) : COLOR
{
	float4 OUT;
	OUT = tex2D( DecaleSamp,In.Tex );
	
	float2 UV = { In.Level ,0 };
	float3 level = tex2D( LightSamp,UV );
	OUT.rgb *= level;
	OUT.rgb += pow( level,4 );
	
	return OUT;
}

//------------------------------------------------------
//		薄膜ピクセルシェーダー	
//------------------------------------------------------
float4 PS_FilmMap( VS_OUTPUT_TOON In ) : COLOR
{
	float4 OUT;
	
	float2 UV = { In.Level ,0 };
	OUT = tex2D( LightSamp,UV );
	
	return OUT;
}

//------------------------------------------------------
//		トゥーン用テクニック
//------------------------------------------------------
technique ToonLine
{
    pass P0
    {
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		CullMode         = CW;
		ZEnable          = true;

		VertexShader = compile vs_3_0 VS_Outline();
		PixelShader  = compile ps_3_0 PS_Outline();
    }
}

technique toon
{
    pass P0
    {
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		CullMode         = CW;
		ZEnable          = true;

		VertexShader = compile vs_3_0 VS_Outline();
		PixelShader  = compile ps_3_0 PS_Outline();
    }
    
    pass Toon
    {
		CullMode		 = CCW;
		ZEnable          = true;
		
		VertexShader = compile vs_3_0 VS_Toon();
		PixelShader  = compile ps_3_0 PS_Toon();
    }
}

//------------------------------------------------------
//		ライトマップ用テクニック
//------------------------------------------------------
technique lightmap
{ 
    pass P0
    {
		CullMode		 = CCW;
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		ZEnable          = true;
		ZWriteEnable     = true;
		
		VertexShader = compile vs_3_0 VS_Toon();
		PixelShader  = compile ps_3_0 PS_LightMap();
    }
}



//--------------追加----------------------------------------------------

float	texPos_u, texPos_v;	//	UVエフェクト用
float	effectAlpha;	//	エフェクトのアルファ

VS_BASIC VS_UVEffect( VS_INPUTL In )
{
	VS_BASIC Out = (VS_BASIC)0;

	float3 P = mul(In.Pos, TransMatrix);

	float3x3	mat = TransMatrix;
	float3 N = mul(In.Normal, mat);
	N = normalize(N);

	Out.Pos   = mul(In.Pos, Projection);
	Out.Color.rgb = float3(1,1,1);
	Out.Color.a = In.Color.a;
	Out.Tex   = In.Tex;

	return Out;
}

float4 PS_UVEffect( VS_BASIC In) : COLOR
{   
	float4	OUT;
	//	ピクセル色決定
	OUT = In.Color * tex2D( DecaleSamp, float2(In.Tex.x + texPos_u, In.Tex.y + texPos_v) );
	OUT.a *= effectAlpha;

	return OUT;
}

technique uvEffect
{
    pass P0
    {
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		ZWriteEnable     = true;

		VertexShader = compile vs_2_0 VS_UVEffect();
		PixelShader  = compile ps_2_0 PS_UVEffect();
    }
}
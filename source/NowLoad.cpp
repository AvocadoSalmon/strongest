#include	"iextreme.h"
#include	"system/system.h"

#include	"nowLoad.h"
#include	"system/Framework.h"

#include	"Camera.h"



//	コンストラクタ
cNowLoad::cNowLoad() : cThreadObject(){

	_obj = new iex2DObj( "DATA\\Env.png" );

	tp = new iexMesh( "DATA\\TP.x" );
	tpAngle = 0.0f;

	m_Pos = Vector3(0,0,0);

	//	パーティクル読み込み
	iexParticle::Initialize("DATA\\particle.png",1024);

	//	カメラモード設定
	CAMERA.SetMode( CMODE_NORMAL );

	//	フェードイン・アウト
	fade = new c_Fade();
	fade->SetColor( 0x000000 );		//	黒色に設定
	fade->SetMode( MODE_FADE_IN );	//	フェードイン
}

//	デストラクタ
cNowLoad::~cNowLoad(){
	//if( _obj != NULL ){
	//	delete _obj;
	//	_obj = NULL;
	//}
	DEL( _obj );
	DEL( fade );
	DEL( tp );
	iexParticle::Release();

	CAMERA.SetViewPort( 0, 0, 0, 0 );	//	手動黒画面化

}

//	実行
void cNowLoad::Exec(){

	if(!CheckFrameRate())return;

	fade->Update();

	tp->SetScale( 0.5f );
	tp->SetPos( Vector3(0,0,0) );
	tp->SetAngle( tpAngle );
	tp->Update();
	tpAngle += 0.07f;

	CAMERA.SetPos( Vector3( 0,0,-10 ) );
	CAMERA.SetTarget( Vector3( 0,0,0 ) );
	CAMERA.Update();

	RiseUpParticle( Vector3(0,-2,5) );

	iexParticle::Update();
}

//	描画
void cNowLoad::Render(){
	
	CAMERA.Begin();

	if( _obj != NULL ){
		//_obj->Render( 412,648,456,48, 0,0,456,48 );
		//_obj->Render( 0,0,1280,720,0,0,128,128,shader2D,"copy",0xFFFFFFFF,0.0f );
	}

	tp->Render( shader, "copy_fx2" );

	iexParticle::Render( shader2D, "Particle" );

	fade->Render();
}

//	光の軌跡エフェクト
void cNowLoad::RiseUpParticle( Vector3 &pos )
{
	PARTICLE	p;

	static float EffRad = 0.0f;	//	初期角度
	const float R = 3.0f;		//	半径
	static float HeightAdjust = 0.0f;

	EffRad -= EULER * 15;

	//HeightAdjust += 0.03f;

	if(HeightAdjust > 3.0f)HeightAdjust = 0.0f;

	for( int i=0 ; i<5 ; i++ )
	{
		//	移動量
		Vector3 move;
		move.x =   0.001f * R * cosf(EffRad);
		move.y =   0.005f;
		move.z =   0.001f * R * sinf(EffRad);

		//	出現位置
		p.Pos.x = pos.x + R * cosf(EffRad);
		p.Pos.y = pos.y + HeightAdjust;
		p.Pos.z = pos.z + R * sinf(EffRad);

		
		p.type = 1;	//	パーティクルの種類
		//	開始フレーム
		p.aFrame = 0;
		p.aColor = 0xFF0000FF;
		//	中間フレーム
		p.mFrame = 90;
		p.mColor = 0x8800FFFF;
		//	終了フレーム
		p.eFrame = 240;
		p.eColor = 0x0000FF00;

		p.angle = .0f;
		p.rotate = (rand()%200-100) * 0.01f;
		p.scale = 0.6f;
		p.stretch = 0.94f;	//	1.05初期値

		p.flag = RS_ADD;

		p.Move = move;	//	移動量
		
		//	パワー
		p.Power.x = 0.0f;
		p.Power.z = 0.0f;
		p.Power.y = 0.002f;

		iexParticle::Set(&p);
	}
	
}

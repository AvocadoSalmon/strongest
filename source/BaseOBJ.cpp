#include	"iextreme.h"
#include	"system/system.h"

#include	"BaseOBJ.h"
#include	"Player.h"
#include	"Collision.h"
#include	"TEST\\TESTCollision.h"

//**************************************************************************************//
//																						//
//																						//
//																						//
//**************************************************************************************//



//**************************************************************************************//
//																						//
//		コンストラクタ・デストラクタ													//
//																						//
//**************************************************************************************//
BaseOBJ::BaseOBJ()
{
	m_state		= 0;
	m_step		= 0;
	m_Pos		= Vector3( 0,0,0 );
	m_angle		= 0.0f;
	m_Move		= Vector3( 0,0,0 );
	m_gravity	= 0.0f;
	m_scale		= 0.0f;
	m_HP		= 0;

	m_AttackID	= -1;
	m_DamageID	= -1;

	m_lpObj		= NULL;
}

BaseOBJ::BaseOBJ(char *name )
{
	m_state		= 0;
	m_step		= 0;
	m_Pos		= Vector3( 0,0,0 );
	m_angle		= 0.0f;
	m_Move		= Vector3( 0,0,0 );
	m_gravity	= 0.0f;
	m_HP		= 10;

	m_lpObj		= new iex3DObj( name );

}

BaseOBJ::~BaseOBJ()
{
	DEL( m_lpObj );
}

//**************************************************************************************//
//																						//
//		更新																			//
//																						//
//**************************************************************************************//
void BaseOBJ::Update()
{
	//	座標更新
	m_Pos	= collision->CheckMove( m_Pos,m_Move );

	GroundCheck();			//	地面チェック

	//	情報更新
	m_lpObj->SetScale( m_scale );
	m_lpObj->SetPos( m_Pos );
	m_lpObj->SetAngle( m_angle );
	m_lpObj->Animation();	//	モーション更新
	m_lpObj->Update();
	

}

//**************************************************************************************//
//																						//
//		描画																			//
//																						//
//**************************************************************************************//
void BaseOBJ::Render()
{
	m_lpObj->Render( shader,"copy_fx2" );
}

void BaseOBJ::Render( char* techname )
{
	m_lpObj->Render( shader, techname );
}

//**************************************************************************************//
//																						//
//																						//
//																						//
//**************************************************************************************//

//--------------------------------------------------------------------------------
//	引数: n:モーション番号、i:補間フレーム
//	機能: n番目のモーションへiフレーム補間する
//--------------------------------------------------------------------------------
void BaseOBJ::SetMotion( int n )
{
	if( m_lpObj->GetMotion() != n )	//	前回と違うモーション時
	{
		m_lpObj->SetMotion( n );				//	モーションセット
	}
}

//--------------------------------------------------------------------------------
//	引数: なし
//	機能: Y軸移動
//--------------------------------------------------------------------------------
void BaseOBJ::YMove()
{
	//	Ｙ軸移動
	m_gravity -= GRAVITY;	//	重力値
	if( m_gravity <= GRAVITY_MAX )m_gravity = GRAVITY_MAX;
	m_Pos.y += m_gravity;	//	Y座標更新
}

//--------------------------------------------------------------------------------
//	引数: なし
//	機能: 地面ならtrue,空中ならfalseを返す
//--------------------------------------------------------------------------------
bool BaseOBJ::GroundCheck()
{
	//	地面チェック
	float height = collision->FloorHeight( m_Pos );

	if( m_Pos.y <= height )
	{
		m_gravity = 0.0f;
		m_Move.y = 0.0;
		m_Pos.y = height;
		return true;
	}

	return false;
}

//--------------------------------------------------------------------------------
//	引数: 行動モード
//	機能: 行動モードを切り替える
//--------------------------------------------------------------------------------
void BaseOBJ::ChangeState( int state )
{
	if( m_state != state )
	{
		
		if( TESTCollision )TESTCollision->DestroyHitObj( &m_AttackID );
		m_step = 0;
		m_state = state;
	}
}

//--------------------------------------------------------------------------------
//	引数: ボーン番号
//	機能: ボーンの行列を返す
//--------------------------------------------------------------------------------
Matrix BaseOBJ::GetBoneMatrix( int n )
{
	return *m_lpObj->GetBone(n);
}


//--------------------------------------------------------------------------------
//	引数: 武器メッシュ,装備するボーン番号
//	機能: 武器をセットする
//--------------------------------------------------------------------------------
void BaseOBJ::WeaponSet(iex3DObj* lpWeapon,int BoneNum)
{
	//	変換行列 〜TransMatrix〜
	D3DXMATRIX	mat	= *m_lpObj->GetBone( BoneNum ) * m_lpObj->TransMatrix;

	CopyMemory(&lpWeapon->TransMatrix , &mat, sizeof(mat) );
	D3DXMatrixIdentity(&mat);
	D3DXMatrixScaling( &mat,WEAPON_SIZE_BASIC,WEAPON_SIZE_BASIC,WEAPON_SIZE_BASIC );

	D3DXMatrixMultiply(&lpWeapon->TransMatrix,&mat,&lpWeapon->TransMatrix);
}
//--------------------------------------------------------------------------------
//	引数: 武器メッシュ,装備するボーン番号
//	機能: 武器をセットする
//--------------------------------------------------------------------------------
void BaseOBJ::WeaponSet(iexMesh* lpWeapon,int BoneNum)
{
	//	変換行列 〜TransMatrix〜
	D3DXMATRIX	mat;
	mat	= *m_lpObj->GetBone( BoneNum ) * m_lpObj->TransMatrix;

	CopyMemory(&lpWeapon->TransMatrix , &mat, sizeof(mat) );
	D3DXMatrixIdentity(&mat);
	D3DXMatrixScaling( &mat,WEAPON_SIZE_BASIC,WEAPON_SIZE_BASIC,WEAPON_SIZE_BASIC );

	D3DXMatrixMultiply(&lpWeapon->TransMatrix,&mat,&lpWeapon->TransMatrix);
}

//--------------------------------------------------------------------------------
//	引数: ボーン番号、(ボーンの長さ)
//	機能: 武器の先の座標を取得する
//--------------------------------------------------------------------------------
Vector3 BaseOBJ::GetBonePos(int n, int d)
{
	Matrix mat =  *m_lpObj->GetBone(n) * m_lpObj->TransMatrix;

	D3DXMatrixIdentity(&mat);
	D3DXMatrixScaling( &mat,WEAPON_SIZE_BASIC,WEAPON_SIZE_BASIC,WEAPON_SIZE_BASIC );

	Vector3 p;
	p.x = mat._41 + mat._31 * d;
	p.y = mat._42 + mat._32 * d;
	p.z = mat._43 + mat._33 * d;

	return p;
}
//--------------------------------------------------------------------------------
//	引数: ボーン番号
//	機能: ボーン座標を取得する
//--------------------------------------------------------------------------------
Vector3 BaseOBJ::GetBonePos(int n)
{
	Matrix mat =  *m_lpObj->GetBone(n) * m_lpObj->TransMatrix;

	D3DXMatrixIdentity(&mat);
	D3DXMatrixScaling( &mat,WEAPON_SIZE_BASIC,WEAPON_SIZE_BASIC,WEAPON_SIZE_BASIC );

	Vector3 p;
	p.x = mat._41;
	p.y = mat._42;
	p.z = mat._43;

	return p;
}
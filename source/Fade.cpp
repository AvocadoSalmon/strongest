#include	"iextreme.h"
#include	"system/system.h"

#include	"Fade.h"


//**************************************************************************************//
//																						//
//																						//
//																						//
//**************************************************************************************//

//**************************************************************************************//
//																						//
//		初期化																			//
//																						//
//**************************************************************************************//
c_Fade::c_Fade()
{
	m_mode = MODE_FADE_RESET;
	m_current = 0;
	m_step = 0;
	m_bDone = false;
	m_color = 0;
}

c_Fade::~c_Fade()
{
	m_mode = MODE_FADE_RESET;
	m_current = 0;
	m_step = 0;
	m_bDone = false;
	m_color = 0;
}

//**************************************************************************************//
//																						//
//		更新																			//
//																						//
//**************************************************************************************//
void c_Fade::Update()
{
	switch( m_mode ){
		case MODE_FADE_IN:		//	フェードイン
			FadeIn();
			break;
		case MODE_FADE_OUT:		//	フェードアウト
			FadeOut();
			break;
		default:
			m_bDone = true;
			break;
	}
}

//**************************************************************************************//
//																						//
//		描画																			//
//																						//
//**************************************************************************************//
void c_Fade::Render()
{
	DWORD a =  m_current <<  FADE_ALPHA_SHIFT;
	
	
	IEX_DrawRect( 0,0,SCREEN_WIDTH,SCREEN_HEIGHT,RS_COPY,a|m_color );
}

//**************************************************************************************//
//																						//
//		処理																			//
//																						//
//**************************************************************************************//

//--------------------------------------------------------------------------------
//	引数: なし
//	機能: 画面をフェードインさせ、終了時にtrueを返す
//--------------------------------------------------------------------------------
void c_Fade::FadeIn()
{
	switch( m_step ){
		case 0:	//	初期化
			m_bDone = false;
			m_current = FADE_MAX_COLOR;
			m_step++;
			break;
		case 1:	//	アルファ減衰

			if( m_current > 0 )
			{
				m_current -= FADE_VALUE;
			}
			else
			{
				m_current = 0;
				m_step++;
			}
			break;
		case 2:
			SetMode( MODE_FADE_RESET );
			
			break;		
	}
}

//--------------------------------------------------------------------------------
//	引数: なし
//	機能: 画面をフェードアウトさせ、終了時にtrueを返す
//--------------------------------------------------------------------------------
void c_Fade::FadeOut()
{
	switch( m_step ){
		case 0:	//	初期化
			m_bDone = false;
			m_current = 0;
			m_step++;
			break;
		case 1:	//	アルファ増加
			if( m_current < FADE_MAX_COLOR )
			{
				m_current += FADE_VALUE;
			}
			else
			{
				m_current = FADE_MAX_COLOR;
				m_step++;
			}
			break;
		case 2:
			SetMode( MODE_FADE_RESET );
			
			break;		
	}
}
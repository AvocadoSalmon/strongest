#ifndef __EFFECTMANAGER_H__
#define __EFFECTMANAGER_H__

#include	"iextreme.h"
#include	"UVEffect.h"
#include	"List.h"
#include	"IDMaker.h"


typedef	enum	tagEFFECTSETKIND{

	EFFECTSETKIND_SAMPLE = 0,
	EFFECTSETKIND_SEIKEN = 1,
	EFFECTSETKIND_PORTAL = 2,
	EFFECTSETKIND_DOTOU  = 3,
	EFFECTSETKIND_GODOTOU = 4,
	EFFECTSETKIND_AURA = 5,


}EFFECTSETKIND;





//-------------------------------------------//
//				エフェクトのリスト			 //
//-------------------------------------------//

class	EffectList{

private:
	EffectList*	next;
	EffectList*	back;

public:
	UVEffect*	m_uvEffect;

	EffectList();
	~EffectList();

	EffectList*	Create(int meshKind,const Vector3& pos, float addU, float addV, float addAlpha, float angleY, float scale, int time, float stretch);


	//	ゲッター
	EffectList	*GetBack(){return back;}
	EffectList	*GetNext(){return next;}
	
	//	セッター
	void	SetBack(EffectList* in_back){back = in_back;}
	void	SetNext(EffectList* in_next){next = in_next;}

};

//-------------------------------------------//
//		エフェクトセットの初期化情報         //
//-------------------------------------------//

class	KindAndTime{

private:

	

public:
	int	m_kind;
	int m_time;
	Vector3	m_pos;
	float m_scale;
	float m_angleY;
	int	  m_ID;

	KindAndTime(){
		m_kind = 0;
		m_time = 120;
		m_pos.x = 0;
		m_pos.y = 0;
		m_pos.z = 0;
		m_scale = 1.0f;
		m_angleY = 1.0f;
		m_ID	= IDMaker::GetInstance()->CreateID();
		
	}
	KindAndTime(EFFECTSETKIND kind, int time, Vector3& pos, float scale, float angleY);
	~KindAndTime();

	void	TimeDecre(){m_time--;}

	void	SetID(int id){m_ID=id;}

};


//-------------------------------------------//
//				   エフェクト管理            //
//-------------------------------------------//

class	EffectManager{

private:
	EffectList*		m_effectList;
	List<KindAndTime>*		m_kindAndTime;
	LPIEXMESH	m_mesh[10];
	
public:
	EffectManager();
	~EffectManager();

	void	SetEffect(EFFECTSETKIND num, Vector3 &pos, float angleY, float scale);
	void	PlayEffect();

	void	DeleteDotou();
	void	DeleteGoDotou();
	void	DeleteEffect(int id);

	void	SetPos(int id,Vector3& pos);

	void	SampleEffect(int timer, const Vector3& pos, float angleY, float scale);
	void	SeikenEffect(int timer, const Vector3& pos, float angleY, float scale);
	void	PortalEffect(int timer, const Vector3& pos, float angleY, float scale);
	void	DotouEffect(int timer, const Vector3& pos, float angleY, float scale);
	void	GoDotouEffect(int timer, const Vector3& pos, float angleY, float scale);
	void	AuraEffect(int timer, const Vector3& pos, float angleY, float scale);
	
	
	void	Update();

	void	Draw(iexShader* shader);


	void	GetEffectPos(int num, Vector3* outPos);

};



#endif
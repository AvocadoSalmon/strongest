#ifndef __HITOBJ_H__
#define __HITOBJ_H__

#include	"iextreme.h"
#include	"IDMaker.h"


//	当たり判定の種類
typedef enum	tagCOLLISIONKIND{

	DAMAGECOLLISION = -1,	//	やられ判定
	ATKKIND_PANCH   =  1,	//	これ以降攻撃判定
	ATKKIND_BARRIER =  2,	//	障害物の衝突
	ATKKIND_SPECIAL =  3,	//	波動拳

}COLLISION;

//	誰が判定を作ったか
typedef enum	tagMASTERKIND{

	MASTERKIND_PLAYER = 0,
	MASTERKIND_ENEMY,

}MASTERKIND;

//	あたり判定のプリミティブの種類
typedef	enum	tagPRIMITIVE{

	PRIMITIVE_BALL = 0x00000001,
	PRIMITIVE_BOX  = 0x00000010,

}PRIMITIVE;

//	どのプリミティブとどのプリミティブで当たり判定をするか
typedef enum	tagPRIMITIVE_BOX_COLLISIONKIND{

	COLLISIONKIND_BALL			= 0x00000001,
	COLLISIONKIND_BOX			= 0x00000010,
	COLLISIONKIND_BALLandBOX	= 0x00000011,

}PRIMITIVE_BOX_COLLISIONKIND;

typedef	enum	tagCOLLISION_DESTROY_KIND{

	COLLISION_DESTROY_KIND_OPTION = 0,
	COLLISION_DESTROY_KIND_TIMER,

}COLLISION_DESTROY_KIND;


//-------------------------------------------//
//	 　　		OBB判定情報クラス			 //
//-------------------------------------------//

class	CubeInfo{

public:
	D3DXVECTOR3	m_dir[3];
	float		m_length[3];

	CubeInfo();

};

//-------------------------------------------//
//	 　　   当り判定した結果情報クラス	     //
//-------------------------------------------//

class	AtkHitResult{

protected:
	int			m_damageCollisionMasterKind;
	int			m_damageCollisionID;
	//int			m_damageCollisionMasterState;
public:
	AtkHitResult();

	int	GetDamageCollisionMasterKind(){return m_damageCollisionMasterKind;}
	int	GetDamageCollisionID(){return m_damageCollisionID;}
	//int	GetDamageCollisionMasterState(){return m_damageCollisionMasterState;}
};

class	DamageHitResult{

protected:
	//int			m_atkCollisionMasterKind;
	int			m_atkCollisionAtkKind;
	int			m_atkCollisionID;
	int			m_damage;
public:
	DamageHitResult();
	DamageHitResult(int atkKind, int damage);

	//int GetAtkCollisionMasterKind(){return m_atkCollisionMasterKind;}
	int GetAtkCollisionID(){return m_atkCollisionID;}
	int GetAtkCollisionDamage(){return m_damage;}
	int	GetAtkCollisionAtkKind(){return m_atkCollisionAtkKind;}
};

//-------------------------------------------//
//	 　　		当り判定基底クラス			 //
//-------------------------------------------//

class	HitObj{

protected:
	D3DXVECTOR3	m_pos;
	int			m_masterKind;
	int			m_ID;
	int			m_groupID;
	int			m_atcKind;
	int			m_damage;
	bool		m_bDestroy;
	int			m_life;
	PRIMITIVE	m_myPrimitive;
	int			m_collisionDestroyKind;
	//bool		m_hited;

	HitObj(){}
	HitObj(int masterKind, int life=10, int atcKind=DAMAGECOLLISION);
public:

	virtual ~HitObj();

	void	Update();

	virtual void	GetTransMatrix(D3DXMATRIX* out)=0;
	

	D3DXVECTOR3	GetPos(){return m_pos;}
	int	GetMansterKind(){return m_masterKind;}
	int GetID(){return m_ID;}
	int	GetGroupID(){return m_groupID;}
	int GetAtcKind(){return m_atcKind;}
	int	GetDamage(){return m_damage;}
	bool	GetbDestroy(){return m_bDestroy;}
	int	GetLife(){return m_life;}
	PRIMITIVE	GetPrimitive(){return m_myPrimitive;}
	
	
	void	SetPos(D3DXVECTOR3& in_pos){m_pos = in_pos;}
	//void	SetDamage(int value){m_damage = value;}
	void	SetbDestroy(bool flag){m_bDestroy = flag;}
	void	SetGroupID(int ID){m_groupID = ID;}
	
	
};

//-------------------------------------------//
//	 　　	BOX型当り判定クラス			 //
//-------------------------------------------//

class	HitObjOBB	:	public	HitObj{

private:
	CubeInfo	m_cubeInfo;

	HitObjOBB(){}

public:
	HitObjOBB(int masterKind, int atcKind=DAMAGECOLLISION, int life=10);
	~HitObjOBB(){}

	void	GetTransMatrix(D3DXMATRIX* out);
	CubeInfo	GetCubeInfo(){return m_cubeInfo;}

	void	SetCubeInfo(const CubeInfo& in_cubeInfo){m_cubeInfo = in_cubeInfo;}
	void	SetCuveInfoDir(const D3DXMATRIX& mat){
		D3DXVECTOR3 vec(mat._11,mat._12,mat._13);
		D3DXVec3Normalize(&vec,&vec);
		m_cubeInfo.m_dir[0] = vec;

		vec = D3DXVECTOR3(mat._21,mat._22,mat._23);
		D3DXVec3Normalize(&vec,&vec);
		m_cubeInfo.m_dir[1] = vec;

		vec = D3DXVECTOR3(mat._31,mat._32,mat._33);
		D3DXVec3Normalize(&vec,&vec);
		m_cubeInfo.m_dir[2] = vec;

	}
	void	SetCuveInfoLength(const D3DXVECTOR3& len){
		m_cubeInfo.m_length[0] = fabs(len.x);
		m_cubeInfo.m_length[1] = fabs(len.y);
		m_cubeInfo.m_length[2] = fabs(len.z);
	}


	static	void	CreateCubeInfo(const D3DXMATRIX& mat, const D3DXVECTOR3& length, CubeInfo& out);
	
};

//-------------------------------------------//
//	 　　	球状当り判定クラス			 //
//-------------------------------------------//

class	HitObjBall	:	public	HitObj{

private:
	float	m_radius;

	HitObjBall(){}
public:
	HitObjBall(int masterKind, int atcKind=DAMAGECOLLISION, int life=10);
	~HitObjBall(){}

	void	GetTransMatrix(D3DXMATRIX* out);

	float	GetRadius(){return m_radius;}

	void	SetRadius(float val){m_radius = fabs(val);}

};


#endif

























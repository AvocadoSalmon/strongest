#ifndef __EVENTSCENE_H__
#define __EVENTSCENE_H__

#include	"..\\system\\Scene.h"
#include	"TESTBaseObj.h"
#include	"View.h"

#define		EVENTACTORNUM	10

#define RGBA(r,g,b,a)	((DWORD)(((a)<<24)|((r)<<16)|((g)<<8)|(b)))


class	EventScene	:	public Scene{

private:
	View*		m_view;
	LPIEXMESH	m_stage;
	TESTBaseObj*	m_EventActorArray[EVENTACTORNUM];
	IEX2DOBJ*	m_blackTex;
	int			m_blackTexAlpha;

	int			m_eventTimer;
	int			m_eventStep;
	float		m_eventGrayVal;

	int			m_bgm;

public:
	EventScene();
	~EventScene();

	void	AddAlpha(int add){

		m_blackTexAlpha += add;

		if(m_blackTexAlpha>255)m_blackTexAlpha = 255;
		else if(m_blackTexAlpha<0)m_blackTexAlpha = 0;


	}

	void	EventProc();


	//	�X�V�E�`��
	void Update();
	void Render();

};





#endif
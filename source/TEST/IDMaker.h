#ifndef __IDMAKER_H__
#define __IDMAKER_H__


#include	"iextreme.h"
#include	"List.h"

#define	ID_MAXSIZE	10000




//-------------------------------------------//
//					ID発行クラス			 //
//-------------------------------------------//

class	IDMaker{

private:
	IDMaker(){
		m_IDPool = NULL;

	}

	~IDMaker(){
		ClearList(&m_IDPool);

	}

	static IDMaker* idMaker;

	IDMaker(const IDMaker& s);
	const IDMaker& operator=(const IDMaker& s);

	List<int>*	m_IDPool;

public:

	//	シングルトン
	static	IDMaker  *GetInstance(){
		if(idMaker==NULL)
			idMaker = new IDMaker();

		return idMaker;
	}

	static	void	deleteInstance(){
		delete idMaker;
		idMaker = NULL;
	}

	int		CreateID();
	int		DestroyID(int in_num);

};

#endif
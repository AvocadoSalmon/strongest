#ifndef __SOUNDMANAGER_H__
#define __SOUNDMANAGER_H__

#include	"iextreme.h"
#include	"List.h"
#include	"IDMaker.h"

//-------------------------------------------//
//				サウンド制御クラス			 //
//-------------------------------------------//

class	SoundManager{

private:
//	サウンド用データリスト
	class	ListSound{

	private:
		int	m_ID;			//	識別用ID
		bool	b_loop;		//	ループの有無
		ListSound*	next;
		ListSound*	back;

	public:
		iexSound*	m_sound;	//	サウンドクラス

		ListSound();
		~ListSound();
		
//***************************************************************************************
//	機能:	サウンドの再生&リストにサウンドデータの追加
//	引数:	
//	filename:	サウンドファイルのパス
//	head:		リストの先頭アドレスのアドレス
//	volume:		音量
//	loop:		ループの有無
//	戻り値:		識別用ID
		int 	Play(char* filename, ListSound** head, int volume, bool loop);


		//	ゲッター
		ListSound	*GetBack(){return back;}
		ListSound	*GetNext(){return next;}
		int			GetID(){return m_ID;}
		bool		GetbLoop(){return b_loop;}
		
		//	セッター
		void	SetBack(ListSound* in_back){back = in_back;}
		void	SetNext(ListSound* in_next){next = in_next;}
		void	SetID(int in_ID){m_ID = in_ID;}
		void	SetbLoop(bool flag){b_loop=flag;}

	};



	ListSound*		listSound;

	//	コンストラクタ
	SoundManager(){
		listSound = NULL;
	}

	//	デストラクタ
	~SoundManager(){
		ClearList(&listSound);
	}

	//	コピーコンストラクタ・代入演算子の使用を禁止
	SoundManager(const SoundManager& s);
	const	SoundManager& operator=(const SoundManager& s);

	static	SoundManager*	soundManager;

public:

	//	シングルトン
	static	SoundManager	*GetInstance(){
		if(soundManager==NULL){
			soundManager = new SoundManager();
		}
		return 	soundManager;
	}

	//	破棄
	static	void	deleteInstance(){
		delete soundManager;
		soundManager = NULL;
	}

//*****************************************************************************
//	機能:	サウンド読み込み、生成
//	引数:	
//	filename:	サウンドファイルのパス
//	volume:		音量
//	loop:		ループの有無
//	戻り値:		識別用ID
	int		Play(char* filename, int volume=255, bool loop=false);

//*****************************************************************************
//	機能:	再生終わったやつ開放
	void	Update();

//****************************************************************************
//	機能:	引数で指定したIDのサウンドデータ破棄
	void	Destroy(int num);

};

#endif
#ifndef __TESTBASEOBJ_H__
#define __TESTBASEOBJ_H__


#include	"iextreme.h"
#include	"MotionCorret.h"


class	DefaultA{

protected:

	D3DXVECTOR3		m_pos;		// 位置ベクトル
	float			m_scale;	// 拡大率		
	float			m_angleY;	// Y軸の回転
	float			m_angleX;
	float			m_angleZ;
	D3DXVECTOR3		m_angVec;	// 向きベクトル
	float			m_gravity;	// 重力
	bool			m_bNonGravity;	//	重力無視フラグ
	bool			m_bbound;	// 地面でバウンドフラグ
	D3DXVECTOR3		m_move;		// 移動量

	int				m_state;	// 状態
	float			m_alpha;	// 透明度

	int				m_hp;		// ヒットポイント
	int				m_maxHp;	// 最大ヒットポイント

	bool			m_bice;		//	滑るフラグ
	D3DXVECTOR2		m_inertia;	//	移動力ベクトル
	bool			m_bknocBack;//	ノックバックの初速度

	bool			m_bGround;	// 地面に立っているかのフラグ
	bool			m_bLand;	//	着地フラグ
	bool			m_bDestroy;	// オブジェクト破棄フラグ

	static  int			m_remainder;	//	存在しているオブジェクトの数
	
	//	識別番号を作る時に番号がかぶらないようにするため必要
	//static	List<int>*	m_disNumPool;	//	オブジェクト識別番号のプール
	
	//	多フレームに渡って判定が続く当たり判定において、一度ヒットしたオブジェクトを認識させるために必要
	int			m_disNum;	// オブジェクト識別用の番号

	//void		CreateDisNum();		//	識別番号を作ってくれる
	//void		DestroyDisNum(int in_num);	//	指定した番号を破棄

	//void		Turn(D3DXVECTOR3 in_target);
	

public:
	DefaultA();
	~DefaultA();

	void		Turn(float turnSpeed=2.0f, float maxTurnSpeed=0.5f);


	//	ゲッター
	D3DXVECTOR3	GetPos()const{return m_pos;}
	D3DXVECTOR3 GetPosForCammera()const{return D3DXVECTOR3(m_pos.x,m_pos.y+1.2f,m_pos.z);}
	float	GetAngle()const{return m_angleY;}
	int		GetHp()const{return m_hp;}
	bool	GetbDestroy(){return m_bDestroy;}
	int	*GetpDisNum(){return &m_disNum;}
	int	GetDisNum()const{return m_disNum;}
	D3DXVECTOR3	GetAngVec()const{return m_angVec;}
	int		GetState()const{return m_state;}
	float	GetAlpha()const{return m_alpha;}
	int		GetMaxHp()const{return m_maxHp;}

	//	セッター
	void	SetbDestroy(bool in_flag){m_bDestroy = in_flag;}
	void	SetHp(int in_hp){m_hp = in_hp;}
	void	SetState(int in_state){m_state = in_state;}
	void	SetbGround(bool flag){m_bGround=flag;}
	void	SetAngle(float in_angle){m_angleY=in_angle;}
	void	SetPos(const D3DXVECTOR3& in_pos){m_pos = in_pos;}
	void	SetAlpha(float in_alpha){m_alpha=in_alpha;}
	void	SetMaxHp(int in_maxHp){m_maxHp=in_maxHp;}
	void	SetGravity(float in_grav){m_gravity = in_grav;}
	void	SetAngVec(D3DXVECTOR3& angV){m_angVec = angV;}


};



class EquipInfo{

public:
	int	m_boneNum;
	const char*	filename;
	int m_kind;

	EquipInfo(){
		filename = NULL;
		m_boneNum = 0;
		m_kind = 0;
	}
	EquipInfo(const char* in_filename, int bone, int kind=0){
		filename = in_filename;
		m_boneNum = bone;
		m_kind = kind;
	}


	~EquipInfo(){}

	EquipInfo	Set(char* name, int num, int in_kind=0){
		filename = name;
		m_boneNum = num;
		m_kind = in_kind;
		return *this;
	}

	void	SetInfo(const char* name, int num, int in_kind=0){
		filename = name;
		m_boneNum = num;
		m_kind = in_kind;
	}

	void	SetBoneNum(int num){m_boneNum=num;}
	void	SetFileName(const char* file){filename = file;}
	void	SetKind(int num){m_kind=num;}

};
class Equip{

public:
	int m_boneNum;
	int m_ID;
	int m_kind;

	Equip(){
		m_boneNum = 0;
		m_ID = -1;
		m_kind = 0;
	}
	~Equip(){}
};

//-------------------------------------------//
//  モーションつきオブジェクトの基本クラス   //
//-------------------------------------------//

class	TESTBaseObj	:	public	DefaultA{

protected:
	LPIEX3DOBJ		m_lpObj;
	MotionCorret*   m_motCoret;	//	モーション補間クラス

	D3DXVECTOR3		m_airialMove;	//	慣性を考えたジャンプ移動量

	float			m_heightestGround_y;	//	一番高かった地面のy座標

	bool			m_bKinematic;
	void			SetKinematicBoxPose(int ID, int boneNum);

	bool			bsubmergence;

public:
	TESTBaseObj();
	~TESTBaseObj();

	void	GoAhead(float speed, D3DXVECTOR3* angVec, int motionNum);

	D3DXVECTOR3	GetFacePos(){
		D3DXVECTOR3 vec(m_pos.x,m_pos.y+2.0f,m_pos.z);
		return vec;
	}

	D3DXVECTOR3 GetPosXZ(){
		D3DXVECTOR3 vec(m_pos.x,0,m_pos.z);
		return vec;
	}
	

	void	CreateObj(char* name, const D3DXVECTOR3& in_pos, float in_angle, int in_motionNum, float in_scale=1.0f, bool in_bmotCoret=true);
	void	Action();
	void	Update();
	void	Draw(char *name=NULL);
	void	Draw_Reflect(char *name=NULL);

	int		SetMotion(const int in_motNum, int in_coretVal=10);

	void	SetAngleXYZ(float x,float y,float z){
		m_angleY = y;	
		m_angleX = x;
		m_angleZ = z;
	}


	//	ゲッター
	LPIEX3DOBJ	GetObj(){return m_lpObj;}



};

#endif
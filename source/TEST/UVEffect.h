#ifndef __UVEFFECT_H__
#define __UVEFFECT_H__


#include	"iextreme.h"
#include	"IDMaker.h"
#include	"Mover.h"


class	UVEffect{

private:
	LPIEXMESH	m_mesh;
	Vector3		m_pos;
	float		m_angleY;
	float		m_scale;
	float		m_stretch;
	int			m_timer;
	bool		m_bDestroy;
	int			m_ID;
	int			m_meshKind;
	int			m_meshNum;

	float		u,v;
	float		m_addU,m_addV;
	float		m_alpha;
	float		m_addAlpha;

	char*		m_tec;

	Mover*		m_mover;

	UVEffect();
public:
	UVEffect(int meshKind,const Vector3& pos, float addU, float addV, float addAlpha, float angleY, float scale, int time, float stretch);
	~UVEffect();

	void	SetMover(MOVERKIND kind, float speed, const Vector3& angVec, float accel=1.0f);

	void	Load(char* filename, char* tec="uvEffect");

	void	Update();

	void	Render(iexShader* shader);

	bool	GetbDestroy(){return m_bDestroy;}
	Vector3	GetPos(){return m_pos;}
	float	GetAngleY(){return m_angleY;}
	float	GetScale(){return m_scale;}
	float	GetU(){return u;}
	float	GetV(){return v;}
	float	GetAlpha(){return m_alpha;}
	int		GetID(){return m_ID;}
	char*	GetTec(){return m_tec;}
	int		GetMeshNum(){return m_meshNum;}

	void	SetID(int id){m_ID=id;}
	void	SetPos(Vector3& pos){m_pos=pos;}

};


#endif
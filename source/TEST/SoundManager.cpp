#include	"SoundManager.h"
#include	<new>

SoundManager* SoundManager::soundManager = NULL;

//-----------------------------------------------//
//				コンストラクタ					 //
//-----------------------------------------------//

SoundManager::ListSound::ListSound(){

	m_ID = -1;
	b_loop = false;
	m_sound = NULL;
	next = NULL;
	back = NULL;
}

//-----------------------------------------------//
//				デストラクタ					 //
//-----------------------------------------------//

SoundManager::ListSound::~ListSound(){
	
	IDMaker::GetInstance()->DestroyID(m_ID);
	
	delete m_sound;
	m_sound = NULL;
}

//-----------------------------------------------//
//				メモリ確保と音再生				 //
//-----------------------------------------------//

int		SoundManager::ListSound::Play(char* filename, ListSound** head, int volume, bool loop){

	ListSound*	p = NULL;
	try{
		p = new ListSound();
		p->m_sound = new iexSound();

	}catch(std::bad_alloc){
		delete p;
		p = NULL;
		return -1;
	}

	p->SetID(IDMaker::GetInstance()->CreateID());

	p->m_sound->Set(0,filename);
	p->m_sound->Play(0,loop);
	p->SetbLoop(loop);
	p->m_sound->SetVolume(0,volume);//DirectXの方ではMAX0〜MIN-10000だけどIEXの方で調整されてるMAX0〜MIN255


	// 今まで先頭だったポインタを次のポインタにする
	p->SetNext(*head);
	
	// 先頭ポインタのは常にNULLにする
	p->SetBack(NULL);

	if(p->GetNext()!=NULL){
		// 今作ったやつを次のデータの前ポインタにする
		p->GetNext()->SetBack(p);
	}

	*head = p;

	return p->GetID();


}

//-----------------------------------------------//
//				　　音管理系					 //
//-----------------------------------------------//

//	読み込みと再生
int		SoundManager::Play(char* filename, int volume, bool loop){

	if(volume>255)		volume = 255;
	else if(volume<0)	volume = 0;
	
	volume -= 255;
	volume  = -volume;

	//return ListSound::Play(filename,&listSound,volume,loop);
	return listSound->Play(filename,&listSound,volume,loop);

}

//	再生終わったやつを解放
void	SoundManager::Update(){

	ListSound* ls = listSound;
	while(ls!=NULL){
		if(!ls->m_sound->isPlay(0) && !ls->GetbLoop()){
			ChangeConnect(&ls,&listSound);
			continue;
		}
		ls = ls->GetNext();
	}

}

//	任意に開放
void	SoundManager::Destroy(int num){

	if(num==-1)return;

	for(ListSound* ls = listSound; ls!=NULL; ls=ls->GetNext()){
		if(ls->GetID()==num){
			ChangeConnect(&ls,&listSound);
			break;
		}
	}

}
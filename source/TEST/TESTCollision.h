#ifndef __COLLISION_H__
#define __COLLISION_H__

#include	<stdarg.h>
#include	"iextreme.h"
#include	"List.h"
#include	"IDMaker.h"
#include	"HitObj.h"


#define RAY_RANGE		30
#define	RAYAJUST_GROUND	5

//-------------------------------------------//
//		地面と壁の当たり判定結果クラス	　　 //
//-------------------------------------------//

class	GroundWallCollisionResult{

private:
	bool	m_bOnGround;	//	地面に接してるフラグ,初期値はfalse
	bool	m_bTouchWall;	//	壁に当たってるフラグ,初期値はfalse

public:
	GroundWallCollisionResult();
	~GroundWallCollisionResult();


	bool	GetbOnGround()const{return m_bOnGround;}
	bool	GetbTouchWall()const{return m_bTouchWall;}

	void	SetbOnGround(bool flag){m_bOnGround = flag;}
	void	SetbTouchWall(bool flag){m_bTouchWall = flag;}

};

//-------------------------------------------//
//				当たり判定クラス			 //
//-------------------------------------------//

class	Collision{

private:
//	デバッグ描画用当たり判定メッシュ
	LPIEXMESH	m_atkBox;
	LPIEXMESH	m_atkBall;
	LPIEXMESH	m_damageBox;
	LPIEXMESH	m_damageBall;
	
//	地面と壁の判定モデル
	LPIEXMESH	m_lpGroundCollisionMesh;	
	LPIEXMESH	m_lpWallCollisionMesh;	

//	当たり判定情報リスト
	class	ListHitObj{

	private:
		HitObj*		data;
		ListHitObj*	back;
		ListHitObj*	next;

	public:
		ListHitObj();
		~ListHitObj();


		ListHitObj*	Add(HitObj* instance);


		//	ゲッター
		ListHitObj*	GetNext(){return next;}
		ListHitObj* GetBack(){return back;}
		HitObj*		GetpData(){return data;}

		//	セッター
		void	SetNext(ListHitObj* in_next){next = in_next;}
		void	SetBack(ListHitObj* in_back){back = in_back;}
		void	SetpData(HitObj*    in_data){data = in_data;}

	};
	ListHitObj*	m_listHitObj;
	
	HitObj*		GetHitObjFromList(int in_ID);

//	当たり判定結果情報リスト
	class	HitResult	:	public	AtkHitResult, public	DamageHitResult{

	private:

	public:
		HitResult();

		void	SetDamageCollisionMasterKind(int kind){m_damageCollisionMasterKind = kind;}
		void	SetDamageCollisionID(int id){m_damageCollisionID = id;}
		//void	SetDamageCollisionMasterState(int state){m_damageCollisionMasterState = state;}

		//void	SetAtkCollisionMasterKind(int kind){m_atkCollisionMasterKind = kind;}
		void	SetAtkCollisionID(int id){m_atkCollisionID = id;}
		void	SetAtkCollisionDamage(int damage){m_damage = damage;}
		void	SetAtkCollisionAtkKind(int kind){m_atkCollisionAtkKind = kind;}
	};
	List<HitResult>*	m_listHitResult;



//	地面,壁判定
	bool	GroundJudge(Vector3& out_pos, const Vector3& in_pos);
	bool	WallJudge(Vector3& out_pos, const Vector3& in_rayVec, const Vector3& in_pos);


	bool	WhetherHitCheck(HitObj& objA, HitObj& objB)const;
	void	SetHitResult(HitObj& objA,HitObj& objB, HitResult& result);
//	OBB当たり判定
	bool	HitCheckOBB(HitObjOBB& objA, HitObjOBB& objB, HitResult& result);
	float   LenSegOnSeparateAxis( const D3DXVECTOR3 *Sep, const D3DXVECTOR3 *e1, const D3DXVECTOR3 *e2, const D3DXVECTOR3 *e3 );

//	球同士の当たり判定
	bool	HitCheckBallToBall(HitObjBall& objA, HitObjBall& objB, HitResult& result);

//	球と箱の当たり判定
	bool	HitCheckBallToBox(HitObjBall& ball, HitObjOBB& box, HitResult& result);
	
public:
//	コンストラクタ
	Collision();

//****************************************************************************************
//	機能：	デストラクタ。Collisionクラスとそのメンバが確保しているメモリを開放
	~Collision();

//****************************************************************************************
//	機能：	初期化。デバッグ表示用の当たり判定メッシュの読み込み
//			デバッグ描画で当たり判定メッシュを描画しないならやらなくても特に不具合なし
	void	Initialize();

//****************************************************************************************
//	機能:	地面と壁の当たり判定をしてその結果を返す
//	引数:
//	result:	地面と壁に衝突したかどうかのフラグを返す
//	out_pos:判定をした結果の座標を返す
//	in_rayVec:移動ベクトルを入れる
//	in_pos:	判定前の座標を入れる
//	備考：	移動が全て済んでから判定を行う
	void	GroundWallJudge(GroundWallCollisionResult& result, Vector3& out_pos, const Vector3& in_rayVec, const Vector3& in_pos);


//****************************************************************************************
//	機能：	地面と壁の当たり判定モデルの読み込み。
//			何も読み込まなくても特にエラーは出ないように作ってるつもり
//	引数:	モデルのあるファイルのパス
//	備考：	既に前に何か読み込んでいたら、古いモデル情報を破棄して新しい方のモデル情報が読み込まれる。
	void	LoadGroundCollisionMesh(char* filename);
	void	LoadWallCollisionMesh(char* filename);


//****************************************************************************************
//	機能：	地面と壁の当たり判定モデルの破棄
	void	DestroyGroundCollisionMesh();
	void	DestroyWallCollisionMesh();

//****************************************************************************************
//	機能：	攻撃判定、やられ判定の作成
//	引数：	HitObjOBB,HitObjBallクラスのインスタンス	
//			BOX状のプレイヤーのやられ判定作成の例：Collisionのインスタンス->CreateCollision(new HitObjOBB(MASTERKIND_PLAYER,DAMAGECOLLISION));
//	
//			確保したメモリは戻り値の値をDestroyHitObj関数を使うことで任意に開放するか
//			HitObjOBB,HitObjBallクラスのコンストラクタの初期化の設定にしたがって
//			CollisionクラスがUpdate()関数の中で開放する
//	戻り値：作った判定をCollisionクラス側で識別するためのID(0〜ID_MAXSIZEの値)を返す
//	備考：	失敗したときは-1を返す
	int		CreateCollision(HitObj* instance);


//****************************************************************************************
//	機能：	判定のグループ化
//	引数：	CreateCollision関数で得たID
//	備考：	引数の数が可変
//	int		CrossedGroup(int num, ...);/*製作中なり*/


//****************************************************************************************
//	機能：	(引数:in_ID)と同じIDを持つ判定情報の更新
//	引数：
//	in_ID:	CreateCollision関数で得たID。これと同じIDを持つ判定情報を更新できる
//	in_mat:	BOX状の判定の姿勢と位置情報を入れる。変更なしならNULLをいれる
//	in_length:BOX状の判定の各辺の長の情報を入れる、負の値いれても正の値に変換しちゃうんだからネ。変更なしならNULLをいれる
//	in_pos:	位置情報。in_matに位置情報が入っているならNULLでも問題なし。
//	b_boneAjust:ボーンに判定を合わせたい時の調整用フラグ
	void	UpdateBox(int in_ID, D3DXMATRIX* in_mat, Vector3* in_length3, Vector3* in_pos3=NULL, bool	b_boneAjust=false);

//	in_radius:	球状の判定の半径の情報を入れる、負の値いれても正の値に変換しちゃうんだからネ。
	void	UpdateBall(int in_ID, float in_radius, Vector3* in_pos3=NULL);
//	備考：	無効なIDを入れたときは何も変化なし。
	
//****************************************************************************************
//	機能：	攻撃判定、やられ判定の削除
//	引数：	CreateCollision関数で得たID。これと同じIDを持つ判定情報を削除できる
//	備考：	無効なIDを入れたときは何も削除されない。削除に成功したときIDには-1が入る
	void	DestroyHitObj(int* in_ID);

//	攻撃判定とやられ判定の当たり判定チェック
	void	HitCheck();
	DamageHitResult	GetHitCheckResult(int in_masterKind);





//	デバッグ描画
	void	Draw();

};

extern Collision*	TESTCollision;
extern int	damageCollisionID;

#endif
#ifndef __MOTIONCORRET_H__
#define __MOTIONCORRET_H__

#include	"iextreme.h"


//------------------------------------------------------//
//				モーション補間クラス					//
//------------------------------------------------------//

class	MotionCorret
{
private:

	int				m_numBone;				//  モデルのボーンの数
	float			m_incValue;
	float			m_coret;				//	現在の補間量
	bool			m_mot_coret_f;			//	モーション補間中のフラグ
	D3DXQUATERNION*	m_mot_pose; //  モーション補間時に使う その時の姿勢を格納
	D3DXVECTOR3*	m_mot_pos;  //	モーション補間時に使う その時のボーン座標を格納

	void	PosePos( float frame, LPIEX3DOBJ in_lpObj );//	今のフレームのを送る
	

public:
	MotionCorret( int const in_numBone );
	MotionCorret( const MotionCorret &in_obj );// コピーコンストラクタ
	~MotionCorret();

	MotionCorret&	operator=(const MotionCorret &in_obj);
	
	void	InitMotionCorret();		// 初期化
	
	//incValueは補間量の増加量,1/icnValueフレームで補間が完了する
	void	StartMotionCorret(LPIEX3DOBJ in_lpObj, int in_incValue=10 );	// モーション変更時に呼び出す
	void	CarryMotionCorret(LPIEX3DOBJ in_lpObj);
	

	// セッター、ゲッター
	bool	GetMotCoretF(){return m_mot_coret_f;}

};

#endif
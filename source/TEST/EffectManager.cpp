#include	"EffectManager.h"
#include	"Particle.h"

KindAndTime::KindAndTime(EFFECTSETKIND kind, int time, Vector3& pos, float scale, float angleY){

	m_kind = kind;
	m_time = time;
	m_pos  = pos;
	m_scale = scale;
	m_angleY = angleY;
	if(kind==3)m_ID=123456;
	else if(kind==4)m_ID=987654;
	else m_ID	= IDMaker::GetInstance()->CreateID();

}

KindAndTime::~KindAndTime(){

	IDMaker::GetInstance()->DestroyID(m_ID);

}

//---------------------------------------------------------------------------------------------------------------------------------//


EffectList::EffectList(){

	m_uvEffect	=	NULL;

}

EffectList::~EffectList(){

	delete m_uvEffect;
	m_uvEffect	=	NULL;

}

//-------------------------------------------//
//				  エフェクト生成             //
//-------------------------------------------//

EffectList*	EffectList::Create(int meshKind,const Vector3& pos, float addU, float addV, float addAlpha, float angleY, float scale, int time, float stretch){

	EffectList* p = NULL;
	
	p = new EffectList();
	p->m_uvEffect = new UVEffect(meshKind,pos,addU,addV,addAlpha,angleY,scale,time,stretch);
	//p->m_uvEffect->Load(filename);

	p->SetNext(this);

	p->SetBack(NULL);

	if(p->GetNext()!=NULL){
		
		p->GetNext()->SetBack(p);
	}

	return p;
}

//------------------------------------------------------------//

EffectManager::EffectManager(){

	m_effectList	=	NULL;
	m_kindAndTime	=	NULL;
	for(int i=0;i<10;i++){
		m_mesh[i] = NULL;
	}
	m_mesh[0] = new iexMesh("DATA\\uveffectsample\\uveffectsample.x");
	m_mesh[1] = new iexMesh("DATA\\Seiken_Effect\\Seiken_Effect.x");
	m_mesh[2] = new iexMesh("DATA\\Portal\\PortalEffect.x");
	m_mesh[3] = new iexMesh("DATA\\Effect\\hadou.x");
	m_mesh[4] = new iexMesh("DATA\\Aura\\Aura.x");

}

EffectManager::~EffectManager(){

	ClearList(&m_effectList);
	ClearList(&m_kindAndTime);

	
	for(int i=0;i<10;i++){
		delete m_mesh[i];
		m_mesh[i] = NULL;
	}


}


//-------------------------------------------//
//		エフェクトセットの位置を取得       //
//-------------------------------------------//

void	EffectManager::GetEffectPos(int num, Vector3* outPos){

	List<KindAndTime>*	p = m_kindAndTime;
	while(p!=NULL){
		if(p->GetData().m_ID==num){
			*outPos = p->GetData().m_pos;
		}
		p = p->GetNext();
	}
	outPos = NULL;

}

//-------------------------------------------//
//		エフェクトセットの生成情報追加       //
//-------------------------------------------//

void	EffectManager::SetEffect(EFFECTSETKIND num, Vector3 &pos, float angleY, float scale){

	int time = 0;
	switch(num){
		case EFFECTSETKIND_SAMPLE:
			time = 180;
			break;
		case EFFECTSETKIND_SEIKEN:
			time = 120;
			break;
		case EFFECTSETKIND_PORTAL:
			time = 1000000;
			break;
		case EFFECTSETKIND_DOTOU:
			time = 1000000;
			break;
		case EFFECTSETKIND_GODOTOU:
			time = 600;
			break;
		case EFFECTSETKIND_AURA:
			time = 60000;
			break;

	}

	if(time==0)return;

	KindAndTime kat(num,time,pos,scale,angleY);
	m_kindAndTime = List<KindAndTime>::Add(kat,m_kindAndTime);

}

//-------------------------------------------//
//			種類に応じてエフェクト再生       //
//-------------------------------------------//

void	EffectManager::PlayEffect(){

	List<KindAndTime>*	p = m_kindAndTime;

	KindAndTime save;
	//	エフェクトセットの再生
	while(p!=NULL){
		switch(p->GetData().m_kind){
			case EFFECTSETKIND_SAMPLE:
				this->SampleEffect(p->GetData().m_time,p->GetData().m_pos,p->GetData().m_angleY,p->GetData().m_scale);
				save = p->GetData();
				save.m_time--;
				p->SetData(save);
				break;

			case EFFECTSETKIND_SEIKEN:
				this->SeikenEffect(p->GetData().m_time,p->GetData().m_pos,p->GetData().m_angleY,p->GetData().m_scale);
				save = p->GetData();
				save.m_time--;
				p->SetData(save);
				break;

			case EFFECTSETKIND_PORTAL:
				this->PortalEffect(p->GetData().m_time,p->GetData().m_pos,p->GetData().m_angleY,p->GetData().m_scale);
				save = p->GetData();
				save.m_time--;
				p->SetData(save);
				break;

			case EFFECTSETKIND_DOTOU:
				this->DotouEffect(p->GetData().m_time,p->GetData().m_pos,p->GetData().m_angleY,p->GetData().m_scale);
				save = p->GetData();
				save.m_time--;
				p->SetData(save);
				break;

			case EFFECTSETKIND_GODOTOU:
				this->GoDotouEffect(p->GetData().m_time,p->GetData().m_pos,p->GetData().m_angleY,p->GetData().m_scale);
				save = p->GetData();
				save.m_time--;
				p->SetData(save);
				break;

			case EFFECTSETKIND_AURA:
				this->AuraEffect(p->GetData().m_time,p->GetData().m_pos,p->GetData().m_angleY,p->GetData().m_scale);
				save = p->GetData();
				save.m_time--;
				p->SetData(save);
				break;

		}
		p = p->GetNext();
	}

	//	エフェクトセットの再生が終わったら破棄
	p = m_kindAndTime;
	List<KindAndTime>* temp = p;
	while(p!=NULL){
		temp = p->GetNext();
		if(p->GetData().m_time<=0){
			ChangeConnect(&p,&m_kindAndTime);
		}
		p = temp;
	}

}

//-------------------------------------------//
//			  エフェクトセット再生           //
//-------------------------------------------//

void	EffectManager::SampleEffect(int timer, const Vector3& pos, float angleY, float scale){

	Vector3 p = pos;
	switch(timer){
		case 180:
			m_effectList = m_effectList->Create(0,p,0.003f,0.002f,-0.01f,angleY,scale,180,1.0f);
			m_effectList->m_uvEffect->SetMover(MOVERKIND_LINE,0.15f,Vector3(0,0,1),1.01f);
			break;
		case 120:
			p.y += 1.5f;
			m_effectList = m_effectList->Create(0,p,0.0015f,0.001f,-0.01f,angleY,scale,180,1.0f);
			
			break;
	}


}

void	EffectManager::SeikenEffect(int timer, const Vector3& pos, float angleY, float scale){

	Vector3 p = pos;
	switch(timer){
		case 120:
			m_effectList = m_effectList->Create(1,p,0.03f,0.03f,-0.01f,angleY,scale,120,1.0f);
			m_effectList->m_uvEffect->SetMover(MOVERKIND_LINE,0.65f,Vector3(sinf(angleY),0,cosf(angleY)),0.97f);
			break;
	}

}

void	EffectManager::PortalEffect(int timer, const Vector3& pos, float angleY, float scale){

	Vector3 p = pos;
	switch(timer){
		case 1000000:
			m_effectList = m_effectList->Create(2,p,0.0f,0.015f,0.0f,angleY,scale,1000000,1.0f);
			break;
	}

}

void	EffectManager::DotouEffect(int timer, const Vector3& pos, float angleY, float scale){

	Vector3 p = pos;
	//Particle::Dotou(pos);
	switch(timer){
		case 1000000:
			m_effectList = m_effectList->Create(3,p,-0.015f,0.0f,0.0f,angleY,scale,1000000,1.0f);
			m_effectList->m_uvEffect->SetID(123456);
			break;
	}

	EffectList*	point = m_effectList;
	while(point!=NULL){
		EffectList*	temp = point->GetNext();
		
		if(point->m_uvEffect->GetID()==123456){
			p = point->m_uvEffect->GetPos();
			Particle::Dotou(p);
			break;
		}
		point = temp;
	}

}

void	EffectManager::GoDotouEffect(int timer, const Vector3& pos, float angleY, float scale){

	Vector3 p = pos;

	switch(timer){
		case 600:
			m_effectList = m_effectList->Create(3,p,0.003f,0.002f,0.0f,angleY,1.0f,600,1.05f);
			Vector3 angVec = Vector3(sinf(angleY),0,cosf(angleY));
			angVec.Normalize();
			m_effectList->m_uvEffect->SetMover(MOVERKIND_LINE,0.3f,angVec,1.02f);
			m_effectList->m_uvEffect->SetID(987654);
			break;
	}

	EffectList*	point = m_effectList;
	while(point!=NULL){
		EffectList*	temp = point->GetNext();
		
		if(point->m_uvEffect->GetID()==987654){
			p = point->m_uvEffect->GetPos();
			Particle::Dotou(p);
			break;
		}
		point = temp;
	}

}

void	EffectManager::AuraEffect(int timer, const Vector3& pos, float angleY, float scale){

	Vector3 p = pos;
	switch(timer){
		case 60000:
			m_effectList = m_effectList->Create(4,p,0.015f,0.0f,0.0f,angleY,scale,60000,1.0f);
			m_effectList->m_uvEffect->SetID(159753);
			break;
	}

}

//-------------------------------------------//
//		  個々のエフェクトの再生と破棄       //
//-------------------------------------------//

void	EffectManager::Update(){

	EffectList*	p = m_effectList;
	while(p!=NULL){
		EffectList*	temp = p->GetNext();
		p->m_uvEffect->Update();
		if(p->m_uvEffect->GetbDestroy()==true){
			ChangeConnect(&p,&m_effectList);
		}
		p = temp;
	}

}

void	EffectManager::SetPos(int id,Vector3& pos){

	EffectList*	p = m_effectList;
	while(p!=NULL){
		EffectList*	temp = p->GetNext();
		if(p->m_uvEffect->GetID()==id){
			p->m_uvEffect->SetPos(pos);
			break;
		}
		p = temp;
	}

}

void	EffectManager::DeleteEffect(int id){

	EffectList*	p = m_effectList;
	while(p!=NULL){
		EffectList*	temp = p->GetNext();
		if(p->m_uvEffect->GetID()==id){
			ChangeConnect(&p,&m_effectList);
		}
		p = temp;
	}

	
	List<KindAndTime>*	pp = m_kindAndTime;
	List<KindAndTime>* temp = pp;
	while(pp!=NULL){
		temp = pp->GetNext();
		if(pp->GetData().m_ID==id){
			ChangeConnect(&pp,&m_kindAndTime);
		}
		pp = temp;
	}

}

//	溜めエフェクト破棄
void	EffectManager::DeleteDotou(){

	EffectList*	p = m_effectList;
	while(p!=NULL){
		EffectList*	temp = p->GetNext();
		if(p->m_uvEffect->GetID()==123456){
			ChangeConnect(&p,&m_effectList);
		}
		p = temp;
	}

	
	List<KindAndTime>*	pp = m_kindAndTime;
	List<KindAndTime>* temp = pp;
	while(pp!=NULL){
		temp = pp->GetNext();
		if(pp->GetData().m_ID==123456){
			ChangeConnect(&pp,&m_kindAndTime);
		}
		pp = temp;
	}


}

//	溜めエフェクト破棄
void	EffectManager::DeleteGoDotou(){

	EffectList*	p = m_effectList;
	while(p!=NULL){
		EffectList*	temp = p->GetNext();
		if(p->m_uvEffect->GetID()==987654){
			ChangeConnect(&p,&m_effectList);
		}
		p = temp;
	}

	
	List<KindAndTime>*	pp = m_kindAndTime;
	List<KindAndTime>* temp = pp;
	while(pp!=NULL){
		temp = pp->GetNext();
		if(pp->GetData().m_ID==987654){
			ChangeConnect(&pp,&m_kindAndTime);
		}
		pp = temp;
	}


}

//-------------------------------------------//
//			      エフェクト描画             //
//-------------------------------------------//

void	EffectManager::Draw(iexShader *shader){

	int num = 0;
	EffectList*	p = m_effectList;
	while(p!=NULL){
		//p->m_uvEffect->Render(shader);
		num = p->m_uvEffect->GetMeshNum();
		m_mesh[num]->SetAngle(p->m_uvEffect->GetAngleY());
		m_mesh[num]->SetScale(p->m_uvEffect->GetScale());
		m_mesh[num]->SetPos(p->m_uvEffect->GetPos());
		m_mesh[num]->Update();


		shader->SetValue("texPos_u",p->m_uvEffect->GetU());
		shader->SetValue("texPos_v",p->m_uvEffect->GetV());
		shader->SetValue("effectAlpha",p->m_uvEffect->GetAlpha());
		m_mesh[num]->RenderAdd(shader,p->m_uvEffect->GetTec());
		
		
		
		p = p->GetNext();
	}

}
#include	"TESTCollision.h"
#include	<new>

//------------------------------------------------------------------------------------------------------------------------------------------------------------//
//			当たり判定クラス
//------------------------------------------------------------------------------------------------------------------------------------------------------------//


//-------------------------------------------//
//				コンストラクタ				 //
//-------------------------------------------//

Collision::Collision(){

	m_atkBox				= NULL;
	m_atkBall				= NULL;
	m_damageBox				= NULL;
	m_damageBall			= NULL;

	m_lpGroundCollisionMesh	= NULL;
	m_lpWallCollisionMesh	= NULL;

	m_listHitObj			= NULL;
	m_listHitResult			= NULL;

}

//-------------------------------------------//
//				デストラクタ				 //
//-------------------------------------------//

Collision::~Collision(){

	ClearList(&m_listHitObj);

	ClearList(&m_listHitResult);

	this->DestroyGroundCollisionMesh();
	this->DestroyWallCollisionMesh();

	delete m_atkBox;
	m_atkBox = NULL;

	delete m_atkBall;
	m_atkBall = NULL;

	delete m_damageBox;
	m_damageBox = NULL;

	delete m_damageBall;
	m_damageBall = NULL;
	
//	サウンド制御クラスを作ったら移動させる必要あり
	//IDMaker::deleteInstance(); -> sceneMainのデストラクタに移動

}

//-------------------------------------------//
//					  初期化				 //
//-------------------------------------------//

void	Collision::Initialize(){

	if(m_atkBox==NULL)m_atkBox = new iexMesh("DATA\\collisionMesh\\atkBox.x");
	if(m_atkBall==NULL)m_atkBall = new iexMesh("DATA\\collisionMesh\\atkBall.x");
	if(m_damageBox==NULL)m_damageBox = new iexMesh("DATA\\collisionMesh\\damageBox.x");
	if(m_damageBall==NULL)m_damageBall = new iexMesh("DATA\\collisionMesh\\damageBall.x");

}

//-------------------------------------------//
//			地面判定モデル読み込み			 //
//-------------------------------------------//

void	Collision::LoadGroundCollisionMesh(char* filename){

	this->DestroyGroundCollisionMesh();
	m_lpGroundCollisionMesh = new iexMesh(filename);

}

//-------------------------------------------//
//				地面判定モデル破棄			 //
//-------------------------------------------//

void	Collision::DestroyGroundCollisionMesh(){

	delete m_lpGroundCollisionMesh;
	m_lpGroundCollisionMesh = NULL;

}

//-------------------------------------------//
//			壁判定モデル読み込み			 //
//-------------------------------------------//

void	Collision::LoadWallCollisionMesh(char *filename){

	this->DestroyWallCollisionMesh();
	m_lpWallCollisionMesh = new iexMesh(filename);

}

//-------------------------------------------//
//				壁判定モデル破棄			 //
//-------------------------------------------//

void	Collision::DestroyWallCollisionMesh(){

	delete m_lpWallCollisionMesh;
	m_lpWallCollisionMesh	= NULL;

}

//-------------------------------------------//
//					地面判定				 //
//-------------------------------------------//

bool	Collision::GroundJudge(Vector3& out_pos, const Vector3& in_pos){

	Vector3 out( 0, 0, 0);
	if(m_lpGroundCollisionMesh!=NULL){
		Vector3 pos( in_pos.x, in_pos.y+RAYAJUST_GROUND, in_pos.z );
		Vector3 vec( 0, -1, 0 );
		float dist = RAY_RANGE;
		m_lpGroundCollisionMesh->RayPick( &out, &pos, &vec, &dist );
	}

	bool b_onGround = false;
	if(in_pos.y < out.y){
		out_pos.y = out.y;
		out_pos.x = in_pos.x;
		out_pos.z = in_pos.z;
		b_onGround = true;
	
	}else	out_pos = in_pos;
	
	return b_onGround;

}

//-------------------------------------------//
//					壁判定				     //
//-------------------------------------------//

bool	Collision::WallJudge(Vector3& out_pos, const Vector3& in_rayVec, const Vector3& in_pos){

	bool	b_touchWall = false;

	if(m_lpWallCollisionMesh==NULL){
		out_pos = in_pos;
		return b_touchWall;
	}

	Vector3 v = in_rayVec;
	//D3DXVec3Normalize(&v,&v);
	v.Normalize();

	Vector3 vec = v;
	Vector3 out(0,0,0);
	Vector3 pos = in_pos - v *(1.0f);
	float Dist = RAY_RANGE;

	int meshNum = m_lpWallCollisionMesh->RayPick(&out,&pos,&vec,&Dist);

	if(meshNum!=-1){
		
		Vector3 B = out - in_pos;
		//float dot = D3DXVec3Dot( &v, &B );	
		float dot = Vector3Dot( v, B );
		if(dot <= 0.0f){

			Vector3 N;
			N = vec;
			//D3DXVec3Normalize(&N, &N);
			N.Normalize();
			Vector3 NP;
			//D3DXVec3Cross( &NP, &N, &v );
			Vector3Cross( NP, N, v );
			//D3DXVec3Normalize(&NP, &NP);
			NP.Normalize();
			Vector3 E;
			//D3DXVec3Cross( &E, &NP, &N );
			Vector3Cross( E, NP, N );
			//D3DXVec3Normalize(&E, &E);
			E.Normalize();

			Vector3 A;
			A = v.operator *(0.25f);
			
			//float s=D3DXVec3Dot( &E, &A );
			float s = Vector3Dot( E, A);
			out_pos = out + E * s;
			b_touchWall = true;

		}
	}
	return b_touchWall;

}

//-------------------------------------------//
//				地面と壁判定				 //
//-------------------------------------------//

void	Collision::GroundWallJudge(GroundWallCollisionResult& result, Vector3& out_pos, const Vector3 &in_rayVec, const Vector3 &in_pos){

	Vector3 pos;

	result.SetbOnGround( this->GroundJudge(pos,in_pos) );

	Vector3 vec = in_rayVec;
	vec.y = 0;
	result.SetbTouchWall( this->WallJudge(pos,vec,pos) );

	out_pos = pos;
}

//-------------------------------------------//
//				 あたり判定作成				 //
//-------------------------------------------//

int	Collision::CreateCollision(HitObj *instan){

	ListHitObj* p = m_listHitObj->Add(instan);
	
	if(p!=NULL){
		m_listHitObj = p;
		return  m_listHitObj->GetpData()->GetID();
	}else{//	当たり判定の追加に失敗した
		delete instan;
		instan = NULL;
		return -1;
	}

}

//-------------------------------------------//
//		指定したIDの当たり判定抽出			 //
//-------------------------------------------//

HitObj*	Collision::GetHitObjFromList(int in_ID){

	ListHitObj* p = m_listHitObj;
	while(p!=NULL){
		if(p->GetpData()->GetID()==in_ID)return p->GetpData();
		p = p->GetNext();
	}
	return NULL;
}

//-------------------------------------------//
//指定したIDを持つ当たり判定でグループを作る //
//-------------------------------------------//
/*
int	Collision::CrossedGroup(int num, ...){

	va_list list;
	va_start( list, num );  

	int	groupID = IDMaker::GetInstance()->CreateID();
	bool flag = false;
	HitObj* hitObj = NULL;
	
	for(int i=0; i<num; i++)
	{
		hitObj = this->GetHitObjFromList(va_arg(list, int));
		if(hitObj!=NULL){
			hitObj->SetGroupID(groupID);
			flag = true;
		}
	}
	
	va_end( list );  

	if(flag)return 	groupID;
	else {
		IDMaker::GetInstance()->DestroyID(groupID);
		return -1;
	}

}
*/
//-------------------------------------------//
//				 　　判定更新				 //
//-------------------------------------------//

//	箱状当たり判定の更新
void	Collision::UpdateBox(int in_ID, D3DXMATRIX* in_mat, Vector3* in_length3, Vector3* in_pos3, bool b_boneAjust){

	D3DXVECTOR3 in_length(in_length3->x,in_length3->y,in_length3->z);
	D3DXVECTOR3 in_pos(0,0,0);
	if(in_pos3!=NULL){
		in_pos.x = in_pos3->x;
		in_pos.y = in_pos3->y;
		in_pos.z = in_pos3->z;
	}
	

	HitObj* hitObj = this->GetHitObjFromList(in_ID);
	if(hitObj!=NULL && hitObj->GetPrimitive()==PRIMITIVE_BOX){

		if(in_length!=NULL){
			D3DXVECTOR3 len = in_length * 0.5f;//	箱の各辺の長さの半分を求める
			static_cast<HitObjOBB *>(hitObj)->SetCuveInfoLength(len);
		}

		if(in_mat!=NULL){
			static_cast<HitObjOBB *>(hitObj)->SetCuveInfoDir(*in_mat);
			hitObj->SetPos( D3DXVECTOR3(in_mat->_41,in_mat->_42,in_mat->_43) );
		}

		if(in_pos3!=NULL){
			hitObj->SetPos( in_pos );
		}
			
		//	ボーンの付け根に箱の端が行くように微調整
		if(b_boneAjust){
			CubeInfo cubeInfo = static_cast<HitObjOBB *>(hitObj)->GetCubeInfo();
			hitObj->SetPos( hitObj->GetPos() + cubeInfo.m_dir[2] * cubeInfo.m_length[2] );
		}

	}

}

//	球状の当たり判定の更新
void	Collision::UpdateBall(int in_ID, float in_radius, Vector3* in_pos3){

	D3DXVECTOR3 in_pos(in_pos3->x,in_pos3->y,in_pos3->z);

	HitObj* hitObj = this->GetHitObjFromList(in_ID);
	if(hitObj!=NULL && hitObj->GetPrimitive()==PRIMITIVE_BALL){
		if(in_radius>=0)static_cast<HitObjBall *>( hitObj )->SetRadius(in_radius);
		if(in_pos!=NULL)hitObj->SetPos(in_pos);
	}

}

//-------------------------------------------//
//			　攻撃、やられ判定削除		　　 //
//-------------------------------------------//

void	Collision::DestroyHitObj(int* in_ID){

	ListHitObj* p = m_listHitObj;

	while(p!=NULL){
		if(p->GetpData()->GetID() == *in_ID){
			ChangeConnect(&p,&m_listHitObj);
			*in_ID = -1;
			break;
		}
		p = p->GetNext();
	}

}

//-------------------------------------------//
//			当たり判定をするかどうか		 //
//-------------------------------------------//

bool	Collision::WhetherHitCheck(HitObj& objA, HitObj& objB)const{

	//	同種の判定同士で当たり判定を取っている
	if( (objA.GetAtcKind() * objB.GetAtcKind())>=0 )return false;
	//	味方の攻撃判定はパス
	if(objA.GetMansterKind()==objB.GetMansterKind())return false;

	return true;
}

//-------------------------------------------//
//	攻撃判定とやられ判定の当たり判定チェック //
//-------------------------------------------//

void	Collision::HitCheck(){



	//	古い当たり判定の結果のリストの内容を破棄
	ClearList(&m_listHitResult);
	

	ListHitObj* atk		= m_listHitObj;
	ListHitObj* damage	= m_listHitObj;
	
/*
	//	破棄フラグ立っているあたり判定を破棄
	while(atk!=NULL){
		damage = atk->GetNext();
		atk->GetpData()->Update();
		if(atk->GetpData()->GetbDestroy()==true){
			ChangeConnect(&atk,&m_listHitObj);
		}
		atk = damage;
	}
*/

	atk		= m_listHitObj;
	damage	= m_listHitObj;

	HitResult result;
	float hitFlag = false;
	List<HitResult>* hitRes_temp = NULL;

	//	あたり判定
	while(damage!=NULL){
		atk = damage->GetNext();
		while(atk!=NULL){
			if(this->WhetherHitCheck(*damage->GetpData(), *atk->GetpData())){

				switch(damage->GetpData()->GetPrimitive() | atk->GetpData()->GetPrimitive()){
					case COLLISIONKIND_BOX://	箱同士
						hitFlag = this->HitCheckOBB(
							*static_cast<HitObjOBB *>( damage->GetpData() ),
							*static_cast<HitObjOBB *>( atk->GetpData() ),
							result );
					break;

					case COLLISIONKIND_BALL://	球同士
						hitFlag = this->HitCheckBallToBall(
							*static_cast<HitObjBall *>( damage->GetpData() ),
							*static_cast<HitObjBall *>( atk->GetpData() ),
							result );
					break;

					case COLLISIONKIND_BALLandBOX://	球と箱
						if(damage->GetpData()->GetPrimitive()==PRIMITIVE_BALL){
							hitFlag = this->HitCheckBallToBox(
								*static_cast<HitObjBall *>( damage->GetpData() ),
								*static_cast<HitObjOBB  *>( atk->GetpData() ),
								result);
						}else{
							hitFlag = this->HitCheckBallToBox(
								*static_cast<HitObjBall *>( atk->GetpData() ),
								*static_cast<HitObjOBB  *>( damage->GetpData() ),
								result);
						}

					break;

				}

				if(hitFlag){
					//	当たり判定の結果情報をリストに追加
					hitRes_temp = m_listHitResult->Add(result,m_listHitResult);
					if(hitRes_temp!=NULL)m_listHitResult = hitRes_temp;
					hitFlag = false;
				}
			}
			atk = atk->GetNext();
		}
		damage	= damage->GetNext();
		atk		= m_listHitObj;
	}

}

//-------------------------------------------//
//当たり判定やった結果を集計した結果を返す 　//
//-------------------------------------------//

DamageHitResult	Collision::GetHitCheckResult(int in_masterKind){

	int	damage = 0;
	int collisionKind = -2;
	
	List<HitResult>* p = m_listHitResult;
	while(p!=NULL){
		if(p->GetData().GetDamageCollisionMasterKind()==in_masterKind){
			damage += p->GetData().GetAtkCollisionDamage();
			if(collisionKind < p->GetData().GetAtkCollisionAtkKind())collisionKind = p->GetData().GetAtkCollisionAtkKind();
		}
		p = p->GetNext();
	}

	DamageHitResult result(collisionKind,damage);
	return result;
}

//-------------------------------------------//
//	　　　球と箱の当たり判定チェック 　　　　//
//-------------------------------------------//

bool	Collision::HitCheckBallToBox(HitObjBall &ball, HitObjOBB &box, HitResult &result){

	//オブジェクト間のベクトルを格納
	D3DXVECTOR3 Interval = ball.GetPos() - box.GetPos();

	float s = 0;
	D3DXVECTOR3 vec(0,0,0);
	D3DXVECTOR3 LV(0,0,0);//最終的に長さを求めるベクトル

	for(int i=0;i<3;i++){
		

		s = D3DXVec3Dot( &Interval, &box.GetCubeInfo().m_dir[i] ) / box.GetCubeInfo().m_length[i];
		if(fabs(s)<1.0f)s=1.0f;

		//各はみ出し部分の長さ
		vec = box.GetCubeInfo().m_dir[i] * ( (1-fabs(s)) * box.GetCubeInfo().m_length[i] );

		switch (i){
			case 0:
				LV.x=D3DXVec3Length( &vec );
			break;
			case 1:
				LV.y=D3DXVec3Length( &vec );
			break;
			case 2:
				LV.z=D3DXVec3Length( &vec );
			break;
		}
	}

	if(D3DXVec3Length( &LV )<=ball.GetRadius()){
		this->SetHitResult(ball,box,result);
		return true;
	}

	return false;

}

//-------------------------------------------//
//	　　　球同士の当たり判定チェック 　　　　//
//-------------------------------------------//

bool	Collision::HitCheckBallToBall(HitObjBall &objA, HitObjBall &objB, HitResult &result){

	float	dis = D3DXVec3Length( &D3DXVECTOR3(objA.GetPos() - objB.GetPos()) );

	if(objA.GetRadius() + objB.GetRadius() >= dis){
		this->SetHitResult(objA,objB,result);
		return true;
	
	}

	return false;
}

//--------------------------------------------//
//分離軸に投影された軸成分から投影線分長を出す// 　　　　
//--------------------------------------------//

float Collision::LenSegOnSeparateAxis( const D3DXVECTOR3 *Sep, const D3DXVECTOR3 *e1, const D3DXVECTOR3 *e2, const D3DXVECTOR3 *e3 )
{
  
   float r1 = fabs(D3DXVec3Dot( Sep, e1 ));
   float r2 = fabs(D3DXVec3Dot( Sep, e2 ));
   float r3 = e3 ? (fabs(D3DXVec3Dot( Sep, e3 ))) : 0;
   return r1 + r2 + r3;
}

//-------------------------------------------//
//	　　　箱同士の当たり判定チェック 　　　　//
//-------------------------------------------//

bool	Collision::HitCheckOBB(HitObjOBB &objA, HitObjOBB &objB, HitResult& result){


	//オブジェクト間のベクトルを格納
	D3DXVECTOR3 Interval = objA.GetPos() - objB.GetPos();

	//分離軸を格納
	D3DXVECTOR3 Ae1 = objA.GetCubeInfo().m_dir[0];
	D3DXVECTOR3 Ae2 = objA.GetCubeInfo().m_dir[1];
	D3DXVECTOR3 Ae3 = objA.GetCubeInfo().m_dir[2];
	D3DXVECTOR3 Be1 = objB.GetCubeInfo().m_dir[0];
	D3DXVECTOR3 Be2 = objB.GetCubeInfo().m_dir[1];
	D3DXVECTOR3 Be3 = objB.GetCubeInfo().m_dir[2];
	//方向ベクトルを格納
	D3DXVECTOR3 NAe1 = Ae1 * objA.GetCubeInfo().m_length[0];
	D3DXVECTOR3 NAe2 = Ae2 * objA.GetCubeInfo().m_length[1];
	D3DXVECTOR3 NAe3 = Ae3 * objA.GetCubeInfo().m_length[2];
	D3DXVECTOR3 NBe1 = Be1 * objB.GetCubeInfo().m_length[0];
	D3DXVECTOR3 NBe2 = Be2 * objB.GetCubeInfo().m_length[1];
	D3DXVECTOR3 NBe3 = Be3 * objB.GetCubeInfo().m_length[2];

	//rA,rB,Lを格納
	float rA,rB,L;

	//当たったかどうかのフラグ
	bool  hitFlag = true;

	for(int i=0;i<1;i++){

		//分離軸:Ae1
		rA = objA.GetCubeInfo().m_length[0];
		rB = this->LenSegOnSeparateAxis( &Ae1, &NBe1, &NBe2, &NBe3 );
		L  = fabs(D3DXVec3Dot( &Interval, &Ae1 ));
		if(L > rA + rB){
			hitFlag = false;
			break;
		}

		//分離軸:Ae2
		rA = objA.GetCubeInfo().m_length[1];
		rB = this->LenSegOnSeparateAxis( &Ae2, &NBe1, &NBe2, &NBe3 );
		L  = fabs(D3DXVec3Dot( &Interval, &Ae2 ));
		if(L > rA + rB){
			hitFlag = false;
			break;
		}
		//分離軸:Ae3
		rA = objA.GetCubeInfo().m_length[2];
		rB = this->LenSegOnSeparateAxis( &Ae3, &NBe1, &NBe2, &NBe3 );
		L  = fabs(D3DXVec3Dot( &Interval, &Ae3 ));
		if(L > rA + rB){
			hitFlag = false;
			break;
		}

		//分離軸:Be1
		rA = this->LenSegOnSeparateAxis( &Be1, &NAe1, &NAe2, &NAe3 );
		rB = objB.GetCubeInfo().m_length[0];
		L  = fabs(D3DXVec3Dot( &Interval, &Be1 ));
		if(L > rA + rB){
			hitFlag = false;
			break;
		}

		//分離軸:Be2
		rA = this->LenSegOnSeparateAxis( &Be2, &NAe1, &NAe2, &NAe3 );
		rB = objB.GetCubeInfo().m_length[1];
		L  = fabs(D3DXVec3Dot( &Interval, &Be2 ));
		if(L > rA + rB){
			hitFlag = false;
			break;
		}

		//分離軸:Be3
		rA = this->LenSegOnSeparateAxis( &Be3, &NAe1, &NAe2, &NAe3 );
		rB = objB.GetCubeInfo().m_length[2];
		L  = fabs(D3DXVec3Dot( &Interval, &Be3 ));
		if(L > rA + rB){
			hitFlag = false;
			break;
		}

		//外積での分離軸を格納
		D3DXVECTOR3 Cross;
		
		//分離軸:C11
		D3DXVec3Cross( &Cross, &Ae1, &Be1 );
		rA = this->LenSegOnSeparateAxis( &Cross, &NAe2, &NAe3, 0 );
		rB = this->LenSegOnSeparateAxis( &Cross, &NBe2, &NBe3, 0 );
		L  = fabs(D3DXVec3Dot( &Interval, &Cross ));
		if(L > rA + rB){
			hitFlag = false;
			break;
		}

		//分離軸:C12
		D3DXVec3Cross( &Cross, &Ae1, &Be2 );
		rA = this->LenSegOnSeparateAxis( &Cross, &NAe2, &NAe3, 0 );
		rB = this->LenSegOnSeparateAxis( &Cross, &NBe1, &NBe3, 0 );
		L  = fabs(D3DXVec3Dot( &Interval, &Cross ));
		if(L > rA + rB){
			hitFlag = false;
			break;
		}

		//分離軸:C13
		D3DXVec3Cross( &Cross, &Ae1, &Be3 );
		rA = this->LenSegOnSeparateAxis( &Cross, &NAe2, &NAe3, 0 );
		rB = this->LenSegOnSeparateAxis( &Cross, &NBe1, &NBe2, 0 );
		L  = fabs(D3DXVec3Dot( &Interval, &Cross ));
		if(L > rA + rB){
			hitFlag = false;
			break;
		}

		//分離軸:C21
		D3DXVec3Cross( &Cross, &Ae2, &Be1 );
		rA = this->LenSegOnSeparateAxis( &Cross, &NAe1, &NAe3, 0 );
		rB = this->LenSegOnSeparateAxis( &Cross, &NBe2, &NBe3, 0 );
		L  = fabs(D3DXVec3Dot( &Interval, &Cross ));
		if(L > rA + rB){
			hitFlag = false;
			break;
		}

		//分離軸:C22
		D3DXVec3Cross( &Cross, &Ae2, &Be2 );
		rA = this->LenSegOnSeparateAxis( &Cross, &NAe1, &NAe3, 0 );
		rB = this->LenSegOnSeparateAxis( &Cross, &NBe1, &NBe3, 0 );
		L  = fabs(D3DXVec3Dot( &Interval, &Cross ));
		if(L > rA + rB){
			hitFlag = false;
			break;
		}

		//分離軸:C23
		D3DXVec3Cross( &Cross, &Ae2, &Be3 );
		rA = this->LenSegOnSeparateAxis( &Cross, &NAe1, &NAe3, 0 );
		rB = this->LenSegOnSeparateAxis( &Cross, &NBe1, &NBe2, 0 );
		L  = fabs(D3DXVec3Dot( &Interval, &Cross ));
		if(L > rA + rB){
			hitFlag = false;
			break;
		}

		//分離軸:C31
		D3DXVec3Cross( &Cross, &Ae3, &Be1 );
		rA = this->LenSegOnSeparateAxis( &Cross, &NAe1, &NAe2, 0 );
		rB = this->LenSegOnSeparateAxis( &Cross, &NBe2, &NBe3, 0 );
		L  = fabs(D3DXVec3Dot( &Interval, &Cross ));
		if(L > rA + rB){
			hitFlag = false;
			break;
		}

		//分離軸:C32
		D3DXVec3Cross( &Cross, &Ae3, &Be2 );
		rA = this->LenSegOnSeparateAxis( &Cross, &NAe1, &NAe2, 0 );
		rB = this->LenSegOnSeparateAxis( &Cross, &NBe1, &NBe3, 0 );
		L  = fabs(D3DXVec3Dot( &Interval, &Cross ));
		if(L > rA + rB){
			hitFlag = false;
			break;
		}

		//分離軸:C33
		D3DXVec3Cross( &Cross, &Ae3, &Be3 );
		rA = this->LenSegOnSeparateAxis( &Cross, &NAe1, &NAe2, 0 );
		rB = this->LenSegOnSeparateAxis( &Cross, &NBe1, &NBe2, 0 );
		L  = fabs(D3DXVec3Dot( &Interval, &Cross ));
		if(L > rA + rB){
			hitFlag = false;
			break;
		}
		

		//衝突してる

	}

	
	if(hitFlag){
		this->SetHitResult(objA,objB,result);
	}


	return hitFlag;
}

//-------------------------------------------//
//	　　当たった当たり判定の情報を返す 　　　//
//-------------------------------------------//

void	Collision::SetHitResult(HitObj& objA,HitObj& objB, HitResult& result){

	HitObj* atkObj = NULL;
	HitObj* damageObj = NULL;
	if(objA.GetAtcKind()!=DAMAGECOLLISION){
		atkObj = &objA;
		damageObj = &objB;
	}else{
		atkObj = &objB;
		damageObj = &objA;
	}
	atkObj->SetbDestroy(true);

	result.SetAtkCollisionAtkKind(atkObj->GetAtcKind());
	result.SetAtkCollisionDamage(atkObj->GetDamage());
	result.SetAtkCollisionID(atkObj->GetID());
	//result.SetAtkCollisionMasterKind(atkObj->GetMansterKind());
	result.SetDamageCollisionID(damageObj->GetID());
	result.SetDamageCollisionMasterKind(damageObj->GetMansterKind());
	
}

//-------------------------------------------//
//			　		デバッグ描画		　　 //
//-------------------------------------------//

void	Collision::Draw(){

	D3DXMATRIX mat;
	
	ListHitObj* p = m_listHitObj;
	while(p!=NULL){

		switch(p->GetpData()->GetPrimitive()){
			
			case PRIMITIVE_BOX:
				static_cast<HitObjOBB *>(p->GetpData())->GetTransMatrix(&mat);
				if(p->GetpData()->GetAtcKind()>=ATKKIND_PANCH){
					if(m_atkBox==NULL)break;
					m_atkBox->TransMatrix = mat;
					m_atkBox->Render();
				}else{
					if(m_damageBox==NULL)break;
					m_damageBox->TransMatrix = mat;
					m_damageBox->Render();
				}
			break;

			case PRIMITIVE_BALL:
				static_cast<HitObjBall *>(p->GetpData())->GetTransMatrix(&mat);
				if(p->GetpData()->GetAtcKind()>=ATKKIND_PANCH){
					if(m_atkBall==NULL)break;
					m_atkBall->TransMatrix = mat;
					m_atkBall->Render();
				}else{
					if(m_damageBall==NULL)break;
					m_damageBall->TransMatrix = mat;
					m_damageBall->Render();
				}
			break;

		}
		p = p->GetNext();
	}
		
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------//
//			地面と壁の当たり判定結果クラス
//------------------------------------------------------------------------------------------------------------------------------------------------------------//



//-------------------------------------------//
//				コンストラクタ				 //
//-------------------------------------------//

GroundWallCollisionResult::GroundWallCollisionResult(){

	m_bOnGround = false;
	m_bTouchWall = false;
	
}

//-------------------------------------------//
//				デストラクタ				 //
//-------------------------------------------//

GroundWallCollisionResult::~GroundWallCollisionResult(){

}


//------------------------------------------------------------------------------------------------------------------------------------------------------------//
//			HitObjのリストクラス
//------------------------------------------------------------------------------------------------------------------------------------------------------------//

//-------------------------------------------//
//				コンストラクタ				 //
//-------------------------------------------//

Collision::ListHitObj::ListHitObj(){

	data = NULL;
	back = NULL;
	next = NULL;

}

//-------------------------------------------//
//				デストラクタ				 //
//-------------------------------------------//

Collision::ListHitObj::~ListHitObj(){

	delete data;
	data = NULL;

}

//-------------------------------------------//
//			リストに情報を追加				 //
//-------------------------------------------//

Collision::ListHitObj*	Collision::ListHitObj::Add(HitObj* instance){

	ListHitObj*	p = NULL;

	//	メモリ確保
	try{
		p = new ListHitObj();
	}catch(std::bad_alloc){//	失敗
		delete p;
		p = NULL;
		return NULL;
	}

	//	データセット
	p->data = instance;
			
	//	今まで先頭だったポインタを次のポインタにする
	p->SetNext(this);
	
	//	先頭ポインタのは常にNULLにする
	p->SetBack(NULL);

	if(p->GetNext()!=NULL){
		//	今作ったやつを次のデータの前ポインタにする
		p->GetNext()->SetBack(p);
	}

	return p;


}

//------------------------------------------------------------------------------------------------------------------------------------------------------------//
//			当たり判定結果情報クラス
//------------------------------------------------------------------------------------------------------------------------------------------------------------//

//-------------------------------------------//
//				コンストラクタ				 //
//-------------------------------------------//

Collision::HitResult::HitResult()	:	AtkHitResult(), DamageHitResult(){


}

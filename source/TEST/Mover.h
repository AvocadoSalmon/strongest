#ifndef __MOVER_H__
#define __MOVER_H__

#include	"iextreme.h"


typedef	enum	tagMOVERKIND{

	MOVERKIND_LINE = 0,


}MOVERKIND;


class	Mover{

protected:
	float	m_speed;
	float	m_accel;
	Vector3	m_AngVec;

public:
	Mover();
	Mover(float speed, const Vector3& angVec, float accel);
	virtual ~Mover();

	virtual Vector3	Move()=0;

};

class	LineMove	:	public	Mover{

private:


public:
	LineMove(float speed, const Vector3& angVec, float accel);
	~LineMove();


	Vector3	Move();





};

#endif
#include	"EventScene.h"
#include	"Particle.h"
#include	"..\\system\\Framework.h"
#include	"..\\TEST\\SoundManager.h"

extern int	glBgmNum;

enum	tagEventStep{

	ESTEP0 = 30,			//	
	ESTEP1 = ESTEP0 + 90,	//  前進f
	ESTEP2 = ESTEP1 + 20,	//	止まるf
	ESTEP3 = ESTEP2 + 60,	//	見上げるf
	ESTEP4 = ESTEP3 + 110,  //  巨人鑑賞1f
	ESTEP5 = ESTEP4 + 110,	//  巨人鑑賞2f
	ESTEP6 = ESTEP5 + 175,	//  巨人鑑賞3f
	ESTEP7 = ESTEP6 + 30,	//	カメラストップf
	ESTEP8 = ESTEP7 + 200,  //	地震f
	ESTEP9 = ESTEP8 + 100,	//	プレイヤーがあたりを見回すf
	ESTEP10 = ESTEP9 + 30,	//	強いられているんだf
	ESTEP11 = ESTEP10 + 30,	//	巨人の攻撃回避f
	ESTEP12 = ESTEP11 + 60,	//	巨人の攻撃回避ごの硬直f
	ESTEP13 = ESTEP12 + 90,	//	スロープに走っていくf



}EVENTSTEP;




extern iexShader*	shader;

void	EventScene::EventProc(){

	D3DXVECTOR3 v(0,0,0);

	switch(m_eventTimer){//m_eventTimer
		case ESTEP0://  前進
			m_EventActorArray[0]->GoAhead(0.1f,&D3DXVECTOR3(0,0,-1),12);
			m_view->GoAhead(0.1f,&D3DXVECTOR3(0,0,-1));
			m_eventStep = ESTEP0;
			break;
		case ESTEP1://	止まる
			m_EventActorArray[0]->GoAhead(0.0f,&D3DXVECTOR3(0,0,-1),12);
			m_view->GoAhead(0.0f,&D3DXVECTOR3(0,0,-1));
			m_eventStep = ESTEP1;
		case ESTEP2://	見上げる
			m_EventActorArray[0]->SetMotion(23);	//	見上げるモーション
			m_eventStep = ESTEP2;
			break;
		case ESTEP3://  巨人鑑賞1
			m_view->SetPos(D3DXVECTOR3(50,180,200));
			m_view->SetTarget(D3DXVECTOR3(50,180,190));
			m_view->GoAhead(0.75f,&D3DXVECTOR3(0,1,0));
			m_view->TargetMove(0.75f,&D3DXVECTOR3(0,1,0));
			m_eventStep = ESTEP3;
			break;
		case ESTEP4://巨人鑑賞2
			m_view->SetPos(D3DXVECTOR3(-50,340,200));
			m_view->SetTarget(D3DXVECTOR3(-50,340,190));
			m_view->GoAhead(0.75f,&D3DXVECTOR3(0,1,0));
			m_view->TargetMove(0.75f,&D3DXVECTOR3(0,1,0));
			m_eventStep = ESTEP4;
			break;
		case ESTEP5://巨人鑑賞3
			m_view->SetPos(D3DXVECTOR3(0,380,200));
			m_view->SetTarget(D3DXVECTOR3(0,380,190));
			m_view->GoAhead(1.9f,&D3DXVECTOR3(0,0,1));
			m_view->TargetMove(1.9f,&D3DXVECTOR3(0,0,1));
			m_eventStep = ESTEP5;
			break;
		case ESTEP6://	カメラストップ
			m_view->GoAhead(0.0f,&D3DXVECTOR3(0,0,1));
			m_view->TargetMove(0.0f,&D3DXVECTOR3(0,0,1));
			m_eventStep = ESTEP6;
			break;
		case ESTEP7://	地震
			m_eventStep = ESTEP7;
			break;
		case ESTEP8://	主人公辺り見回し
			m_EventActorArray[0]->SetMotion(25);	//	見上げるモーション
			m_view->SetPos(D3DXVECTOR3(0,5,290));//460
			m_view->SetTarget(m_EventActorArray[0]->GetFacePos());
			m_eventStep = ESTEP8;
			break;
		case ESTEP9://	強いられているんだ
			m_EventActorArray[0]->SetMotion(24);
			m_view->SetPos(D3DXVECTOR3(0,4.7f,305.5f));//474
			m_view->SetTarget(m_EventActorArray[0]->GetFacePos());

			
			
			m_eventStep = ESTEP9;
			break;
		case ESTEP10://	巨人の攻撃回避
			m_view->SetPos(D3DXVECTOR3(0,5,315));//485
			m_view->SetTarget(D3DXVECTOR3(-5,1.0f,300));//m_EventActorArray[0]->GetPosXZ()470
			m_EventActorArray[0]->GoAhead(0.35f,&D3DXVECTOR3(-1,0,0),11);
			m_EventActorArray[0]->SetAngle(-D3DX_PI*0.5f);
			
			m_EventActorArray[2]->SetPos(D3DXVECTOR3(0,450,130));//300
			m_EventActorArray[2]->SetAngleXYZ(2.45f*0.5f,0,D3DX_PI*0.5f);
			m_EventActorArray[2]->Update();
			
			v.x = m_EventActorArray[2]->GetObj()->TransMatrix._31;
			v.y = m_EventActorArray[2]->GetObj()->TransMatrix._32;
			v.z = m_EventActorArray[2]->GetObj()->TransMatrix._33;
			D3DXVec3Normalize(&v,&v);
			m_EventActorArray[2]->GoAhead(9.5f,&v,0);
			
			
			m_eventStep = ESTEP10;
			break;
		case ESTEP11:
			m_EventActorArray[0]->GoAhead(0.0f,&D3DXVECTOR3(-1,0,0),1);
			m_eventStep = ESTEP11;
			break;
		case ESTEP12:
			v = D3DXVECTOR3(-0.5f,0,-0.6f);
			D3DXVec3Normalize(&v,&v);
			m_EventActorArray[0]->SetAngVec(v);
			m_EventActorArray[0]->GoAhead(0.27f,&v,13);
			
			m_eventStep = ESTEP12;
			break;



	}

	switch(m_eventStep){//m_eventStep
		case ESTEP0:
			m_view->SetTarget(m_EventActorArray[0]->GetPos());
			break;
		case ESTEP2:
			if(m_eventTimer>=ESTEP3-20)this->AddAlpha(13);
			break;
		case ESTEP3:
			if(m_eventTimer<=ESTEP3+20)this->AddAlpha(-13);
			else if(m_eventTimer>=ESTEP4-20)this->AddAlpha(13);
			break;
		case ESTEP4:
			if(m_eventTimer<=ESTEP4+20)this->AddAlpha(-13);
			else if(m_eventTimer>=ESTEP5-20)this->AddAlpha(13);
			break;
		case ESTEP5:
			if(m_eventTimer<=ESTEP5+20)this->AddAlpha(-13);
			break;
		case ESTEP7:
			m_view->EarthQuake(0.005f);
			m_eventGrayVal -= 0.01f;
			if(m_eventGrayVal<0.0f)m_eventGrayVal = 0.0f;
			shader->SetValue("eventGray",m_eventGrayVal);
			break;
		case ESTEP10:

			if(m_eventTimer==ESTEP10+10)Particle::SandExplosion(Vector3(0,0.5f,300));//470
			
			if(m_EventActorArray[2]->GetPos().y <= 215){
				m_EventActorArray[2]->GoAhead(0.0f,&D3DXVECTOR3(0,0,1),0);
			}else{
				m_view->EarthQuake(0.00003f);
			}
			
			break;
		case ESTEP12:
			m_EventActorArray[0]->Turn();
			if(m_eventTimer>=ESTEP13-52)this->AddAlpha(5);
			break;




	}


}

EventScene::EventScene(){

	Particle::LoadParticleTex("DATA\\particle.png");

	m_eventTimer = 0;
	m_eventStep  = ESTEP0;
	m_blackTexAlpha = 0;

	m_blackTex = new iex2DObj("DATA\\black.png");

	m_view	= new View();
	m_view->SetPos(D3DXVECTOR3(0,3,330));

	m_stage = new iexMesh("DATA\\BG\\stage01\\stage01\\stage01.imo");
	m_stage->SetScale(1.0f);
	


	for(int i=0;i<10;i++){
		m_EventActorArray[i] = NULL;
	}
	
	m_EventActorArray[0] = new TESTBaseObj();
	m_EventActorArray[0]->CreateObj("DATA\\CHR\\player\\player.IEM",D3DXVECTOR3(0,0,315),D3DX_PI,1,0.007f);//0,0,485
	m_view->SetTarget(m_EventActorArray[0]->GetPos());

	m_EventActorArray[1] = new TESTBaseObj();
	m_EventActorArray[1]->CreateObj("DATA\\CHR\\bigknight\\knight\\knight.IEM",D3DXVECTOR3(0,0,0),0,1,1);

	m_EventActorArray[2] = new TESTBaseObj();
	m_EventActorArray[2]->CreateObj("DATA\\CHR\\bigknight\\sword\\sword.IEM",D3DXVECTOR3(0,0,0),0,1,1);

	m_eventGrayVal = 1.0f;
	shader->SetValue("eventGray",m_eventGrayVal);

	SoundManager::GetInstance()->Destroy(glBgmNum);
	glBgmNum = SoundManager::GetInstance()->Play("DATA\\BGM\\BGM_2.wav",255,true);

}

EventScene::~EventScene(){

	delete m_stage;
	m_stage = NULL;
	
	for(int i=0;i<EVENTACTORNUM;i++){
		delete m_EventActorArray[i];
		m_EventActorArray[i] = NULL;
	}

	delete m_blackTex;
	m_blackTex = NULL;

	delete m_view;
	m_view = NULL;

	

}

void	EventScene::Update(){



	
	this->EventProc();

	


	for(int i=0;i<EVENTACTORNUM;i++){
		if(m_EventActorArray[i]==NULL)continue;
		m_EventActorArray[i]->Action();
	}

	for(int i=0;i<EVENTACTORNUM;i++){
		if(m_EventActorArray[i]==NULL)continue;
		m_EventActorArray[i]->Update();
	}

	if(m_eventStep<=ESTEP9){
		m_EventActorArray[2]->GetObj()->TransMatrix = *(m_EventActorArray[1]->GetObj()->GetBone(33)) * m_EventActorArray[1]->GetObj()->TransMatrix;
	}
	
	m_stage->Update();
	m_view->Update();

	m_eventTimer++;

	Particle::Update();

	SoundManager::GetInstance()->Update();

	if(m_eventTimer==ESTEP13 || KEY(KEY_A) == 3){
		ChangeScene( SCENE_MAIN );
	}

}

void	EventScene::Render(){

	if(m_view!=NULL){
		//	ビュー設定
		m_view->GetView()->Activate();
		
		//	ビュークリア
		m_view->GetView()->Clear();
	}

	for(int i=0;i<EVENTACTORNUM;i++){
		if(m_EventActorArray[i]==NULL)continue;

		if(i==1){
			m_EventActorArray[i]->Draw("copyGray");
			continue;
		}else if(i==2){
			m_EventActorArray[i]->Draw("copyGray");
			continue;
		}
		m_EventActorArray[i]->Draw("copy");
	}

	m_stage->Render(shader,"lcopy");

	Particle::Draw(shader);

	m_blackTex->Render(0,0,1280,720,0,0,64,64,RS_COPY,RGBA(255,255,255,m_blackTexAlpha));

}
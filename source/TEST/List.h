#ifndef __LISTINT_H__
#define __LISTINT_H__

#include	"iextreme.h"
#include	<new>

//-------------------------------------------//
//				リスト構造テンプレート       //
//-------------------------------------------//

template<class T>
class	List{

private:
	T		data;
	List*	back;
	List*	next;
	

public:
	List(){
		back = NULL;
		next = NULL;
	}

	~List(){

	}

//	引数:
//	data:	代入するデータ
//	head:	リストの先頭アドレス
	static List<T>	*Add(T data,List<T>* head){
		
		List<T>* p = NULL;

		//	メモリ確保
		try{

			p = new List<T>();

		}catch(std::bad_alloc){
			delete p;
			p = NULL;
			return NULL;
		}


		//	データセット
		p->SetData(data);

		p->SetNext(head);
	
		p->SetBack(NULL);

		if(p->GetNext()!=NULL){
			
			p->GetNext()->SetBack(p);
		}

		return p;
	}

	void	SetData(T     in_data){data = in_data;}
	void	SetBack(List* in_back){back = in_back;}
	void	SetNext(List* in_next){next = in_next;}

	T			GetData(){return data;}
	List*		GetBack(){return back;}
	List*		GetNext(){return next;}


};

//	消える前のポインタのつなぎ変え処理
template<class T>
void	ChangeConnect(T** p,T** head,bool destroy=true){

	if((*p)->GetNext()!=NULL)(*p)->GetNext()->SetBack((*p)->GetBack());
	if((*p)->GetBack()!=NULL)(*p)->GetBack()->SetNext((*p)->GetNext());
	if((*p) == *head)*head = (*head)->GetNext();

	if(destroy){
		delete *p;
		*p = NULL;
	}

}

//	リストの中身全部開放
template<class T>
void	ClearList(T** head){

	T *p = NULL;
	while( *head!=NULL){
		p = (*head)->GetNext();
		delete *head;
		*head = p;
	}
	*head = NULL;

}





#endif
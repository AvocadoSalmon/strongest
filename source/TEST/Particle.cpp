#include	"Particle.h"

void	Particle::Release(){

	iexParticle::Release();

}


void	Particle::LoadParticleTex(char *filename){

	iexParticle::Initialize(filename, 1024);

}

void	Particle::Sample(const Vector3 &pos){

	for(int i=0;i<10;i++){
		Vector3 move;
		move.x = (rand()%200-100) * 0.00007f;
		move.y = (rand()%200) * 0.00025f;
		move.z = (rand()%200-100) * 0.00007f;

		PARTICLE	p;
		p.type = 5;
		p.aFrame = 0;
		p.aColor = 0xFFFF0060;
		p.mFrame = 40;
		p.mColor = 0xBFFF0060;
		p.eFrame = 80;
		p.eColor = 0x00FF0060;

		p.angle = (rand()%200) * 0.025f;
		p.rotate = (rand()%200-100) * 0.003f;
		p.scale = 0.7f;
		p.stretch = 1.03f;

		p.flag = RS_COPY;

		p.Pos = pos;
		p.Move = move;
		p.Power = Vector3( 0, -0.0005f, 0 );

		iexParticle::Set(&p);

		
	}

}



void	Particle::SandExplosion(const Vector3 &pos){

	for(int i=0;i<20;i++){
		Vector3 move;
		move.x = (rand()%200-100) * 0.0003f;
		move.y = (rand()%200) * 0.00035f;
		move.z = (rand()%200-100) * 0.00075f;

		PARTICLE	p;
		p.type = 5;
		p.aFrame = 0;
		p.aColor = 0x3FFFFF00;
		p.mFrame = 40;
		p.mColor = 0x5FFFFF00;
		p.eFrame = 80;
		p.eColor = 0x00FFFF00;

		p.angle = (rand()%200) * 0.025f;
		p.rotate = (rand()%200-100) * 0.003f;
		p.scale = 2.0f;
		p.stretch = 1.01f;

		p.flag = RS_COPY;

		p.Pos = pos;
		p.Move = move;
		p.Power = Vector3( 0, -0.0015f, 0 );

		iexParticle::Set(&p);

		
	}

}

void	Particle::Dotou(const Vector3 &pos){

	for(int i=0;i<10;i++){
		Vector3 move;
		move.x = (rand()%200-100) * 0.0003f;
		move.y = (rand()%200-100) * 0.0003f;
		move.z = (rand()%200-100) * 0.0003f;

		PARTICLE	p;
		p.type = 5;
		p.aFrame = 0;
		p.aColor = 0xFFFF0000;//
		p.mFrame = 10;
		p.mColor = 0xFF880088;//
		p.eFrame = 20;
		p.eColor = 0xFF0000FF;//

		p.angle = (rand()%200) * 0.025f;
		p.rotate = (rand()%200-100) * 0.003f;
		p.scale = 0.5f;
		p.stretch = 1.01f;

		p.flag = RS_COPY;

		p.Pos = pos;
		p.Move = move;
		p.Power = Vector3( 0, -0.0f, 0 );

		iexParticle::Set(&p);

		
	}

}

void	Particle::Update(){

	iexParticle::Update();

}

void	Particle::Draw(iexShader* shader, char* tec){

	iexParticle::Render(shader, tec);



}
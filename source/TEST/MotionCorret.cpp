#include	"MotionCorret.h"


// コンストラクタ
MotionCorret::MotionCorret( int const in_numBone )
{
	m_numBone = in_numBone;
	m_incValue = 0.1f;	// 補間量、一応初期化

	m_mot_pose = (D3DXQUATERNION*)malloc(sizeof(D3DXQUATERNION)*m_numBone);
	
	m_mot_pos  = (D3DXVECTOR3*)malloc(sizeof(D3DXVECTOR3)*m_numBone);
	

	this->InitMotionCorret();
}

// コピーコンストラクタ
MotionCorret::MotionCorret(const MotionCorret &in_obj)
{
	
	m_numBone = in_obj.m_numBone;
	m_incValue = in_obj.m_incValue;
	m_coret = in_obj.m_coret;
	m_mot_coret_f = in_obj.m_mot_coret_f;
	
	m_mot_pose = (D3DXQUATERNION*)malloc(sizeof(D3DXQUATERNION)*m_numBone);
	m_mot_pos  = (D3DXVECTOR3*)malloc(sizeof(D3DXVECTOR3)*m_numBone);

	for(int i=0;i<m_numBone;i++)
	{
		m_mot_pose[i] = in_obj.m_mot_pose[i];
		m_mot_pos[i]  = in_obj.m_mot_pos[i];
	}

}

// 代入演算子のオーバーロード
MotionCorret& MotionCorret::operator =(const MotionCorret &in_obj)
{
	if(this == &in_obj)return *this;

	free(m_mot_pose);
	free(m_mot_pos);

	m_numBone = in_obj.m_numBone;
	m_incValue = in_obj.m_incValue;
	m_coret = in_obj.m_coret;
	m_mot_coret_f = in_obj.m_mot_coret_f;
	
	m_mot_pose = (D3DXQUATERNION*)malloc(sizeof(D3DXQUATERNION)*m_numBone);
	m_mot_pos  = (D3DXVECTOR3*)malloc(sizeof(D3DXVECTOR3)*m_numBone);

	for(int i=0;i<m_numBone;i++)
	{
		m_mot_pose[i] = in_obj.m_mot_pose[i];
		m_mot_pos[i]  = in_obj.m_mot_pos[i];
	}

	return *this;

}

// デストラクタ
MotionCorret::~MotionCorret()
{
	if(m_mot_pose)free(m_mot_pose);
	if(m_mot_pos) free(m_mot_pos);

}

// モーション補間系の変数初期化
void	MotionCorret::InitMotionCorret()
{
	m_coret = 0;					//	モーション補間量を初期化
	m_mot_coret_f = false;			//	モーション補間中のフラグ
}

// モーション補間開始
void	MotionCorret::StartMotionCorret(LPIEX3DOBJ in_lpObj, int in_incValue )
{
	m_coret = 0;					//	モーション補間量を初期化
	m_mot_coret_f = true;			//	モーション補間中のフラグ
	m_incValue = 1.0f / (float)in_incValue;		//	モーション補間量を代入

	this->PosePos( (float)in_lpObj->GetFrame(), in_lpObj );	//	今のボーンのクォータニオンと座標を得る
}

// モーションのボーンの姿勢と位置を得る
void	MotionCorret::PosePos( float frame, LPIEX3DOBJ in_lpObj ){

	
	int			i;
	DWORD		j;
	LPIEXANIME2	lpAnime;
	float		t;

	for( i=0 ; i<m_numBone ; i++ ){
		lpAnime = &in_lpObj->GetAnime( i );

		//	ポーズ設定
		if( lpAnime->rotNum == 1 ){
			CopyMemory( &m_mot_pose[i], &lpAnime->rot[0], sizeof(D3DXQUATERNION) );
		} else {
			for( j=0 ; j<lpAnime->rotNum-1 ; j++ ){
				if( (frame>=lpAnime->rotFrame[j]) && (frame<lpAnime->rotFrame[j+1]) ){
					t = (float)(frame-lpAnime->rotFrame[j]) / (float)(lpAnime->rotFrame[j+1] - lpAnime->rotFrame[j]);
					
					
					//
					D3DXQUATERNION	qtemp;
					D3DXQUATERNION	qtemp2;
					qtemp.x = lpAnime->rot[j].x;
					qtemp.y = lpAnime->rot[j].y;
					qtemp.z = lpAnime->rot[j].z;
					qtemp.w = lpAnime->rot[j].w;
					qtemp2.x = lpAnime->rot[j+1].x;
					qtemp2.y = lpAnime->rot[j+1].y;
					qtemp2.z = lpAnime->rot[j+1].z;
					qtemp2.w = lpAnime->rot[j+1].w;
					//
					D3DXQuaternionSlerp( &m_mot_pose[i], &qtemp, &qtemp2, t );
					//
					lpAnime->rot[j].x = qtemp.x;
					lpAnime->rot[j].y = qtemp.y;
					lpAnime->rot[j].z = qtemp.z;
					lpAnime->rot[j].w = qtemp.w;
					lpAnime->rot[j+1].x = qtemp2.x;
					lpAnime->rot[j+1].y = qtemp2.y;
					lpAnime->rot[j+1].z = qtemp2.z;
					lpAnime->rot[j+1].w = qtemp2.w;

					//


					break;
				}
			}
			if( j == lpAnime->rotNum-1 ){
				m_mot_pose[i].x = lpAnime->rot[lpAnime->rotNum-1].x;
				m_mot_pose[i].y = lpAnime->rot[lpAnime->rotNum-1].y;
				m_mot_pose[i].z = lpAnime->rot[lpAnime->rotNum-1].z;
				m_mot_pose[i].w = lpAnime->rot[lpAnime->rotNum-1].w;
			}
		}
		//	座標設定
		if( lpAnime->posNum == 0 ){
			m_mot_pos[i].x = in_lpObj->GetPosOrg(i).x;
			m_mot_pos[i].y = in_lpObj->GetPosOrg(i).y;
			m_mot_pos[i].z = in_lpObj->GetPosOrg(i).z;
		} else {
			for( j=0 ; j<lpAnime->posNum-1 ; j++ ){
				if( (frame>=lpAnime->posFrame[j]) && (frame<lpAnime->posFrame[j+1]) ){
					t = (float)(frame-lpAnime->posFrame[j]) / (float)(lpAnime->posFrame[j+1] - lpAnime->posFrame[j]);
					m_mot_pos[i].x = lpAnime->pos[j].x + (lpAnime->pos[j+1].x-lpAnime->pos[j].x)*t;
					m_mot_pos[i].y = lpAnime->pos[j].y + (lpAnime->pos[j+1].y-lpAnime->pos[j].y)*t;
					m_mot_pos[i].z = lpAnime->pos[j].z + (lpAnime->pos[j+1].z-lpAnime->pos[j].z)*t;
					break;
				}
			}
			if( j == lpAnime->posNum-1 ){
				m_mot_pos[i].x = lpAnime->pos[lpAnime->posNum-1].x;
				m_mot_pos[i].y = lpAnime->pos[lpAnime->posNum-1].y;
				m_mot_pos[i].z = lpAnime->pos[lpAnime->posNum-1].z;
			}
		}
	}

}

// モーション補間
void	MotionCorret::CarryMotionCorret(LPIEX3DOBJ in_lpObj){
	
	
	m_coret += m_incValue;	//	補間量増加
	if( m_coret >= 1.0f )m_coret = 1.0f;	//	完全に補間した,10Fで完全に補間される
	
	D3DXQUATERNION	save_quat;
	D3DXVECTOR3		save_vec;
	for(int i=0;i<m_numBone;i++){
		save_quat.x = in_lpObj->GetPose(i)->x;	//	次のモーションのポーズを格納
		save_quat.y = in_lpObj->GetPose(i)->y;
		save_quat.z = in_lpObj->GetPose(i)->z;
		save_quat.w = in_lpObj->GetPose(i)->w;

		save_vec.x  = in_lpObj->GetBonePos(i)->x;	//	次のモーションのボーン座標を格納
		save_vec.y  = in_lpObj->GetBonePos(i)->y;
		save_vec.z  = in_lpObj->GetBonePos(i)->z;
	
		//	補間	:セットするポーズ、前のポーズ、次のモーションのポーズ、補間量
		D3DXQuaternionSlerp( &save_quat, m_mot_pose+i, &save_quat, m_coret );
		D3DXVec3Lerp( &save_vec, m_mot_pos+i, &save_vec, m_coret );

		//	セット
		in_lpObj->SetPose( i, save_quat );
		in_lpObj->SetBPos( i, save_vec );
		
	}


	in_lpObj->UpdateBoneMatrix();
	in_lpObj->UpdateSkinMesh();
	
	//	補間し終わったので補間系メンバを初期化
	if( m_coret >= 1.0f )this->InitMotionCorret();

}
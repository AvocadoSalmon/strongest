
#include	"TESTBaseObj.h"



//	静的メンバ変数初期化
int			DefaultA::m_remainder  = 0;

DefaultA::DefaultA(){

	m_pos = D3DXVECTOR3(0,0,0);
	m_scale = 1.0f;
	m_angleY = .0f;
	m_angleX = .0f;
	m_angleZ = .0f;
	m_angVec = D3DXVECTOR3(0,0,1);
	m_gravity = .0f;
	m_bNonGravity = false;
	m_bbound = false;
	m_move = D3DXVECTOR3(0,0,0);

	m_maxHp = 10;
	m_hp = m_maxHp;
	m_state = -1;
	m_alpha = 1.0f;

	m_bice = false;
	m_inertia = D3DXVECTOR2(0,0);
	m_bGround = true;
	m_bLand   = false;
	m_bDestroy = false;
	m_bknocBack = false;

	
	m_disNum = -1;

	//	オブジェクトの総数インクリ
	m_remainder++;

}


DefaultA::~DefaultA(){

	//	オブジェクトの総数デクリ
	m_remainder--;
	

}

//-----------------------------------------------//
//				  移動方向を向				     //
//-----------------------------------------------//

void	DefaultA::Turn(float turnSpeed, float maxTurnSpeed){

	// 向き修正
	D3DXVECTOR2 Vm,Vp;	//	向きたいベクトル、今の向きベクトル
	float dot;	//	内積角
	float g;	//	外積左右判定用

	Vm.x = m_angVec.x;
	Vm.y = m_angVec.z;
	D3DXVec2Normalize( &Vm, &Vm );
	Vp.x = sinf( m_angleY );
	Vp.y = cosf( m_angleY );
	

	//	内積による補正量
	dot = D3DXVec2Dot( &Vm, &Vp );

	dot = (1-dot) * 1.0f;
	if(dot>0.5f)dot=0.5f;

	//	外積による左右判定
	g = D3DXVec2CCW( &Vm, &Vp );

	if(g > 0)m_angleY += dot;
	else     m_angleY -= dot;


}
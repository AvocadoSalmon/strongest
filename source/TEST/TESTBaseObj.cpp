
#include	"TESTBaseObj.h"






//	シェーダー
//extern iexShader*	shader;
extern iexShader*	shader;





//-----------------------------------------------//
//				コンストラクタ					 //
//-----------------------------------------------//

TESTBaseObj::TESTBaseObj(){

	m_lpObj = NULL;
	m_motCoret = NULL;
	m_airialMove = D3DXVECTOR3(0,0,0);
	m_heightestGround_y = -100;
	m_bKinematic = false;
	bsubmergence = false;

}

//-----------------------------------------------//
//				デストラクタ					 //
//-----------------------------------------------//

TESTBaseObj::~TESTBaseObj(){

	
	delete m_lpObj;
	m_lpObj = NULL;
	

	delete m_motCoret;
	m_motCoret = NULL;
	
	
}

//-----------------------------------------------//
//				オブジェクト生成				 //
//-----------------------------------------------//

void	TESTBaseObj::CreateObj(char* name, const D3DXVECTOR3& in_pos, float in_angle, int in_motionNum, float in_scale, bool in_bmotCoret){

	
	
	// オブジェクト読み込み
	
	m_lpObj = new iex3DObj(name);
	
	
	// モーション補間クラス用メモリ確保
	if(in_bmotCoret){
		
		m_motCoret = new MotionCorret(m_lpObj->GetNumBone());
		
		// 初期化
		m_motCoret->InitMotionCorret();
	}


	m_lpObj->SetMotion(in_motionNum);

	m_pos = in_pos;
	m_scale = in_scale;
	m_angleY = in_angle;
	m_angVec.x = sinf(m_angleY);
	m_angVec.y = 0;
	m_angVec.z = cosf(m_angleY);
	m_gravity = .0f;

	m_lpObj->SetScale(in_scale);
	m_lpObj->SetAngle(m_angleY);
	m_lpObj->SetPos(Vector3(m_pos.x,m_pos.y,m_pos.z));
	m_lpObj->Update();

	m_hp = 100 + rand()%200;

	
	
}

//-----------------------------------------------//
//					  行動				         //
//-----------------------------------------------//

void	TESTBaseObj::Action(){

	
	m_pos += m_move;
	
	

	//	重力処理
	


	//	地面,天井判定

	

	
	

	//	着地フラグを立てる
	

	//	壁判定
	

}

//-----------------------------------------------//
//					  更新				         //
//-----------------------------------------------//

void	TESTBaseObj::Update(){


	
	m_lpObj->SetScale(m_scale);
	m_lpObj->SetAngle(m_angleX,m_angleY,m_angleZ);
	m_lpObj->SetPos(Vector3(m_pos.x,m_pos.y,m_pos.z));

	if(m_motCoret!=NULL){//	モーション補間あり
		if( m_motCoret->GetMotCoretF() == false ){//	モーション補間をしていない
			m_lpObj->Animation();
		}

		m_lpObj->Update();

		if( m_motCoret->GetMotCoretF() == true ){	
			m_motCoret->CarryMotionCorret(m_lpObj);//	モーション補間
		}

	}else{//	モーション補間なし
		m_lpObj->Animation();
		m_lpObj->Update();
	
	}

}




//-----------------------------------------------//
//					  描画						 //
//-----------------------------------------------//

void	TESTBaseObj::Draw(char *name){

	if(m_lpObj==NULL)return;

	if(name==NULL)m_lpObj->Render();	//	シェーダーなし
	else m_lpObj->Render(shader,name);  //  あり

}

void	TESTBaseObj::Draw_Reflect(char *name){

	m_lpObj->SetScale(m_scale,-m_scale,m_scale);
	m_lpObj->SetPos(m_pos.x, -m_pos.y, m_pos.z);
	m_lpObj->iexMesh::Update();
	if(name==NULL)m_lpObj->Render();	//	シェーダーなし
	else m_lpObj->Render(shader,name);  //  あり

	m_lpObj->SetScale(m_scale);
	m_lpObj->SetPos(m_pos.x, m_pos.y, m_pos.z);
	m_lpObj->iexMesh::Update();

}

//-----------------------------------------------//
//				モーション設定				     //
//-----------------------------------------------//

int		TESTBaseObj::SetMotion(const int in_motNum, int in_coretVal){

	if(m_lpObj->GetMotion() == in_motNum)return 0;// モーション変更なし
	

	if(m_motCoret!=NULL){//	モーション補間あり
		m_motCoret->StartMotionCorret(m_lpObj, in_coretVal);// モーション変われば補間開始
		m_lpObj->SetMotion(in_motNum);
		return in_coretVal;	// 補間にかかるフレーム数を返す
	
	}else{//	モーション補間なし
		m_lpObj->SetMotion(in_motNum);
		return 0;
	}
		
}






void	TESTBaseObj::GoAhead(float speed, D3DXVECTOR3* angVec, int motionNum){

	this->SetMotion(motionNum);
	D3DXVec3Normalize(angVec,angVec);
	m_move = *angVec * speed;


}



#include	"IDMaker.h"

IDMaker*	IDMaker::idMaker = NULL;

//-------------------------------------------//
//					ID発行				 　　//
//-------------------------------------------//

int	IDMaker::CreateID(){

	int id = 0;
	List<int>* temp = m_IDPool;
	do{
		id = rand()%ID_MAXSIZE;
		while(temp!=NULL){
			if(temp->GetData()==id){
				temp = m_IDPool;
				break;
			}
			temp = temp->GetNext();
		}
	}while(temp!=NULL);

	m_IDPool = m_IDPool->Add(id,m_IDPool);

	return id;
}

//-------------------------------------------//
//				任意のID削除				 //
//-------------------------------------------//

int 	IDMaker::DestroyID(int in_num){

	List<int>* temp = m_IDPool;
	while(temp!=NULL){
		if(temp->GetData() == in_num){
			ChangeConnect(&temp,&m_IDPool);
			delete temp;
			temp = NULL;
			return -1;
		}
		temp = temp->GetNext();
	}
	return in_num;
}


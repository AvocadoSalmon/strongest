#include	"UVEffect.h"



UVEffect::UVEffect(int meshKind,const Vector3& pos, float addU, float addV, float addAlpha, float angleY, float scale, int time, float stretch){

	m_mesh		=	NULL;
	m_pos		=	pos;
	m_angleY	=	angleY;
	m_scale		=	scale;
	m_stretch	=	stretch;
	m_timer		=	time;
	m_bDestroy	=	false;
	m_ID		=	IDMaker::GetInstance()->CreateID();
	m_meshKind	=   0;
	m_meshNum   =   meshKind;

	u			=	0.0f;
	v			=	0.0f;
	m_alpha		=	1.0f;

	m_addU		=   addU;
	m_addV		=	addV;
	m_addAlpha  =	addAlpha;

	m_tec		=	"uvEffect";

	m_mover		=	NULL;


}

UVEffect::~UVEffect(){

	delete m_mesh;
	m_mesh	= NULL;

	delete m_mover;
	m_mover = NULL;

	IDMaker::GetInstance()->DestroyID(m_ID);
	
}

void	UVEffect::SetMover(MOVERKIND kind, float speed, const Vector3& angVec, float accel){

	if(m_mover!=NULL){
		delete m_mover;
		m_mover = NULL;
	}

	switch(kind){
		case MOVERKIND_LINE:
			m_mover = new LineMove(speed,angVec,accel);
			break;

	}


}

void	UVEffect::Load(char *filename, char *tec){

	if(m_mesh==NULL){
		m_mesh = new iexMesh(filename);
		m_tec  = tec;
	}

}

void	UVEffect::Update(){

	m_timer--;
	if(m_timer<=0)m_bDestroy = true;

	u	+= m_addU;
	v	+= m_addV;
	m_alpha += m_addAlpha;

	m_scale *= m_stretch;

	//	移動クラスのインスタンスがあれば移動もする
	if(m_mover!=NULL){
		m_pos += m_mover->Move();
	}
/*
	if(m_mesh!=NULL){
		m_mesh->SetAngle(m_angleY);
		m_mesh->SetPos(m_pos);
		m_mesh->SetScale(m_scale);
		m_mesh->Update();
	}
*/

}

void	UVEffect::Render(iexShader* shader){
/*
	if(m_mesh!=NULL){

		shader->SetValue("texPos_u",u);
		shader->SetValue("texPos_v",v);
		shader->SetValue("effectAlpha",m_alpha);
		m_mesh->Render(shader,m_tec);//shader,m_tec
	}
*/

}
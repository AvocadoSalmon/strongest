#ifndef __PARTICLE_H__
#define __PARTICLE_H__


#include	"iextreme.h"




class	Particle{

public:
	static  void	Release();

	static	void	LoadParticleTex(char* filename);

	static	void	Sample(const Vector3& pos);
	static	void	SandExplosion(const Vector3 &pos);
	static  void	Dotou(const Vector3 &pos);

	static	void	Update();

	static	void	Draw(iexShader* shader, char* tec=NULL);






};

#endif
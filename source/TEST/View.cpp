
#include	"view.h"

extern iexShader*	shader;

//-----------------------------------------------//
//				コンストラクタ					 //
//-----------------------------------------------//

View::View(){

	view = new iexView();
	if(view==NULL)exit(1);// メモリ確保失敗
	view->SetProjection( D3DX_PI/4, 1.0f, 1200.0f );
	view->Set( .0f, 2.0f, -10.0f, .0f, .0f, .0f );
	m_pos = D3DXVECTOR3(0, 2.0f, -10.0f);
	m_target = D3DXVECTOR3(0, 0, 0);

	m_targetMove = D3DXVECTOR3(0, 0, 0);
	m_move = D3DXVECTOR3(0, 0, 0);
	m_quake = D3DXVECTOR3(0, 0, 0);

}

//-----------------------------------------------//
//				デストラクタ					 //
//-----------------------------------------------//

View::~View(){

	if(view!=NULL){
		delete view;
		view = NULL;
	}

}

void	View::Move(D3DXVECTOR3& vec, float speed){

	D3DXVECTOR3 v = vec;
	D3DXVec3Normalize(&v,&v);

	m_pos += v * speed;
}

void	View::Move(D3DXVECTOR3 &move){

	m_pos += move;

}

bool	View::ChangeTarget(const D3DXVECTOR3& in_pos,float speed){

	D3DXVECTOR3 v = in_pos - m_target;
	float d = D3DXVec3Length(&v);
	D3DXVec3Normalize(&v,&v);

	bool flag=false;
	if(speed>d){
		speed= d;
		flag = true;
	}

	m_targetMove += v.operator *(speed);

	return flag;

}

//-----------------------------------------------//
//				     更新					     //
//-----------------------------------------------//

void	View::Update(const D3DXVECTOR3* playerPos){

	//	プレイヤーとの距離
	float dx = playerPos->x - m_pos.x;
	float dz = playerPos->z - m_pos.z;
	float d = sqrtf( dx*dx + dz*dz );

	if( d > 11.0f )
	{
		dx /= d;
		dz /= d;
		m_pos.x = playerPos->x - dx*11.0f;
		m_pos.z = playerPos->z - dz*11.0f;
	
	}else if( d < 5.0f){

		dx /= d;
		dz /= d;
		m_pos.x = playerPos->x - dx*5.0f;
		m_pos.z = playerPos->z - dz*5.0f;

	}
	m_pos.y = playerPos->y + 4.0f;
	if(m_pos.y<10.0f)m_pos.y = 10.0f;



	//if(m_pos.y<=9.5f)m_pos.y=9.5f;
	D3DXVECTOR3	Target( playerPos->x, playerPos->y+1.5f, playerPos->z );
	if(Target.y<0)Target.y = 0;
	Vector ps;
	ps.x = m_pos.x;
	ps.y = m_pos.y;
	ps.z = m_pos.z;
	Vector tg;
	tg.x = Target.x;
	tg.y = Target.y;
	tg.z = Target.z;
	view->Set( ps, tg );

	shader->SetValue( "ViewPos", Vector3(m_pos.x,m_pos.y,m_pos.z) );

}

void	View::Update(){

	m_target += m_targetMove;
	m_pos += m_move;

	Vector ps;
	ps.x = m_pos.x + m_quake.x;
	ps.y = m_pos.y + m_quake.y;
	ps.z = m_pos.z + m_quake.z;
	Vector tg;
	tg.x = m_target.x + m_quake.x;
	tg.y = m_target.y + m_quake.y;
	tg.z = m_target.z + m_quake.z;
	view->Set( ps, tg );

	
	view->Set( ps, tg );

	shader->SetValue( "ViewPos", Vector3(m_pos.x,m_pos.y,m_pos.z) );

	m_quake = D3DXVECTOR3(0, 0, 0);

}




void	View::GoAhead(float speed, D3DXVECTOR3* angVec){

	D3DXVec3Normalize(angVec,angVec);
	m_move = *angVec * speed;

}

void	View::TargetMove(float speed, D3DXVECTOR3 *angVec){

	D3DXVec3Normalize(angVec,angVec);
	m_targetMove = *angVec * speed;

}

void	View::EarthQuake(float power){

	m_quake.x += (float)(rand()%2000-1000) * power;
	m_quake.y += (float)(rand()%2000-1000) * power;
	m_quake.z += (float)(rand()%2000-1000) * power;

}
#ifndef __VIEW_H__
#define __VIEW_H__


#include	"iextreme.h"

class	View{

private:
	iexView*	view;
	D3DXVECTOR3	m_pos;
	D3DXVECTOR3 m_target;

	D3DXVECTOR3 m_targetMove;
	D3DXVECTOR3 m_move;
	D3DXVECTOR3 m_quake;

public:
	View();
	~View();

	void	GoAhead(float speed, D3DXVECTOR3* angVec);
	void	TargetMove(float speed, D3DXVECTOR3* angVec);
	void	EarthQuake(float power);

	void	Update(const D3DXVECTOR3* playerPos);
	void	Update();

	void	Move(D3DXVECTOR3& moveVec, float speed);
	void	Move(D3DXVECTOR3& move);


	void	SetPos(const D3DXVECTOR3& in_pos){m_pos = in_pos;}
	void	SetTarget(const D3DXVECTOR3& in_pos){m_target = in_pos;}
	void	SetTargetMove(const D3DXVECTOR3& in_move){m_targetMove=in_move;}
	void	TargetMoveInit(){m_targetMove=D3DXVECTOR3(0,0,0);}

	bool	ChangeTarget(const D3DXVECTOR3& in_pos,float speed=0.1f);

	D3DXVECTOR3	GetPos()const{return m_pos;}

	iexView	*GetView(){return view;}

};


#endif
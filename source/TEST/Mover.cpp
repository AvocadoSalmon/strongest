#include	"Mover.h"



Mover::Mover(){

	m_speed	= 0.15f;
	m_accel = 1.0f;
	m_AngVec.x = 0;
	m_AngVec.y = 0;
	m_AngVec.z = 1;

}

Mover::Mover(float speed, const Vector3& angVec, float accel){

	m_speed = speed;
	m_accel = accel;
	m_AngVec = angVec;

}

Mover::~Mover(){}



LineMove::LineMove(float speed, const Vector3& angVec, float accel) : Mover(speed,angVec,accel){




}

LineMove::~LineMove(){}

Vector3	LineMove::Move(){

	Vector3 move(0,0,0);

	move = m_AngVec * m_speed;
	m_speed *= m_accel;

	return move;

}
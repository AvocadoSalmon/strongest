#include	"HitObj.h"

//------------------------------------------------------------------------------------------------------------------------------------------------------------//
//			当り判定した結果情報クラス
//------------------------------------------------------------------------------------------------------------------------------------------------------------//

AtkHitResult::AtkHitResult(){

	m_damageCollisionMasterKind	=	-1;
	m_damageCollisionID			=	-1;
	//m_damageCollisionMasterState=	-1;

}

DamageHitResult::DamageHitResult(){

	//m_atkCollisionMasterKind	=	-1;
	m_atkCollisionID			=	-1;
	m_damage					=	 0;
	m_atkCollisionAtkKind		=	-2;

}

DamageHitResult::DamageHitResult(int atkKind, int damage){

	//m_atkCollisionMasterKind	=	masterKind;
	m_atkCollisionID			=	-1;
	m_damage					=	damage;
	m_atkCollisionAtkKind		=	atkKind;

}


//------------------------------------------------------------------------------------------------------------------------------------------------------------//
//			当り判定基底クラス
//------------------------------------------------------------------------------------------------------------------------------------------------------------//

//-------------------------------------------//
//				コンストラクタ				 //
//-------------------------------------------//

HitObj::HitObj(int masterKind, int life, int atcKind){

	m_pos.x			= 0;
	m_pos.y			= 0;
	m_pos.z			= 0;
	m_masterKind	= masterKind;
	m_ID			= IDMaker::GetInstance()->CreateID();
	m_groupID       = -1;
	m_bDestroy		= false;
	m_life			= life;
	m_atcKind		= atcKind;
	m_damage		= 0;

	if(atcKind==DAMAGECOLLISION){
		m_collisionDestroyKind = COLLISION_DESTROY_KIND_OPTION;
	}else{
		m_collisionDestroyKind = COLLISION_DESTROY_KIND_TIMER;
	}

}

//-------------------------------------------//
//				デストラクタ				 //
//-------------------------------------------//

HitObj::~HitObj(){

	IDMaker::GetInstance()->DestroyID(m_ID);

}

//-------------------------------------------//
//						更新				 //
//-------------------------------------------//

void	HitObj::Update(){

	if(m_collisionDestroyKind!=COLLISION_DESTROY_KIND_OPTION)this->m_life--;
	if(m_life<0)m_bDestroy = true;

}

//------------------------------------------------------------------------------------------------------------------------------------------------------------//
//			OBB判定情報クラス
//------------------------------------------------------------------------------------------------------------------------------------------------------------//

//-------------------------------------------//
//				コンストラクタ				 //
//-------------------------------------------//

CubeInfo::CubeInfo(){

	m_dir[0] = D3DXVECTOR3(1,0,0);
	m_dir[1] = D3DXVECTOR3(0,1,0);
	m_dir[2] = D3DXVECTOR3(0,0,1);
	m_length[0]=1;
	m_length[1]=1;
	m_length[2]=1;
}

//-------------------------------------------//
//				コンストラクタ				 //
//-------------------------------------------//

HitObjOBB::HitObjOBB(int masterKind, int atcKind, int life) : HitObj(masterKind,life,atcKind){

	m_myPrimitive = PRIMITIVE_BOX;

}

//-------------------------------------------//
//			OBB判定情報クラスの設定			 //
//-------------------------------------------//

void	HitObjOBB::CreateCubeInfo(const D3DXMATRIX& mat, const D3DXVECTOR3& length, CubeInfo& out){

	out.m_dir[0].x = mat._11;
	out.m_dir[0].y = mat._12;
	out.m_dir[0].z = mat._13;
	D3DXVec3Normalize(&out.m_dir[0],&out.m_dir[0]);
	out.m_length[0] = length.x;

	out.m_dir[1].x = mat._21;
	out.m_dir[1].y = mat._22;
	out.m_dir[1].z = mat._23;
	D3DXVec3Normalize(&out.m_dir[1],&out.m_dir[1]);
	out.m_length[1] = length.y;

	out.m_dir[2].x = mat._31;
	out.m_dir[2].y = mat._32;
	out.m_dir[2].z = mat._33;
	D3DXVec3Normalize(&out.m_dir[2],&out.m_dir[2]);
	out.m_length[2] = length.z;

}

//-------------------------------------------//
//	デバッグ表示用のメッシュの姿勢行列を返す //
//-------------------------------------------//

void	HitObjOBB::GetTransMatrix(D3DXMATRIX *out){

	D3DXMatrixIdentity(out);
	D3DXVECTOR3 vec;
	
	vec	= this->m_cubeInfo.m_dir[0];
	vec *= m_cubeInfo.m_length[0];
	out->_11 = vec.x;
	out->_12 = vec.y;
	out->_13 = vec.z;

	vec	= this->m_cubeInfo.m_dir[1];
	vec *= m_cubeInfo.m_length[1];
	out->_21 = vec.x;
	out->_22 = vec.y;
	out->_23 = vec.z;

	vec	= this->m_cubeInfo.m_dir[2];
	vec *= m_cubeInfo.m_length[2];
	out->_31 = vec.x;
	out->_32 = vec.y;
	out->_33 = vec.z;

	out->_41 = this->m_pos.x;
	out->_42 = this->m_pos.y;
	out->_43 = this->m_pos.z;

}

//------------------------------------------------------------------------------------------------------------------------------------------------------------//
//			球状のあたり判定情報クラス
//------------------------------------------------------------------------------------------------------------------------------------------------------------//

//-------------------------------------------//
//				コンストラクタ				 //
//-------------------------------------------//

HitObjBall::HitObjBall(int masterKind, int atcKind, int life) : HitObj(masterKind,life,atcKind){

	m_myPrimitive = PRIMITIVE_BALL;
	m_radius = 1.0f;

}

//-------------------------------------------//
//	デバッグ表示用のメッシュの姿勢行列を返す //
//-------------------------------------------//

void	HitObjBall::GetTransMatrix(D3DXMATRIX* out){

	D3DXMatrixIdentity(out);
	out->_11 *= m_radius;
	out->_22 *= m_radius;
	out->_33 *= m_radius;

	out->_41 = m_pos.x;
	out->_42 = m_pos.y;
	out->_43 = m_pos.z;

}

#ifndef _SYSTEM_H_
#define _SYSTEM_H_

#include	"Scene.h"

//#include	"sceneMain.h"

//*****************************************************************************
//
//		ゲームシステム全般
//
//*****************************************************************************



//*****************************************************************************************************************************
//
//
//
//*****************************************************************************************************************************
#define DEL(x)			if(x)delete x; x = NULL;
#define EULER			PI/180.0f
#define RAD				180.0f/PI
#define SCREEN_WIDTH	1280
#define SCREEN_HEIGHT	720


//	シーン切り替え用定数
enum{
	SCENE_TITLE,
	SCENE_LOAD,
	SCENE_MAIN,
	SCENE_EVENT,
	SCENE_ENDING,
};


//*****************************************************************************
//		グローバル変数
//*****************************************************************************
extern iexShader* shader;
extern iexShader* shader2D;

//*****************************************************************************
//		関数
//*****************************************************************************
//------------------------------------------------------
//	システム初期化・解放
//------------------------------------------------------
void	SYSTEM_Initialize();
void	SYSTEM_Release();

void ChangeScene( int ModeNum );



#endif	// _SYSTEM_H_
#ifndef _PLAYER_H_
#define _PLAYER_H_

#include "BaseOBJ.h"

extern bool bOver;

//-----------------------------------
//	定数
//-----------------------------------

//	行動モード
enum
{
	PMODE_NORMAL,		//	通常モード
	PMODE_JUMP,			//	ジャンプモード
	PMODE_EMERGENCY,	//	緊急回避モード
	PMODE_COMBO,		//	コンボモード
	PMODE_POWER,		//	溜め攻撃モード
	PMODE_SPECIAL,		//	屠怒滅の一撃モード
	PMODE_DAMAGE,		//	ダメージモード
	PMODE_DEATH,		//	死亡モード
	PMODE_2DMOVE,		//	2D移動モード
	PMODE_2DJUMP,		//	2Dジャンプモード
	PMODE_FALL,			//	落下モード
};

//	モーション一覧
enum
{
	PMOTION_LINEAR,		//	補間
	PMOTION_WAIT,		//	待機
	PMOTION_COMBO1,		//	コンボ１
	PMOTION_COMBO2,		//	コンボ２
	PMOTION_COMBO3,		//	コンボ３
	PMOTION_POWER1,		//	溜め１
	PMOTION_POWER2,		//	溜め２
	PMOTION_POWER3,		//	溜め３
	PMOTION_JUMP,		//	ジャンプ
	PMOTION_FALL,		//	落下中
	PMOTION_LAND,		//	着地
	PMOTION_EMERGENCY,	//	緊急回避
	PMOTION_WALK,		//	歩き
	PMOTION_RUN,		//	走り
	PMOTION_RAPID,		//	高速移動
	PMOTION_DAMAGE,		//	ダメージ
	PMOTION_DOWN,		//	倒れ
	PMOTION_SITUP,		//	起き上がり
	PMOTION_SPECIAL1,	//	屠怒滅の一撃モーション１
	PMOTION_SPECIAL2,	//	屠怒滅の一撃モーション２
	PMOTION_SPECIAL3,	//	屠怒滅の一撃モーション３
	PMOTION_STEP_R,		//	高速右ステップ
	PMOTION_STEP_L,		//	高速左ステップ

};

//	SE
enum
{
	SE_RUN,				//	足音
	SE_JUMP,			//	ジャンプ声
	SE_COMBO1,			//	コンボ声１
	SE_COMBO2,			//	コンボ声２
	SE_COMBO3,			//	コンボ声３
	SE_COMBO3_2,		//	コンボ声３−２
	SE_SEIKEN1,			//	溜め攻撃声１
	SE_SEIKEN2,			//	溜め攻撃声２
	SE_HADOO1,			//	屠怒滅の一撃声１
	SE_HADOO2,			//	屠怒滅の一撃声２
	SE_DEATH,			//	死亡声
};

#define OBJECT_SIZE_PLAYER				0.007f			//	プレイヤーサイズ
#define PLAYER_SPEED_NORMAL				0.3f			//	通常移動スピード
#define PLAYER_SPEED_NORMAL_JUMP		0.3f			//	通常ジャンプ中移動スピード

#define PLAYER_SPEED_EMERGENCY			0.7f			//	緊急回避の移動量
#define PLAYER_SPEED_POWER				0.1f			//	溜め攻撃の移動量
#define PLAYER_DEATH_HEIGHT				10.0f			//	落下して死亡する高さ
#define PLAYER_STICK_BIAS				0.001f			//	アナログスティック調整値
#define PLAYER_JUMP_HEIGHT				1.0f			//	通常ジャンプする高さ
#define PLAYER_JUMP2D_HEIGHT			1.25f			//	2D移動中のジャンプする高さ
#define PLAYER_COMBO_MOVE				0.05f			//	コンボ中の移動量

#define PLAYER_SP_STORE_TIME			(60*3)			//	屠怒滅の一撃のタメ時間
#define PLAYER_SP_RELEASE_TIME			(60*3)			//	屠怒滅の一撃の放射時間

//	巨人戦
#define PLAYER_SPEED_2D					0.3f			//	移動スピード
#define PLAYER_SPEED_JUMP2D				0.1f			//	ジャンプ中の移動量
#define PLAYER_MOVE_2D_EULER			(EULER*0.2f)	//	移動スピード
#define PLAYER_MOVE_JUMP2D_EULER		(EULER*0.2f)	//	ジャンプ中の移動量

#define PLAYER_RIGHT_ANGLE				(EULER*89.9f)	//	ほぼ直角

#define PLAYER_RANGE_MAX				3				//	距離のパターン数
#define PLAYER_RANGE_SHORT				305.1f			//	中心からの距離(近)
#define PLAYER_RANGE_CENTER				312.0f			//	中央値
#define PLAYER_RANGE_LONG				320.0f			//	中心からの距離(遠)
#define PLAYER_SIDE_MOVE				0.04f			//	初期横移動量
#define PLAYER_SIDE_MOVE_INC			0.08f			//	横移動増加量
#define PLAYER_SIDE_MOVE_MAX			5.0f			//	最大横移動量

//	ステップ番号用
enum
{
	P_STEP_R_SHORT,		//	近
	P_STEP_R_CENTER,	//	中
	P_STEP_R_LONG,		//	遠
};

//*****************************************************************************************************************************
//
//		プレイヤークラス
//
//*****************************************************************************************************************************
class c_Player : public BaseOBJ
{
private:
	iexSound *m_sound;			//	SE
	int hit_result;				//	判定結果
	int RangeStep;				//	距離別ステップ
	float SideMove;				//	円運動中の横移動量
	float idRange;				//	理想の円運動半径
	float CurrentRange;			//	現在の円運動半径
	bool m_bMoved;				//	移動完了フラグ
	iex2DObj* m_special;		//	トドメ
public:
	c_Player();					//	コンストラクタ
	c_Player(char* name);		//	引数付きコンストラクタ
	~c_Player();				//	デストラクタ

	void Update();				//	更新
	void Render();				//	描画
	void Render2D();			//	インターフェース描画

	void ChangeState(int state);//	行動モード切り替え

	/* 行動モード */
	void Normal();				//	通常モード
	void Jump();				//	ジャンプ
	void InAirMove();			//	空中移動
	void EmergencyAvoidance();	//	緊急回避
	void Combo();				//	コンボ
	void Power();				//	溜め攻撃
	void Hadoo();				//	屠怒滅の一撃
	void Fall();				//	落下
	void Damage();				//	被ダメージ
	void Death();				//	死亡
	//	2Dモード時
	void Move2D();				//	敵を中心として円(2Dカメラ)移動
	void Jump2D();				//	円(2Dカメラ)移動中のジャンプ

	//	ゲッター
	int GetAttackID(){ return m_AttackID; }		//	攻撃判定ID
	Vector3 GetHeight();		//	現在の足元の座標を返す


};

#endif	//	_PLAYER_H_
#ifndef _BASE_OBJ_H_
#define _BASE_OBJ_H_

#include	"Collision.h"

//*****************************************************************************
//
//		キャラ基本クラス
//
//*****************************************************************************

//-----------------------------------
//	定数
//-----------------------------------
#define GRAVITY					0.05f		//	重力値
#define GRAVITY_MAX				-3.0f		//	重力最大値
#define WEAPON_SIZE_BASIC		1.0f		//	武器モデルの標準サイズ

class BaseOBJ{
private:

protected:
	iex3DObj* m_lpObj;			//	キャラクターメッシュ
	int		m_state;			//	行動モード
	int		m_step;				//	行動ステップ
	Vector3 m_Pos;				//	座標
	Vector3	m_Move;				//	移動量
	float	m_angle;			//	方向
	float	m_scale;			//	モデルの大きさ
	float	m_gravity;			//	Y軸スピード
	int		m_AttackID;			//	攻撃判定ＩＤ
	int		m_DamageID;			//	やられ判定ＩＤ
	int		m_HP;				//	体力

public:

	BaseOBJ();											//	コンストラクタ
	BaseOBJ(char *name);								//	引数付きコンストラクタ
	~BaseOBJ();											//	デストラクタ

	void Update();										//	更新	
	void Render();										//	描画
	void Render( char* techname );						//	テクニック指定描画

	void SetMotion( int n );							//	モーション設定(モーション番号、補間する時間)
	bool GroundCheck();									//	地面チェック(地面ならtrue,空中ならfalse)
	void YMove();										//	Y軸移動有効化
	void ChangeState( int state );						//	行動モード切り替え

	void WeaponSet(iex3DObj* lpWeapon,int BoneNum);		//	武器セット(IEMファイル版)
	void WeaponSet(iexMesh* lpWeapon,int BoneNum);		//	武器セット(Xファイル版)
	Vector3 GetBonePos(int n, int d);					//	ボーン座標取得（番号、長さ）
	Vector3 GetBonePos(int n);							//	ボーン座標取得（番号）

	//	セッター
	void SetPos( Vector3 &p ){ m_Pos = p; }				//	座標設定
	void SetAngle( float Angle ){ m_angle = Angle; }	//	アングル設定
	void SetScale( float Scale ){ m_scale = Scale; }	//	スケール設定

	void SetState( int state ){ m_state = state; }

	//	ゲッター
	Vector3& GetPos(){ return m_Pos; }					//	座標取得
	Vector3& GetMove(){ return m_Move; }				//	移動量取得
	float GetAngle(){ return m_angle; }					//	アングル取得
	float GetScale(){ return m_scale; }					//	スケール取得
	Matrix GetMatrix(){ return m_lpObj->TransMatrix; }	//	変換行列取得
	Matrix GetBoneMatrix( int n );						//	ボーン行列取得(n:ボーン番号)
	

};

#endif	//	_BASE_OBJ_H_
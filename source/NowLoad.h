#ifndef __NOWLOAD_H__
#define __NOWLOAD_H__

#include "ThreadObject.h"
#include "Fade.h"

class cNowLoad : public cThreadObject
{
private:
	iex2DObj*	_obj;
	iexMesh* tp;
	float tpAngle;
	Vector3		m_Pos;
	c_Fade *fade;		//	フェードIOクラス
public:
	cNowLoad();
	~cNowLoad();

	void Exec();
	void Render();

	void RiseUpParticle( Vector3 &pos );
};

#endif // __NOWLOAD_H__
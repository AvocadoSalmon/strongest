#ifndef _FADE_H_
#define _FADE_H_


#define MODE_FADE_RESET		-1
#define MODE_FADE_IN		0
#define MODE_FADE_OUT		1

#define FADE_ALPHA_SHIFT	24
#define FADE_MAX_COLOR		255

#define FADE_VALUE			5		//	アルファの増減値

class c_Fade
{
private:
	int m_mode;		//	0->イン 1->アウト
	int m_current;	//	現在の値
	int	m_step;		//	ステップ処理用
	bool m_bDone;	//	フェード完了フラグ
	DWORD m_color;	//	色
public:
	c_Fade();
	~c_Fade();

	void Update();
	void Render();

	void FadeIn();
	void FadeOut();
	
	bool GetDone(){ return m_bDone; }	//	処理完了したかどうか調べる
	
	void SetColor(DWORD c){  m_color = c; }
	void SetMode(int m){  m_step = 0; m_mode = m; }

};


#endif	//	_FADE_H_
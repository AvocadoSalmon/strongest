#include	"iextreme.h"
#include	"system/system.h"
#include	"Player.h"
#include	"Camera.h"
#include	"Collision.h"


//*****************************************************************************************************************************
//
//
//
//*****************************************************************************************************************************



//*****************************************************************************************************************************
//
//
//
//*****************************************************************************************************************************
//--------------------------------------------
//	コンストラクタ
//--------------------------------------------
c_Camera::c_Camera()
{
	//	ビュー設定
	m_view = new iexView();
	m_view->SetProjection( D3DX_PI/4, 0.1f, 1200.0f );
	m_view->Set( .0f, 2.0f, -10.0f, .0f, .0f, .0f );

	m_Pos		 = Vector3( 0,0,0 );
	m_IdealPos = Vector3( 0,0,0 );
	m_Move	 = Vector3( 0,0,0 );
	m_Target	 = Vector3( 0,0,0 );

	m_mode	= CMODE_GIANT_2D;
	m_angle = 0.0f;


}

//--------------------------------------------
//	デストラクタ
//--------------------------------------------
c_Camera::~c_Camera()
{
	DEL( m_view );
}

//*****************************************************************************************************************************
//
//
//
//*****************************************************************************************************************************


//*****************************************************************************************************************************
//
//		主処理
//
//*****************************************************************************************************************************
void c_Camera::Update( c_Player *p )
{
	//	カメラモード切り替え
	switch( m_mode )
	{
		case CMODE_GIANT_CHASE:		//	巨人戦用追跡カメラ
			ChaseCamera( p );
			break;
		case CMODE_GIANT_2D:		//	巨人戦用2Dカメラ
			SideCamera( p );
			break;
		case CMODE_GIANT_FIGHT:		//	巨人戦用戦闘カメラ
			GiantCamera( p );
			break;
		case CMODE_NORMAL:			//	通常カメラ
			NormalCamera( p );
			break;
		case CMODE_TITLE:			//	タイトルカメラ
			TitleCamera( p );
			break;
	}
}

void c_Camera::Update()
{
	m_view->Set( m_Pos, m_Target );
	shader->SetValue( "ViewPos", &m_Pos );
}

//---------------------------------------
//	描画開始
//---------------------------------------
void c_Camera::Begin()
{
	//	ビュー設定
	m_view->Set( m_Pos, m_Target );
	m_view->Activate();
	m_view->Clear();
}

//*****************************************************************************************************************************
//
//		処理
//
//*****************************************************************************************************************************

//----------------------------------------------------------
//	機能：ビューポート設定
//----------------------------------------------------------
void c_Camera::SetViewPort(int x, int y, int w, int h)
{
	m_view->SetViewport( x, y, w, h );
}

//----------------------------------------------------------
//	機能：投影設定
//----------------------------------------------------------
void c_Camera::SetProjection(float fovY, float Near, float Far, float Asp)
{
	m_view->SetProjection( fovY, Near, Far, Asp );
}

//----------------------------------------------------------
//	引数：プレイヤークラス
//	機能：巨人戦用追跡カメラ
//----------------------------------------------------------
void c_Camera::ChaseCamera( c_Player *p )
{
	Vector3 ZeroPoint(0,0,0);	//	ボス座標
	Vector3 ppos;				//	ターゲット座標

	//	カメラ位置
	m_Pos.y = p->GetPos().y + 4.0f;
	m_Pos.x = p->GetPos().x + sinf( p->GetAngle() + EULER*180 ) * CAMERA_GIANT_XZPOS_BIAS;
	m_Pos.z = p->GetPos().z + cosf( p->GetAngle() + EULER*180 ) * CAMERA_GIANT_XZPOS_BIAS;

	//	プレイヤーよりやや前の座標	
	ppos.x = p->GetPos().x + sinf( p->GetAngle() ) * CAMERA_GIANT_XZTARGET_BIAS; 
	ppos.z = p->GetPos().z + cosf( p->GetAngle() ) * CAMERA_GIANT_XZTARGET_BIAS; 
	ppos.y = p->GetPos().y + CAMERA_GIANT_YTARGET_BIAS; 

	/* 一人称視点 */
	//m_Pos.y = p->GetPos().y + 3.3f;
	//m_Pos.x = p->GetPos().x + sinf( p->GetAngle() ) * 1.0f;
	//m_Pos.z = p->GetPos().z + cosf( p->GetAngle() ) * 1.0f;

	//ppos.x = p->GetPos().x + sinf( p->GetAngle() ) * 30.0f; 
	//ppos.z = p->GetPos().z + cosf( p->GetAngle() ) * 30.0f; 
	//ppos.y = p->GetPos().y + 4.0f; 

	SetPos( m_Pos );
	SetTarget( ppos );
	m_view->Set( m_Pos, m_Target );
	//shader->SetValue( "ViewPos", &m_Pos );
}

//----------------------------------------------------------
//	引数：プレイヤークラス
//	機能：巨人戦用２Ｄカメラ
//----------------------------------------------------------
void c_Camera::SideCamera( c_Player *p )
{
	Vector3 ZeroPoint(0,0,0);	//	ボス座標

	//	カメラ位置
	m_Pos.y = p->GetPos().y + CAMERA_GIANT_YPOS_BIAS;
	m_Pos.x = p->GetPos().x + sinf( p->GetAngle() + CAMERA_GIANT_ANGLE_BIAS ) * CAMERA_GIANT_XZPOS_BIAS;
	m_Pos.z = p->GetPos().z + cosf( p->GetAngle() + CAMERA_GIANT_ANGLE_BIAS ) * CAMERA_GIANT_XZPOS_BIAS;

	//	プレイヤーよりやや前の座標
	Vector3 ppos;
	ppos.x = p->GetPos().x + sinf( p->GetAngle() ) * CAMERA_GIANT_XZTARGET_BIAS; 
	ppos.z = p->GetPos().z + cosf( p->GetAngle() ) * CAMERA_GIANT_XZTARGET_BIAS; 
	ppos.y = p->GetPos().y + CAMERA_GIANT_YTARGET_BIAS; 

	SetTarget( Vector3( ppos.x, ppos.y, ppos.z ) );
	m_view->Set( m_Pos, m_Target );
	shader->SetValue( "ViewPos", &m_Pos );
}

//----------------------------------------------------------
//	引数：プレイヤークラス
//	機能：巨人戦用戦闘カメラ
//----------------------------------------------------------
void c_Camera::GiantCamera(c_Player *p)
{
	Vector3 BossPos(0,420,0);	//	ボス座標

	//	ボスへのベクトル算出
	float dx = m_Pos.x - BossPos.x;
	float dz = m_Pos.z - BossPos.z;
	float dy = m_Pos.y - BossPos.y;
	float d = sqrtf( dx*dx + dy*dy + dz*dz );
	dx /= d; dy /= d;  dz /= d;	//	正規化
	float angle = atan2f( dx , dz );

	//	カメラ位置
	m_Pos.y = p->GetPos().y + CAMERA_YPOS_BIAS;
	m_Pos.x = p->GetPos().x + sinf( angle ) * CAMERA_GIANT_XZPOS_BIAS;
	m_Pos.z = p->GetPos().z + cosf( angle ) * CAMERA_GIANT_XZPOS_BIAS;


	SetTarget( BossPos );
	m_view->Set( m_Pos, m_Target );
	shader->SetValue( "ViewPos", &m_Pos );

}

//----------------------------------------------------------
//	引数：プレイヤークラス
//	機能：通常カメラ
//----------------------------------------------------------
void c_Camera::NormalCamera( c_Player *p )
{
	//	プレイヤーとの距離
	float dx = m_Pos.x - p->GetPos().x;
	float dz = m_Pos.z - p->GetPos().z;
	float dy = m_Pos.y - p->GetPos().y;
	float d = sqrtf( dx*dx + dy*dy + dz*dz );
	dx /= d; dy /= d;  dz /= d;	//	正規化
	float angle = atan2f( dx , dz );

	//	理想位置の決定
	m_IdealPos.x = p->GetPos().x + dx * CAMERA_CHASE_FAR;
	m_IdealPos.z = p->GetPos().z + dz * CAMERA_CHASE_FAR;
	m_IdealPos.y = p->GetPos().y + CAMERA_YPOS_BIAS;

	Vector3 dd = m_IdealPos - m_Pos;

	//	プレイヤーと一定距離を保つ
	if( d <= CAMERA_CHASE_NEAR ){
		m_Pos.x = p->GetPos().x + sinf( angle ) * CAMERA_CHASE_NEAR_BIAS_XZ;
		m_Pos.z = p->GetPos().z + cosf( angle ) * CAMERA_CHASE_NEAR_BIAS_XZ;
		m_Pos.y += dd.y / CAMERA_CHASE_NEAR_BIAS_Y;
	}
	//	理想へ近付く
	if( d > CAMERA_CHASE_FAR ){
		m_Pos.x += dd.x / CAMERA_CHASE_FAR_BIAS_XZ;
		m_Pos.z += dd.z / CAMERA_CHASE_FAR_BIAS_XZ;
		m_Pos.y += dd.y / 64.0f;//CAMERA_CHASE_FAR_BIAS_Y;
	}

	//	入力処理
	if( KEY(KEY_L) == 1 )
	{
		angle += CAMERA_LR_ROTATE;
		m_Pos.y = p->GetPos().y + CAMERA_YPOS_BIAS;
		m_Pos.x = p->GetPos().x + sinf( angle ) * d;
		m_Pos.z = p->GetPos().z + cosf( angle ) * d;
	}
	if( KEY(KEY_R) == 1 )
	{
		angle -= CAMERA_LR_ROTATE;
		m_Pos.y = p->GetPos().y + CAMERA_YPOS_BIAS;
		m_Pos.x = p->GetPos().x + sinf( angle ) * d;
		m_Pos.z = p->GetPos().z + cosf( angle ) * d;
	}

	//	カメラからプレイヤーへのアングル算出
	float ax = p->GetPos().x - m_Pos.x;
	float az = p->GetPos().z - m_Pos.z;
	float ang = atan2f( ax , az );

	//	ターゲット設定( カメラからみてプレイヤーのずっと先をターゲットにする )
	//Vector3 Target( p->GetPos().x + sinf(ang) * CAMERA_TARGET_DIST, p->GetPos().y + CAMERA_TARGETY_BIAS, p->GetPos().z + cosf(ang) * CAMERA_TARGET_DIST );
	Vector3 Target( p->GetPos().x, p->GetPos().y + 2.0f, p->GetPos().z );
	SetTarget( Target );

	//	カメラセット
	m_view->Set( m_Pos, m_Target );
	shader->SetValue( "ViewPos", &m_Pos );

}


//----------------------------------------------------------
//	引数：プレイヤークラス
//	機能：タイトルカメラ
//----------------------------------------------------------
void c_Camera::TitleCamera( c_Player *p )
{

	Vector3 Target( p->GetPos().x, p->GetPos().y + 2.0f, p->GetPos().z );
	m_Target = Target;

	m_Pos.x = m_Target.x + sinf(m_angle) * 60.0f;
	m_Pos.y = m_Target.y + 30.0f;
	m_Pos.z = m_Target.z + cosf(m_angle) * 60.0f;

	//	カメラセット
	m_view->Set( m_Pos, m_Target );
	shader->SetValue( "ViewPos", &m_Pos );

	m_angle += 0.005f;

}
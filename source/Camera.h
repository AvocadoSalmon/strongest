#ifndef _CAMERA_H_
#define _CAMERA_H_

#include	"Player.h"

//-----------------------------------
//	定数
//-----------------------------------
enum
{
	CMODE_GIANT_CHASE,		//	巨人戦用追跡カメラ
	CMODE_GIANT_2D,			//	巨人戦用カメラ
	CMODE_GIANT_FIGHT,		//	巨人戦用戦闘カメラ
	CMODE_NORMAL,			//	通常カメラ
	CMODE_TITLE,			//	イベントカメラ
};

//------------------------------
//	巨人戦カメラ
//------------------------------
#define CAMERA_GIANT_ANGLE_BIAS		(EULER*100)		//	巨人戦カメラの方向補正値(100度)
#define CAMERA_GIANT_YPOS_BIAS		2.0f			//	巨人戦カメラのY補正値
#define CAMERA_GIANT_XZPOS_BIAS		13.0f			//	巨人戦カメラのXZ補正値

#define CAMERA_GIANT_YTARGET_BIAS	3.0f			//	巨人戦カメラのYターゲット補正値
#define CAMERA_GIANT_XZTARGET_BIAS	5.0f			//	巨人戦カメラのXZターゲット補正値

//------------------------------
//	通常カメラ
//------------------------------
#define CAMERA_YPOS_BIAS			3.0f			//	カメラのY補正値
#define CAMERA_TARGET_DIST			30.0f			//	カメラターゲットまでの距離
#define CAMERA_TARGETY_BIAS			5.0f			//	カメラターゲットまでの距離
#define CAMERA_LR_ROTATE			0.01f			//	カメラ左右回転値

#define CAMERA_CHASE_NEAR			8.0f			//	ターゲットに近づく限界距離
#define CAMERA_CHASE_NEAR_BIAS_XZ	7.6f			//	XZ方向の補正値
#define CAMERA_CHASE_NEAR_BIAS_Y	8.0f			//	Y方向の補正値
#define CAMERA_CHASE_FAR			10.0f			//	ターゲットから遠ざかる限界距離
#define CAMERA_CHASE_FAR_BIAS_XZ	64.0f			//	XZ方向の補正値
#define CAMERA_CHASE_FAR_BIAS_Y		8.0f			//	Y方向の補正値

//*****************************************************************************************************************************
//
//		カメラクラス
//
//*****************************************************************************************************************************
class c_Camera
{
private:
	iexView* m_view;					//	視界クラスの実体
	Vector3	m_Pos;						//	座標
	Vector3	m_IdealPos;					//	理想座標
	Vector3	m_Move;						//	移動量
	Vector3	m_Target;					//	ターゲット
	int	m_mode;							//	モード
	float m_NearLimit;					//	クリッピング近距離限界値
	float m_FarLimit;					//	クリッピング遠距離限界値
	float m_angle;						//	カメラアングル
public:
	c_Camera();							//	コンストラクタ
	~c_Camera();						//	デストラクタ

	void Update( c_Player *p );			//	更新(p:プレイヤークラスの実体)
	void Update();						//	更新
	void Begin();						//	描画開始

	void ChaseCamera( c_Player *p );		//	追跡カメラ
	void SideCamera( c_Player *p );		//	2D移動カメラ
	void GiantCamera( c_Player *p );	//	巨人戦カメラ
	void NormalCamera( c_Player *p );	//	通常カメラ
	void TitleCamera( c_Player *p );

	//	セッター
	void SetPos(Vector3& p){ m_Pos = p; }								//	カメラ座標設定( p:カメラ座標 )
	void SetTarget(Vector3& t){ m_Target = t; }							//	ターゲット設定( t:ターゲット座標 )
	void SetViewPort(int x, int y, int w, int h);						//	ビューポート設定( x:X座標,y:Y座標,w:幅,h:高さ )
	void SetProjection(float fovY,float Near,float Far,float Asp);		//	投影設定( fovY:画角,Near:ニアクリップ,Near:ファークリップ:Far,Asp:アスペクト比 )
	void SetMode( int m ){ m_mode = m; }								//	カメラモード設定( m:モード番号 )

	//	ゲッター
	Vector3 GetPos(){ return m_Pos; }									//	カメラ座標取得
	Vector3 GetTarget(){ return m_Target; }								//	カメラターゲット座標取得
	float GetAngle(){ return m_angle; }									//	カメラアングル取得
	int GetMode(){ return m_mode; }										//	カメラモード取得

	//	シングルトン
	static c_Camera &GetInstance(){
		static c_Camera	camera;
		return camera;
	}

	
};

#define CAMERA c_Camera::GetInstance()		//	シングルトンのマクロ

#endif //	_CAMERA_H_
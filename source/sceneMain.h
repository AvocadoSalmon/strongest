#ifndef _SCENE_MAIN_H_
#define _SCENE_MAIN_H_

#include	"Camera.h"
#include	"Player.h"
#include	"Enemy.h"
#include	"TEST\\TESTCollision.h"
#include	"Fade.h"

#define		BARRIER_MAX		39

//*****************************************************************************************************************************
//
//	メインモードクラス
//
//*****************************************************************************************************************************

class	sceneMain : public Scene
{
private:
	LPIEXMESH	lpMesh;			//	ステージオブジェクト
	c_Player *m_player;			//	プレイヤークラス
	c_EnemyManager *m_EnMng;	//	エネミークラス
	c_Fade *fade;				//	フェードクラス
	int	m_BarrierID[BARRIER_MAX];		//	障害物当たり判定ＩＤ
	Vector3 m_BarrierPos[BARRIER_MAX];	//	障害物の座標	
public:
	sceneMain();				//	コンストラクタ
	~sceneMain();				//	デストラクタ

	bool Initialize();			//	初期化
	void Update();				//	更新
	void Render();				//	描画

};

#endif	//	_SCENE_MAIN_H_


#include	"iextreme.h"
#include	"system/system.h"
#include	"system/Framework.h"
#include	"sceneMain.h"

#include	"TEST\\EffectManager.h"
#include	"TEST\\SoundManager.h"
#include	"TEST\\Particle.h"

#include	"ThreadObject.h"
#include	"NowLoad.h"




//*****************************************************************************************************************************
//
//
//
//*****************************************************************************************************************************



//c_Collision* collision;		//	コリジョンクラス
//Collision*	TESTCollision;	//	TESTコリジョンクラス
EffectManager* glEffectManager = NULL;

//*****************************************************************************************************************************
//
//
//
//*****************************************************************************************************************************


LPDSSTREAM	lpStream;

sceneMain::sceneMain()
{
	lpMesh		 = NULL;
	m_player	 = NULL;
	m_EnMng		 = NULL;
	collision	 = NULL;
	TESTCollision  = NULL;
	fade		 = NULL;

	for(int i=0;i<BARRIER_MAX;i++)
	{
		m_BarrierID[i]	 = -1;
	}

}

sceneMain::~sceneMain()
{
	DEL( m_player );
	DEL( m_EnMng );
	DEL( lpMesh );
	DEL( collision );
	DEL( TESTCollision );
	DEL( fade );
	delete glEffectManager;
	glEffectManager = NULL;
}

//*********************************
//	初期化
//*********************************
bool sceneMain::Initialize()
{
	//	スレッド作成
	cNowLoad *load = new cNowLoad();
	cThread *th	   = new cThread( load );
	th->Start();

	if(glEffectManager==NULL){
		glEffectManager = new EffectManager();
	}

	//	フェードイン・アウト
	fade = new c_Fade();
	fade->SetColor( 0x000000 );		//	黒色に設定
	fade->SetMode( MODE_FADE_IN );	//	フェードイン

	//	ステージ読み込み
	lpMesh = new iexMesh("DATA\\BG\\stage01\\stage01\\stage01.imo");
	lpMesh->SetScale(1.0f);
	//	コリジョン
	collision = new c_Collision();
	collision->Load( "DATA\\BG\\stage01\\Hit01\\Hit03.imo" );
	//	TESTコリジョン
	TESTCollision = new Collision();
	TESTCollision->Initialize();		//	デバッグメッシュの読み込み
	TESTCollision->LoadWallCollisionMesh( "DATA\\AWall.imo" );
	damageCollisionID = -1;

	//	プレイヤー読み込み
	m_player = new c_Player( "DATA\\CHR\\player\\player.IEM" );

	//	エネミー読み込み
	m_EnMng = new c_EnemyManager();

	//	障害物当たり判定作成
	for(int i=0;i<BARRIER_MAX;i++)
	{
		m_BarrierID[i] = TESTCollision->CreateCollision( new HitObjOBB( MASTERKIND_ENEMY, ATKKIND_PANCH, 0 ) );
	}

	//	障害物座標初期化(全39個:[0]〜[14]=柱、[15]〜[26])
	//	柱
	m_BarrierPos[0]  = Vector3( -222.0f, 25.0f, -210.27f );
	m_BarrierPos[1]  = Vector3( -10.20f, 45.64f,  -306.32f );
	m_BarrierPos[2]  = Vector3( 209.71f, 70.78f,  -223.51f );
	m_BarrierPos[3]  = Vector3( 304.73f, 95.30f,  -10.30f );
	m_BarrierPos[4]  = Vector3( 223.14f, 120.49f,  207.88f );
	m_BarrierPos[5]  = Vector3( 9.23f,   145.73f,  304.19f );
	m_BarrierPos[6]  = Vector3( -208.21f, 170.0f, 220.84f );
	m_BarrierPos[7]  = Vector3( -304.90f, 195.33f, 7.72f );
	m_BarrierPos[8]  = Vector3( -222.82f, 219.97f, -210.15f );
	m_BarrierPos[9]  = Vector3( -10.8f, 244.33f, -306.61f );
	m_BarrierPos[10] = Vector3( 209.62f, 269.51f, -223.78f );
	m_BarrierPos[11] = Vector3( 304.62f, 294.26f, -10.44f );
	m_BarrierPos[12] = Vector3( 223.26f, 319.18f, 207.93f );
	m_BarrierPos[13] = Vector3( 9.12f, 343.80f, 303.90f );
	m_BarrierPos[14] = Vector3( -208.70f, 368.97f, 220.32f );
	//	岩
	m_BarrierPos[15] = Vector3( -131.38f, 32.78f, -283.99f );
	m_BarrierPos[16] = Vector3( 283.15f, 82.29f, -135.50f );
	m_BarrierPos[17] = Vector3( 312.80f, 95.41f, -10.48f );
	m_BarrierPos[18] = Vector3( 250.54f, 116.95f, 185.55f );
	m_BarrierPos[19] = Vector3( -96.01f, 156.40f, 295.72f );
	m_BarrierPos[20] = Vector3( -261.22f, 177.66f, 168.42f );
	m_BarrierPos[21] = Vector3( -279.50f, 210.84f, -143.69f );
	m_BarrierPos[22] = Vector3( -63.49f, 238.94f, -308.22f );
	m_BarrierPos[23] = Vector3( 213.62f, 269.49f, -228.14f );
	m_BarrierPos[24] = Vector3( 305.52f, 303.43f, 76.00f );
	m_BarrierPos[25] = Vector3( 65.52f, 338.33f, 303.93f );
	m_BarrierPos[26] = Vector3( -185.49f, 364.89f, 251.02f );
	//	石像（直立）
	m_BarrierPos[27] = Vector3( 132.64f, 60.16f, -291.0f );
	m_BarrierPos[28] = Vector3( 312.41f, 89.80f, -65.78f );
	m_BarrierPos[29] = Vector3( 288.75f, 110.70f, 138.97f );
	m_BarrierPos[30] = Vector3( 118.23f, 134.40f, 296.74f );
	m_BarrierPos[31] = Vector3( -276.17f, 212.57f, -163.14f );
	m_BarrierPos[32] = Vector3( 138.23f, 331.18f, 286.92f );
	m_BarrierPos[33] = Vector3( 0.27f, 345.12f, 319.22f );
	m_BarrierPos[34] = Vector3( -189.63f, 364.83f, 257.65f );
	//	石像（倒壊）
	m_BarrierPos[35] = Vector3( -163.08f, 164.15f, 266.55f );
	m_BarrierPos[36] = Vector3( 52.83f, 250.80f, -310.78f );
	m_BarrierPos[37] = Vector3( 276.08f, 279.81f, -152.71f );
	m_BarrierPos[38] = Vector3( 245.12f, 317.25f, 198.36f );

	//	環境設定
	iexLight::SetAmbient(0x808080);
	iexLight::SetFog( 1200, 1200, 0 );

	//	ライト方向設定
	Vector3 dir( 0.5f, -0.5f, 0.5f );
	iexLight::DirLight( shader, 0, &dir, 0.65f, 0.6f, 1.0f );

	//	カメラ設定
	CAMERA.SetPos( Vector3( -311.0f, 0.0f, -14.5f ) );

	th->End();	//	スレッドの破棄
	delete th;
	delete load;

	Particle::LoadParticleTex("DATA\\particle.png");

	return true;

}

//*****************************************************************************************************************************
//
//
//
//*****************************************************************************************************************************



//*****************************************************************************************************************************
//
//		主処理
//
//*****************************************************************************************************************************

void	sceneMain::Update()
{
	CAMERA.SetViewPort( 0, 0, 1280, 720 );	//	ビューポートリセット

	//	障害物当たり判定
	D3DXMATRIX matPillar;
	D3DXMatrixIdentity( &matPillar );

	for(int i=0;i<BARRIER_MAX;i++)	//	柱の本数分回す
	{
		if( m_BarrierID[i] == -1 )continue;	//	消えたら飛ばす 

		matPillar._41 = m_BarrierPos[i].x;
		matPillar._42 = m_BarrierPos[i].y;
		matPillar._43 = m_BarrierPos[i].z;

		if( i < 15 )TESTCollision->UpdateBox( m_BarrierID[i], &matPillar, &Vector3( 2.5f,30.0f,2.5f ), NULL, false );
		else if( i < 27 )TESTCollision->UpdateBox( m_BarrierID[i], &matPillar, &Vector3( 6.0f,40.0f,6.0f ), NULL, false );
		else if( i < 35 )TESTCollision->UpdateBox( m_BarrierID[i], &matPillar, &Vector3( 4.0f,50.0f,4.0f ), NULL, false );
		else TESTCollision->UpdateBox( m_BarrierID[i], &matPillar, &Vector3( 8.0f,5.0f,8.0f ), NULL, false );
		
		if( m_player->GetPos().y == 394.0f )TESTCollision->DestroyHitObj( &m_BarrierID[i] );
	}



	fade->Update();		//	画面エフェクト更新

	m_player->Update();

	m_EnMng->Update( m_player->GetHeight() );

	Particle::Update();
	glEffectManager->PlayEffect();
	glEffectManager->Update();

	SoundManager::GetInstance()->Update();

	

	//	カメラ更新
	CAMERA.Update( m_player );

	if( bOver )ChangeScene( SCENE_TITLE );		//	タイトルモードへ

}

//*****************************************************************************************************************************
//
//		描画関連
//
//*****************************************************************************************************************************
void	sceneMain::Render( void )
{

	CAMERA.Begin();
	
	//iexSystem::GetDevice()->SetRenderState( D3DRS_FILLMODE, D3DFILL_WIREFRAME );	//	ワイヤーフレーム描画

	lpMesh->Render( shader, "lcopy_fx" );
	//lpMesh->Render();

	m_player->Render();

	m_EnMng->Render();

	//TESTCollision->Draw();

	//collision->Render();
	
	//iexSystem::GetDevice()->SetRenderState( D3DRS_FILLMODE, D3DFILL_SOLID );		//	元に戻す

	Particle::Draw(shader);
	glEffectManager->Draw(shader);

	m_player->Render2D();

	m_EnMng->Render2D();

	fade->Render();


}


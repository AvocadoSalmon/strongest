#include	"iextreme.h"
#include	"system/system.h"
#include	"system/Framework.h"
#include	"sceneTitle.h"
#include	"sceneMain.h"
#include	"Camera.h"
#include	"Player.h"
#include	"TEST\\TESTCollision.h"
#include	"TEST\\Particle.h"
#include	"TEST\\SoundManager.h"

//*****************************************************************************************************************************
//
//
//
//*****************************************************************************************************************************

LPIEXMESH	lpMesh;

c_Collision* collision;		//	コリジョンクラス
Collision*	TESTCollision;	//	TESTコリジョンクラス
extern EffectManager* glEffectManager;
int	glBgmNum = -1;


int	damageCollisionID;

//*****************************************************************************************************************************
//
//
//
//*****************************************************************************************************************************

sceneTitle::sceneTitle()
{
	lpMesh		 = NULL;
	m_player	 = NULL;
	collision	 = NULL;
	TESTCollision  = NULL;
	fade		 = NULL;
	m_bchange	 = false;
	m_bshowTitle = true;
	m_timer      = 90;
	lpPortal	 = NULL;
	lpStage      = NULL;
	m_effectManager = NULL;
	m_titleRogo  = NULL;

	m_bgm  = -1;
}

sceneTitle::~sceneTitle()
{
	DEL( lpMesh );
	DEL( m_player );
	DEL( collision );
	DEL( TESTCollision );
	DEL( fade );
	delete lpPortal;
	lpPortal = NULL;
	delete lpStage;
	lpStage  = NULL;
	delete m_titleRogo;
	m_titleRogo = NULL;

	m_effectManager->DeleteEffect(123456);
	m_effectManager->DeleteEffect(987654);
	m_effectManager->DeleteEffect(159753);

	delete m_effectManager;
	m_effectManager = NULL;

	

	//delete glEffectManager;
	//glEffectManager = NULL;

}

//*********************************
//	初期化
//*********************************
bool sceneTitle::Initialize()
{
	m_bchange = false;	//	シーン移行フラグ

	bTitleEnter = false;

	Particle::LoadParticleTex("DATA\\particle.png");

	//	環境設定
	iexLight::SetAmbient(0x808080);
	iexLight::SetFog( 1200, 1200, 0 );

	Vector3 dir( -0.0f, -1.0f, 0.5f );
	iexLight::DirLight( shader, 0, &dir, 0.65f, 0.6f, 0.5f );

	m_titleRogo = new iex2DObj("DATA\\UI\\UI.png");

	//	ステージ読み込みlpStage
/*
	lpMesh = new iexMesh("DATA\\Portal_Stage\\Portal_stage.x");
	lpMesh->SetScale(1.0f);
	lpMesh->SetPos(0,0,0);
	lpMesh->Update();
*/
	lpStage = new iexMesh("DATA\\Portal_Stage\\Portal_stage.imo");
	lpStage->SetScale(1.00f);
	lpStage->SetPos(0,0,0);
	lpStage->Update();

	lpPortal = new iexMesh("DATA\\Portal\\portal_dodai.x");
	lpPortal->SetScale(0.01f);
	lpPortal->SetPos(0,0,-135.0f);
	lpPortal->Update();



	//	コリジョン
	collision = new c_Collision();
	collision->Load( "DATA\\Portal_Stage\\Portal_Stage_Wall.imo" );
	//collision->Load( "DATA\\BG\\stage01\\Hit02\\Hit02.imo" );
	//collision->Load( "DATA\\BG\\stage01\\stage01\\stage01_.imo" );

	//	TESTコリジョン
	TESTCollision = new Collision();
	TESTCollision->Initialize();		//	デバッグメッシュの読み込み
	damageCollisionID = -1;

	//	プレイヤー読み込み
	m_player = new c_Player( "DATA\\CHR\\player\\player.IEM" );
	m_player->SetPos(Vector3(0,0,0));
	m_player->SetAngle(D3DX_PI);
	m_player->SetState(PMODE_NORMAL);

	//	フェードイン・アウト
	fade = new c_Fade();
	fade->SetColor( 0x000000 );		//	黒色に設定
	fade->SetMode( MODE_FADE_IN );	//	フェードイン

	//	カメラモード設定
	CAMERA.SetMode( CMODE_TITLE );

	//	エフェクト
	m_effectManager = new EffectManager();
	m_effectManager->SetEffect(EFFECTSETKIND_PORTAL,Vector3(0,0,-135.0f),0,0.01f);
	//m_effectManager->SetEffect(EFFECTSETKIND_DOTOU, Vector3(0,2.0f,0),0,0.02f);
	//m_effectManager->SetEffect(EFFECTSETKIND_GODOTOU, Vector3(0,2.0f,0),0,0.01f);
	//m_effectManager->SetEffect(EFFECTSETKIND_AURA, Vector3(0,0.0f,0),0,0.01f);

	glEffectManager = new EffectManager();
	//SE_COMBO1
	SoundManager::GetInstance()->Destroy(glBgmNum);
	glBgmNum = SoundManager::GetInstance()->Play("DATA\\BGM\\BGM_1.wav",255,true);


	return true;
}

//*****************************************************************************************************************************
//
//
//
//*****************************************************************************************************************************



//*****************************************************************************************************************************
//
//		主処理
//
//*****************************************************************************************************************************

void	sceneTitle::Update()
{
	if(m_timer>0)m_player->BaseOBJ::Update();
	else m_player->Update();

	//m_effectManager->SetPos(159753,m_player->GetPos());

	m_effectManager->PlayEffect();
	Particle::Update();
	m_effectManager->Update();

	glEffectManager->PlayEffect();
	glEffectManager->Update();


	if( fade )fade->Update();


	if(KEY(KEY_ENTER) == 3)
	{
		bTitleEnter = true;
		m_bshowTitle = false;
		CAMERA.SetMode( CMODE_NORMAL );
		//m_effectManager->DeleteGoDotou();
		//m_effectManager->SetPos(123456,Vector3(3,2,0));
	}

	if(m_bshowTitle==false)m_timer--;

	//	イベントモードへ移行
	Vector3 vvv = m_player->GetPos() - Vector3(0,0,-135.0f);
	if(  vvv.Length()<=3.0f )
	{
		fade->SetMode( MODE_FADE_OUT );		//	フェードアウト
		m_bchange = true;					//	シーン移行
	}
	if( m_bchange )	//	移行フラグON
	{
		if( fade->GetDone() )
		{
			//ChangeScene( SCENE_MAIN );		//	メインモードへ
			ChangeScene( SCENE_EVENT );		//	イベントモードへ
			//ChangeScene( SCENE_LOAD );
		}
	}
	else
	{
		//	カメラ更新
		CAMERA.Update( m_player );
	}

	SoundManager::GetInstance()->Update();
}

//*****************************************************************************************************************************
//
//		描画関連
//
//*****************************************************************************************************************************
void	sceneTitle::Render( void )
{
	CAMERA.Begin();
	
	//iexSystem::GetDevice()->SetRenderState( D3DRS_FILLMODE, D3DFILL_WIREFRAME );	//	ワイヤーフレーム描画

	//lpMesh->Render( shader, "lcopy_fx" );
	//lpMesh->Render();
	lpStage->Render(shader,"copy_fx2");
	lpPortal->Render();

	m_player->Render();

	//collision->Render();
	
	//iexSystem::GetDevice()->SetRenderState( D3DRS_FILLMODE, D3DFILL_SOLID );		//	元に戻す

	Particle::Draw(shader);
	m_effectManager->Draw(shader);
	glEffectManager->Draw(shader);

	int hen = (90 - m_timer)*3;
	if(m_timer>0){
		m_titleRogo->Render(320-hen,30+hen,640+hen*2,360-hen*2,0,0,1024,512);
	}

	fade->Render();
}
#include	"iextreme.h"
#include	"system/system.h"

#include	"threadobject.h"

DWORD WINAPI ThreadMain( LPVOID Param );
//================================================================================//
//
//		CLoadDisp
//
//================================================================================//

cThreadObject::cThreadObject()
{
	_State = -1;
	_bRender = false;
	_fFrameTime = 0;
	//_Object = NEW( C2DObject( FileName ) );
}

cThreadObject::~cThreadObject()
{
	//SAFE_DELETE( _Object );
}

bool cThreadObject::CheckFrameRate()
{
	float	dwNow;
	//	フレーム制限
	dwNow = (float)(timeGetTime()&0xffffff);
	if(dwNow < _fFrameTime-1000) _fFrameTime -= 0xffffff;

	if( dwNow < _fFrameTime ) return false;
	if( dwNow > _fFrameTime+100 ) _fFrameTime = dwNow;

	//	スキップタイプ
	_bRender = true;

	_fFrameTime += 1000.0f / 60.0f;

	return true;
}

//================================================================================//
//
//		cLoadDispThread
//
//================================================================================//

cThread::cThread( cThreadObject* Object )
{
	_ThreadObject = Object;
	//strncpy_s( _Message, "NowLoading", 64);
	_Thread = CreateThread( NULL, 0, ThreadMain, (LPVOID)_ThreadObject, 0, &_ThreadId);
}

cThread::~cThread()
{

	//	ハンドルの開放
	//CloseHandle(_Thread);
}

void cThread::Start()
{
	_ThreadObject->SetState( LOAD_STATE_EXEC );
}

void cThread::End()
{
	_ThreadObject->SetState(LOAD_STATE_END);
	//	スレッド終了待ち
	while(1)
	{
		DWORD param;
		GetExitCodeThread(_Thread , &param);
		
		if(param != STILL_ACTIVE)	//	終了
		{
			//	ハンドルの開放
			CloseHandle(_Thread);
			break;	
		}
	}
}

iexView view;

DWORD WINAPI ThreadMain( LPVOID Param )
{
	cThreadObject* object = (cThreadObject*)Param;
	while(object->GetState() != LOAD_STATE_END)
	{
		
		object->Exec();

		if(!object->GetRender())continue;

		LPDIRECT3DDEVICE9	lpDevice = iexSystem::GetDevice();

		//	シーン開始												  
		lpDevice->BeginScene();
		//	ビュー設定
		view.Activate();
		//	バッファクリア
		view.Clear(0x000000);

		object->Render();

		// シーン終了
		lpDevice->EndScene();

		// バックバッファの内容をプライマリに転送
		if( FAILED( lpDevice->Present( NULL, NULL, NULL, NULL ) )){
			// リセット
			lpDevice->Reset( &iexSystem::GetPP() );
		}
	}
	return 0;
}
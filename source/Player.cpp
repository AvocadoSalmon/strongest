#include	"iextreme.h"
#include	"system/system.h"
#include	"Player.h"
#include	"Enemy.h"
#include	"Camera.h"
#include	"Collision.h"
#include	"TEST\\TESTCollision.h"
#include	"TEST\\TESTCollision.h"
#include	"TEST\\EffectManager.h"
#include	"TEST\\SoundManager.h"

//*****************************************************************************************************************************
//
//
//
//*****************************************************************************************************************************

extern EffectManager* glEffectManager;
extern int	glBgmNum;

bool bTitleEnter;	//	GAMESTARTフラグ
int width;			//	タイトルロゴの幅
bool bOver;		//	GAMEOVERフラグ

//*****************************************************************************************************************************
//
//
//
//*****************************************************************************************************************************
//--------------------------------------------
//	コンストラクタ
//--------------------------------------------
c_Player::c_Player()
{
	m_bMoved = false;
	m_lpObj = NULL;
	m_special = NULL;

	m_state = PMODE_NORMAL;
	m_Pos = Vector3( 0,0,0 );
	m_angle = 0.0f;

	m_sound = NULL;

	m_AttackID = -1;
	m_DamageID = -1;
	hit_result = -1;

	RangeStep    = 0;
	SideMove     = 0.0f;
	idRange      = 0.0f;
	CurrentRange = 0.0f;
}

c_Player::c_Player(char *name) : BaseOBJ(name)
{
	bOver = false;

	m_bMoved = false;

	width = 0;	//	ゲージの初期値

	SetMotion( PMOTION_WAIT );		//	待機モーション

	/* ２Ｄ移動 */
	m_state = PMODE_2DMOVE;
	m_Pos = Vector3( -311.0f, 0.0f, -14.5f );
	CAMERA.SetMode( CMODE_GIANT_CHASE );
	
	//CAMERA.SetMode( CMODE_GIANT_2D );

	/* 頂上 */
	//m_state = PMODE_NORMAL;
	//m_Pos = Vector3( -311.0f, 394.0f, -14.5f  );
	//CAMERA.SetMode( CMODE_GIANT_FIGHT );
	
	/*自由カメラ*/
	//m_state = PMODE_NORMAL;
	//m_Pos = Vector3( -311.0f, 0.0f, -14.5f  );
	//CAMERA.SetMode( CMODE_NORMAL );

	m_DamageID = TESTCollision->CreateCollision( new HitObjOBB( MASTERKIND_PLAYER, DAMAGECOLLISION ) );

	m_HP	= 10;		//	プレイヤーの体力
	m_angle = 0.0f;
	m_scale = OBJECT_SIZE_PLAYER;

	m_special = new iex2DObj( "DATA\\UI\\UI.png" );

	//	2D移動時のパラメータ
	RangeStep    = P_STEP_R_SHORT;
	SideMove     = PLAYER_SIDE_MOVE;
	idRange      = PLAYER_RANGE_SHORT;
	CurrentRange = PLAYER_RANGE_SHORT;

	//	SEセット
	m_sound = new iexSound();
	m_sound->Set( SE_RUN		, "DATA\\SE\\SE_RUN.wav" );				//	走りSE
	m_sound->Set( SE_JUMP		, "DATA\\SE\\SE_JUMP.wav" );			//	ジャンプSE
	m_sound->Set( SE_COMBO1		, "DATA\\SE\\SE_COMBO1.wav" );			//	コンボ１SE
	m_sound->Set( SE_COMBO2		, "DATA\\SE\\SE_COMBO2.wav" );			//	コンボ２SE
	m_sound->Set( SE_COMBO3		, "DATA\\SE\\SE_COMBO3.wav" );			//	コンボ３SE
	m_sound->Set( SE_COMBO3_2	, "DATA\\SE\\SE_COMBO3-2.wav" );		//	コンボ３ver.2SE
	m_sound->Set( SE_SEIKEN1	, "DATA\\SE\\SE_SEIKEN01.wav" );		//	コンボ３ver.2SE
	m_sound->Set( SE_SEIKEN2	, "DATA\\SE\\SE_SEIKEN02.wav" );		//	コンボ３ver.2SE
	m_sound->Set( SE_HADOO1		, "DATA\\SE\\SE_HADOO_01.wav" );		//	コンボ３ver.2SE
	m_sound->Set( SE_HADOO2		, "DATA\\SE\\SE_HADOO_02.wav" );		//	コンボ３ver.2SE
	m_sound->Set( SE_DEATH		, "DATA\\SE\\SE_DEATH.wav" );			//	死亡SE
}

//--------------------------------------------
//	デストラクタ
//--------------------------------------------
c_Player::~c_Player()
{
	DEL( m_lpObj );
	DEL( m_sound );
	DEL( m_special );
}

//*****************************************************************************************************************************
//
//
//
//*****************************************************************************************************************************


//*****************************************************************************************************************************
//
//		主処理
//
//*****************************************************************************************************************************
void c_Player::Update()
{
	//m_Move = Vector3(0,0,0);		//	滑り止め

	//*******************************
	//	行動モード
	//*******************************
	switch( m_state ){
		case PMODE_NORMAL:			//	通常モード
			Normal();
			break;
		case PMODE_JUMP:			//	ジャンプモード
			Jump();
			break;
		case PMODE_EMERGENCY:		//	緊急回避モード
			EmergencyAvoidance();
			break;
		case PMODE_COMBO:			//	コンボモード
			Combo();
			break;	
		case PMODE_POWER:			//	溜め攻撃モード
			Power();
			break;
		case PMODE_SPECIAL:			//	必殺技モード
			Hadoo();
			break;
		case PMODE_DAMAGE:			//	ダメージモード
			Damage();
			break;
		case PMODE_DEATH:			//	死亡モード
			Death();
			break;
		case PMODE_2DMOVE:			//	２Ｄ移動モード
			Move2D();
			break;
		case PMODE_2DJUMP:			//	２Ｄジャンプモード
			Jump2D();
			break;
		case PMODE_FALL:			//	落下モード
			Fall();
			break;
	}


	BaseOBJ::Update();


	if( m_HP > 0 )
	{
		/* やられ判定 */
		D3DXMATRIX mat = GetBoneMatrix( 0 ) * GetMatrix();	//	体の中心
		TESTCollision->UpdateBox( m_DamageID, &mat , &Vector3( 1.0f,1.0f,2.0f ), NULL, false );
		TESTCollision->HitCheck();
		DamageHitResult result = TESTCollision->GetHitCheckResult( MASTERKIND_PLAYER );
		hit_result = result.GetAtkCollisionAtkKind();

		//	やられ処理
		if( hit_result == ATKKIND_PANCH )			//	ボスの攻撃を受けた場合
		{
			m_Move.y = 0.5f;
			m_HP -= 10;
			ChangeState( PMODE_DEATH );		//	死亡モードへ
			glEffectManager->DeleteEffect(159753);
			
		}
		else if( hit_result == ATKKIND_BARRIER )	//	障害物に衝突した場合
		{
			m_Move.y = 0.5f;
			m_HP -= 10;
			ChangeState( PMODE_DEATH );		//	死亡モードへ
			glEffectManager->DeleteEffect(159753);
		}
	}
	else	//	死亡！
	{
		
	}

	//TESTCollision->DestroyHitObj( &m_DamageID );



	//	デバッグ処理
	#ifndef DEBUG

	//	仮やられ処理
	if( KEY(KEY_B9) == 3 )		//	1キー
	{
		m_Move.y = 0.5f;
		ChangeState( PMODE_DAMAGE );
		glEffectManager->DeleteEffect(159753);
	}

	if( KEY(KEY_SPACE) == 3 )
	{
		if( CAMERA.GetMode() != CMODE_GIANT_FIGHT )
		{
			m_state = PMODE_NORMAL;
			m_Pos = Vector3( -311.0f, 394.0f, -14.5f );
			CAMERA.SetMode( CMODE_GIANT_FIGHT );
			glEffectManager->DeleteEffect(159753);
		}
	}

	#endif

}

//*****************************************************************************************************************************
//
//		描画関連
//
//*****************************************************************************************************************************
void c_Player::Render()
{
	m_lpObj->Render( shader, "copy_fx2" );
	//m_lpObj->Render();

#ifdef _DEBUG
	//char	str[64];
	//wsprintf( str, "MODE %d\n", m_state );
	//IEX_DrawText( str, 10,40,200,20, 0xFFFF0000 );
	//char	str2[64];
	//wsprintf( str2, "STEP %d\n", m_step );
	//IEX_DrawText( str2, 10,80,200,20, 0xFFFF0000 );

	////	座標
	//char	x[64];
	//sprintf_s( x, "X %f\n", m_Pos.x );
	//IEX_DrawText( x, 10,120,200,20, 0xFFFF0000 );
	//char	y[64];
	//sprintf_s( y, "Y %f\n", m_Pos.y );
	//IEX_DrawText( y, 10,160,200,20, 0xFFFF0000 );
	//char	z[64];
	//sprintf_s( z, "Z %f\n", m_Pos.z );
	//IEX_DrawText( z, 10,200,200,20, 0xFFFF0000 );
	//char	angle[64];
	//sprintf_s( angle, "ANGLE %f\n", m_angle );
	//IEX_DrawText( angle, 10,240,200,20, 0xFFFF0000 );
	//char	ymove[64];
	//sprintf_s( ymove, "GRAVITY %f\n", m_gravity );
	//IEX_DrawText( ymove, 10,280,200,20, 0xFFFF0000 );
	//char	atr[64];
	//wsprintf( atr, "HIT %d\n", hit_result );
	//IEX_DrawText( atr, 10,320,200,20, 0xFF00FF00 );

#endif


}

void c_Player::Render2D()
{
	if( bTitleEnter )
	{
		m_special->Render( 0,550,512,192,0,512,512,192,shader2D,"copy" );
		m_special->Render( 148,550+43,width,192,512,512,width,192,shader2D,"copy" );
	}
}

//*****************************************************************************************************************************
//
//		処理
//
//*****************************************************************************************************************************

//--------------------------------------------------------------------------------
//	引数: n:モーション番号、i:補間フレーム
//	機能: 地面の高さを返す
//--------------------------------------------------------------------------------
Vector3 c_Player::GetHeight()
{
	Vector3 pos = m_Pos;
	pos.y = collision->FloorHeight( m_Pos );

	return pos;
}

//--------------------------------------------------------------------------------
//	引数: 行動モード
//	機能: 行動モードを切り替える
//--------------------------------------------------------------------------------
void c_Player::ChangeState( int state )
{
	BaseOBJ::ChangeState( state );	//	行動モード切り替え
}
//----------------------------------------------------------
//	引数：なし
//	機能：プレイヤーの通常行動
//----------------------------------------------------------
void c_Player::Normal()
{
	if( m_bClear )return;

	m_Move = Vector3(0,0,0);		//	滑り止め
	YMove();						//	Ｙ軸移動有効

	//	高所から落下で落下モードへ
	float height = collision->FloorHeight( m_Pos );
	height = m_Pos.y - height;
	if( m_gravity <= GRAVITY_MAX || height >= PLAYER_DEATH_HEIGHT )
	{
		ChangeState( PMODE_FALL );
	}

	//	初期設定
	Vector3 front(matView._13,0,matView._33);
	Vector3 right(matView._11,0,matView._31);

	//	正規化
	front.Normalize();
	right.Normalize();

	//	軸の検出
	float axisX =   KEY_GetAxisX() * PLAYER_STICK_BIAS;
	float axisY =  -KEY_GetAxisY() * PLAYER_STICK_BIAS;

	//	移動量決定
	m_Move = ( front * axisY + right * axisX ) * PLAYER_SPEED_NORMAL;

	if( m_Move.x != 0 || m_Move.z != 0 ){	//	入力あり

		float vx = m_Move.x;
		float vz = m_Move.z;
		float vx2 = sinf(m_angle);
		float vz2 = cosf(m_angle);

		//	内積による補正量
		float d = sqrtf(vx*vx + vz*vz);
		vx /= d;
		vz /= d;
		float	n = (vx*vx2 + vz*vz2);
		n = (1-n) * 2.0f;

		//	外積による方向補正
		float cross = vx * vz2 - vz * vx2;
		if( cross < 0 )m_angle -= n;
		else m_angle += n;

		
		SetMotion( PMOTION_RUN );	//	前回移動してないなら走りモーション

		//	SE再生
		if( m_lpObj->GetParam(2) == 1 )
		{
			m_sound->Play( SE_RUN );	//	足音
			m_lpObj->SetParam(2,0);
		}

	}else
	{												//	入力なし
			SetMotion( PMOTION_WAIT );					//	待機モーション
	}


	//---------------------//
	//	入力時行動			//
	//----------------------//

	//	コンボ攻撃
	if( KEY(KEY_A) == 3 )
	{
		m_sound->Play( SE_COMBO1 );
		SetMotion( PMOTION_COMBO1 );			//	コンボ攻撃モーション
		ChangeState( PMODE_COMBO );				//	コンボモードへ
	}

	//	ジャンプ
	if( KEY(KEY_B) == 3 )
	{
		m_sound->Play( SE_JUMP );				//	ジャンプSE
		SetMotion( PMOTION_JUMP );
		ChangeState( PMODE_JUMP );				//	ジャンプモードへ
	}

	//	溜め攻撃
	if( KEY(KEY_C) == 3 )
	{
		m_sound->Play( SE_SEIKEN1 );			//	正拳突き溜めSE
		SetMotion( PMOTION_POWER1 );
		ChangeState( PMODE_POWER );				//	正拳突きモードへ
		//	エフェクト設定
		glEffectManager->SetEffect(EFFECTSETKIND_AURA, m_Pos,0,0.01f);
	}

	//	緊急回避
	if( KEY(KEY_D) == 3 )
	{
		m_sound->Play( SE_JUMP );				//	ジャンプSEを流用
		SetMotion( PMOTION_EMERGENCY );
		ChangeState( PMODE_EMERGENCY );			//	緊急回避モードへ
	}

	//	屠怒滅の一撃
	if( KEY(KEY_B12) == 3 )
	{
		if( width < 320 )return;

		m_sound->Play( SE_HADOO1 );				//	屠怒滅の一撃溜めSE
		SetMotion( PMOTION_SPECIAL1 );
		ChangeState( PMODE_SPECIAL );			//	屠怒滅モードへ
		
		Vector3 ev(m_Pos.x+sinf(m_angle+D3DX_PI*0.5f),m_Pos.y+1.5f,m_Pos.z+cosf(m_angle+D3DX_PI*0.5f));
		ev.x += sinf(m_angle+D3DX_PI)*0.5f;
		ev.z += cosf(m_angle+D3DX_PI)*0.5f;
		glEffectManager->SetEffect(EFFECTSETKIND_DOTOU,ev,m_angle,0.02f);
		
	}
}

//----------------------------------------------------------
//	引数：なし
//	機能：ジャンプ中の移動入力処理
//----------------------------------------------------------
void c_Player::InAirMove()
{

	//---------------------------------------------
	//	ジャンプ時入力処理
	//---------------------------------------------

	//	初期設定
	Vector3 front(matView._13,0,matView._33);
	Vector3 right(matView._11,0,matView._31);

	//	正規化
	front.Normalize();
	right.Normalize();

	//	軸の検出
	float axisX =   KEY_GetAxisX() * PLAYER_STICK_BIAS;
	float axisY =  -KEY_GetAxisY() * PLAYER_STICK_BIAS;

	m_Move = ( front * axisY + right * axisX ) * PLAYER_SPEED_NORMAL_JUMP;

	if( m_Move.x != 0 || m_Move.z != 0 ){	//	入力あり

		float vx = m_Move.x;
		float vz = m_Move.z;
		float vx2 = sinf(m_angle);
		float vz2 = cosf(m_angle);

		//	内積による補正量
		float d = sqrtf(vx*vx + vz*vz);
		vx /= d;
		vz /= d;
		float	n = (vx*vx2 + vz*vz2);
		n = (1-n) * 2.0f;

		//	外積による方向補正
		float cross = vx * vz2 - vz * vx2;
		if( cross < 0 )m_angle -= n;
		else m_angle += n;

	}

}

//----------------------------------------------------------
//	引数：なし
//	機能：ジャンプ処理
//----------------------------------------------------------
void c_Player::Jump()
{
	Vector3 BossPos( 0,0,0 );
	Vector3 vec;

	/* 行動ステップ */
	switch( m_step )
	{
		case 0:		//	ジャンプ
			YMove();	//	Ｙ軸移動有効

			//	パラメータでＹ軸移動
			if( m_lpObj->GetParam(1) == 1 )
			{
				m_lpObj->SetParam( 1, 0 );
				m_Move.y = PLAYER_JUMP_HEIGHT;
				//SetMotion( PMOTION_FALL, 0 );
				m_step++;
			}
			break;
		case 1:		//	ジャンプ中

			YMove();	//	Ｙ軸移動有効

			InAirMove();
			// Y軸ジャンプ分を補正 //
			m_Move.y += PLAYER_JUMP_HEIGHT;

			//	高所から落下で落下モードへ
			if( m_gravity <= GRAVITY_MAX )
			{
				ChangeState( PMODE_FALL );
			}

			//	着地時処理
			if( GroundCheck() == true )
			{
				SetMotion( PMOTION_LAND );
				m_step++;
			}
			break;
		case 2:		//	着地モーション中

			m_Move.x = sinf( m_angle ) * PLAYER_SPEED_NORMAL_JUMP;
			m_Move.z = cosf( m_angle ) * PLAYER_SPEED_NORMAL_JUMP;

			if( m_lpObj->GetFrame() >= 453 )
			{
				m_Move = Vector3(0,0,0);		//	滑り止め
			}

			if( m_lpObj->GetFrame() >= 460 )
			{
				ChangeState( PMODE_NORMAL );	//	通常モードへ
			}
			break;
	}
}

//----------------------------------------------------------
//	引数：なし
//	機能：緊急回避処理
//----------------------------------------------------------
void c_Player::EmergencyAvoidance()
{
	YMove();

	//	高所から落下で落下モードへ
	float height = collision->FloorHeight( m_Pos );
	height = m_Pos.y - height;
	if( m_gravity <= GRAVITY_MAX || height >= PLAYER_DEATH_HEIGHT )
	{
		ChangeState( PMODE_FALL );
	}



	/* 行動ステップ */
	switch( m_step )
	{
		case 0:		//	ローリング

			//	移動量決定
			m_Move.x = sinf( m_angle ) * PLAYER_SPEED_EMERGENCY;
			m_Move.z = cosf( m_angle ) * PLAYER_SPEED_EMERGENCY;

			if( m_lpObj->GetFrame() >= 491 )
			{
				m_Move = Vector3( 0,0,0 );
			}

			//	次のステップ
			if( m_lpObj->GetFrame() >= 530 )
			{
				ChangeState( PMODE_NORMAL );	//	通常モードへ
			}
			break;
	}


}

//----------------------------------------------------------
//	引数：なし
//	機能：コンボ攻撃処理
//----------------------------------------------------------
void c_Player::Combo()
{
	m_Move = Vector3(0,0,0);		//	滑り止め

	D3DXMATRIX mat;

	/* 行動ステップ */
	switch( m_step )
	{
		case 0:			//	１撃目

			YMove();	//	Ｙ軸移動有効

			m_Move.x = sinf( m_angle ) * PLAYER_COMBO_MOVE;
			m_Move.z = cosf( m_angle ) * PLAYER_COMBO_MOVE;

			//	当たり判定
			if( m_lpObj->GetParam( 0 ) == 1 )
			{
				m_AttackID = TESTCollision->CreateCollision( new HitObjOBB( MASTERKIND_PLAYER, ATKKIND_PANCH, 6 ) );
				m_lpObj->SetParam(0, 0);
			}
			
			//	判定更新
			mat = GetBoneMatrix( 17 ) * GetMatrix();
			TESTCollision->UpdateBox( m_AttackID, &mat, &Vector3( 0.5f,0.5f,0.5f ), NULL, true );

			//	判定消去
			if( m_lpObj->GetParam( 0 ) == 2  )
			{
				TESTCollision->DestroyHitObj( &m_AttackID );
				m_lpObj->SetParam(0, 0);
			}	

			//	通常モードへ
			if( m_lpObj->GetFrame() >= 120 )
			{
				ChangeState( PMODE_NORMAL );
			}
			else
			{
				//	次のコンボへ
				if( m_lpObj->GetFrame() >= 87  )
				{
					if( KEY(KEY_A) == 3 )
					{
						m_lpObj->SetParam(0, 0);
						TESTCollision->DestroyHitObj( &m_AttackID );

						m_sound->Play( SE_COMBO2 );
						SetMotion( PMOTION_COMBO2 );
						m_step++;
					}
					m_Move = Vector3(0,0,0);		//	滑り止め
				}	
			}

			break;
		case 1:			//	２撃目
			YMove();	//	Ｙ軸移動有効

			m_Move = Vector3(0,0,0);		//	滑り止め

			m_Move.x = sinf( m_angle ) * PLAYER_COMBO_MOVE;
			m_Move.z = cosf( m_angle ) * PLAYER_COMBO_MOVE;

			//	当たり判定
			if( m_lpObj->GetParam( 0 ) == 1 )
			{
				m_AttackID = TESTCollision->CreateCollision( new HitObjOBB( MASTERKIND_PLAYER, ATKKIND_PANCH, 5 ) );
				m_lpObj->SetParam(0, 0);
			}

			//	判定更新
			mat = GetBoneMatrix( 5 ) * GetMatrix();
			TESTCollision->UpdateBox( m_AttackID, &mat, &Vector3( 0.5f,0.5f,0.5f ), NULL, true );

			//	判定消去
			if( m_lpObj->GetParam( 0 ) == 2  )
			{
				TESTCollision->DestroyHitObj( &m_AttackID );
				m_lpObj->SetParam(0, 0);
			}

			//	通常モードへ
			if( m_lpObj->GetFrame() >= 160 )
			{
				ChangeState( PMODE_NORMAL );
			}
			else
			{
				//	次のコンボへ
				if( m_lpObj->GetFrame() >= 132  )
				{
					if( KEY(KEY_A) == 3 )
					{				
						int num = rand()%255;
						if( num <= 200 )m_sound->Play( SE_COMBO3 );
						else m_sound->Play( SE_COMBO3_2 );

						m_lpObj->SetParam(0, 0);
						TESTCollision->DestroyHitObj( &m_AttackID );
						
						SetMotion( PMOTION_COMBO3 );
						m_step++;
					}
					m_Move = Vector3(0,0,0);		//	滑り止め
				}	
			}

			break;
		case 2:			//	３撃目
			YMove();	//	Ｙ軸移動有効

			//	座標移動
			m_Move.x = sinf( m_angle ) * 0.1f;
			m_Move.z = cosf( m_angle ) * 0.1f;

			if( m_lpObj->GetParam(1) == 1 )
			{
				m_Move.y = 0.5f;
				m_lpObj->SetParam(1, 0);
			}

			if( m_lpObj->GetParam(1) == 2 )
			{
				m_lpObj->SetParam(1, 0);
			}

			//	当たり判定
			if( m_lpObj->GetParam( 0 ) == 1 )
			{
				m_AttackID = TESTCollision->CreateCollision( new HitObjOBB( MASTERKIND_PLAYER, ATKKIND_PANCH, 6 ) );
				m_lpObj->SetParam(0, 0);
			}

			//	判定更新
			mat = GetBoneMatrix( 34 ) * GetMatrix();
			TESTCollision->UpdateBox( m_AttackID, &mat, &Vector3( 0.5f,0.5f,1.5f ), NULL, true );

			//	判定消去
			if( m_lpObj->GetParam( 0 ) == 2  )
			{
				TESTCollision->DestroyHitObj( &m_AttackID );
				m_lpObj->SetParam(0, 0);
			}

			//	停止
			if( m_lpObj->GetFrame() >= 193 )
			{
				m_Move = Vector3(0,0,0);		//	滑り止め
			}

			//	通常モードへ
			if( m_lpObj->GetFrame() >= 256 )
			{
				ChangeState( PMODE_NORMAL );
			}
			
			break;
	}
}

static float theta;

//----------------------------------------------------------
//	引数：なし
//	機能：巨人戦用2D移動処理
//----------------------------------------------------------
void c_Player::Move2D()
{
	Vector3 BossPos( 0,0,0 );	//	ボスの想定座標

	static bool bLeft  = false;
	static bool bRight = false;

	YMove();	//	Ｙ軸移動有効

	//	高所から落下で落下モードへ
	float height = collision->FloorHeight( m_Pos );
	height = m_Pos.y - height;
	if( m_gravity <= GRAVITY_MAX || height >= PLAYER_DEATH_HEIGHT )
	{
		ChangeState( PMODE_FALL );
	}

	//******************************//
	//	移動方向決定				//
	//******************************//

	//	ボスからプレイヤーへのベクトル
	Vector3 vector = m_Pos - BossPos;
	float dist = vector.Length();
	vector.Normalize();		//	正規化
	theta = atan2f( vector.x , vector.z );

	//	プレイヤーからボスへのベクトル
	Vector3 vec = BossPos - m_Pos;
	vec.Normalize();							//	正規化
	//	ボスへのアングル
	float angle = atan2f( vec.x , vec.z );

	m_angle = angle + ( EULER * 90.0f );		//	ほぼ直角方向にセット

	//	初期設定
	Vector3 front(matView._13,0,matView._33);
	Vector3 right(matView._11,0,matView._31);

	//	正規化
	front.Normalize();
	right.Normalize();

	//	軸の検出
	float axisX =   KEY_GetAxisX() * PLAYER_STICK_BIAS;


	//******************//
	//	距離別処理		//
	//******************//
	switch( RangeStep )
	{
		case P_STEP_R_SHORT:	//	近

			YMove();	//	Ｙ軸移動有効

			if( !m_bMoved )		//	移動完了フラグOFF
			{
				if( axisX > 0 )						//	右入力
				{
					m_sound->Play( SE_JUMP );		//	ジャンプSEを流用
					SetMotion( PMOTION_STEP_R );	//	右ステップ
					bRight = true;					//	右移動フラグON
					m_bMoved = true;				//	移動中!
					idRange = PLAYER_RANGE_CENTER;	//	理想を中央へ
					RangeStep = P_STEP_R_CENTER;	//	次のステップへ
				}
				else if( axisX < 0 )				//	左入力
				{
					m_sound->Play( SE_JUMP );		//	ジャンプSEを流用
					bLeft = true;					//	左移動フラグON
					m_bMoved = true;				//	移動中!
					idRange = PLAYER_RANGE_SHORT;	//	理想を近距離へ
					RangeStep = P_STEP_R_SHORT;		//	次のステップへ
				}
				else SetMotion( PMOTION_RAPID );	//	走りモーション
			}
			break;
		case P_STEP_R_CENTER:	//	中

			YMove();	//	Ｙ軸移動有効

			if( !m_bMoved )		//	移動完了フラグOFF
			{
				if( axisX > 0 )						//	右入力
				{
					m_sound->Play( SE_JUMP );		//	ジャンプSEを流用
					SetMotion( PMOTION_STEP_R );	//	右ステップ
					bRight = true;					//	右移動フラグON
					m_bMoved = true;				//	移動中!
					idRange = PLAYER_RANGE_LONG;	//	理想を遠距離へ
					RangeStep = P_STEP_R_LONG;		//	次のステップへ
				}
				else if( axisX < 0 )				//	左入力
				{
					m_sound->Play( SE_JUMP );		//	ジャンプSEを流用
					SetMotion( PMOTION_STEP_L );	//	左ステップ
					bLeft = true;					//	左移動フラグON
					m_bMoved = true;				//	移動中!
					idRange = PLAYER_RANGE_SHORT;	//	理想を近距離へ
					RangeStep = P_STEP_R_SHORT;		//	次のステップへ
				}
				else SetMotion( PMOTION_RAPID );	//	走りモーション
			}
			break;
		case P_STEP_R_LONG:		//	遠

			YMove();	//	Ｙ軸移動有効

			if( !m_bMoved )		//	移動完了フラグOFF
			{
				if( axisX > 0 )						//	右入力
				{
					m_sound->Play( SE_JUMP );		//	ジャンプSEを流用
					bRight = true;					//	右移動フラグON
					m_bMoved = true;				//	移動中!
					idRange = PLAYER_RANGE_LONG;	//	理想を遠距離へ
					RangeStep = P_STEP_R_LONG;		//	次のステップへ
				}
				else if( axisX < 0 )				//	左入力
				{
					m_sound->Play( SE_JUMP );		//	ジャンプSEを流用
					SetMotion( PMOTION_STEP_L );	//	左ステップ
					bLeft = true;					//	左移動フラグON
					m_bMoved = true;				//	移動中!
					idRange = PLAYER_RANGE_CENTER;	//	理想を中央へ
					RangeStep = P_STEP_R_CENTER;	//	次のステップへ
				}
				else SetMotion( PMOTION_RAPID );	//	走りモーション
			}
			break;
	}

	//	右移動開始
	if( bRight && !bLeft )
	{
		//	理想に到達したら移動フラグOFF
		if( CurrentRange >= idRange )
		{
			SideMove = PLAYER_SIDE_MOVE;	//	横移動量初期化
			bRight = false;					//	右移動フラグOFF
			m_bMoved = false;				//	移動可能に
			m_Pos.x = vector.x * idRange;	//	理想位置で止める
			m_Pos.z = vector.z * idRange;	//	理想位置で止める
			CurrentRange = idRange;			//	理想位置で止める
		}
		else
		{
			SideMove += PLAYER_SIDE_MOVE_INC;
			if( SideMove >= PLAYER_SIDE_MOVE_MAX )SideMove = PLAYER_SIDE_MOVE_MAX;
			CurrentRange += SideMove * SideMove;
		}
	}
	//	左移動開始
	if( bLeft && !bRight )
	{
		//	理想に到達したら移動フラグOFF
		if( CurrentRange <= idRange )
		{
			SideMove = PLAYER_SIDE_MOVE;	//	横移動量初期化
			bLeft = false;					//	右移動フラグOFF
			m_bMoved = false;				//	移動可能に
			m_Pos.x = vector.x * idRange;	//	理想位置で止める
			m_Pos.z = vector.z * idRange;	//	理想位置で止める
			CurrentRange = idRange;			//	理想位置で止める
		}
		else
		{
			SideMove += PLAYER_SIDE_MOVE_INC;
			if( SideMove >= PLAYER_SIDE_MOVE_MAX )SideMove = PLAYER_SIDE_MOVE_MAX;
			CurrentRange -= SideMove * SideMove;
		}
	}

	//	サウンド再生
	if( m_lpObj->GetParam(2) == 1 )
	{	
		m_sound->Play( SE_RUN );
		m_lpObj->SetParam(2,0);
	}

	//	カメラ・行動モード切り替え
	if( m_Pos.y >= 393.0f )			//	頂上に到達
	{
		m_step++;					//	タイマー
		SetMotion( PMOTION_WAIT );
		
		if( m_step == 60 )
		{
			m_Pos = Vector3( -269,393,-103 );
			SoundManager::GetInstance()->Destroy(glBgmNum);
			glBgmNum = SoundManager::GetInstance()->Play("DATA\\BGM\\BGM_3.wav",255,true);
			CAMERA.SetMode( CMODE_GIANT_FIGHT );
			ChangeState( PMODE_NORMAL );
		}
	}
	//	入力時行動
	else
	{
		if( m_Pos.y < 360.0f )
		{
			if( !m_bMoved )				//	移動していない時
			{
				if( KEY(KEY_B) == 3 )	//	Bボタン押
				{
					SetMotion( PMOTION_JUMP );
					m_sound->Play( SE_JUMP );				//	ジャンプSE
					ChangeState( PMODE_2DJUMP );			//	２Ｄジャンプモードへ
				}
			}
		}

		theta -= PLAYER_MOVE_2D_EULER;	//	移動角度設定

		//	座標決定
		m_Pos.x = BossPos.x + sinf(theta) * CurrentRange;
		m_Pos.z = BossPos.z + cosf(theta) * CurrentRange;

	}
}

//----------------------------------------------------------
//	引数：なし
//	機能：巨人戦用2Dジャンプ処理
//----------------------------------------------------------
void c_Player::Jump2D()
{
	Vector3 BossPos( 0,0,0 );
	Vector3 vec;

	theta -= PLAYER_MOVE_2D_EULER;	//	移動角度設定

	/* 行動ステップ */
	switch( m_step )
	{
		case 0:		//	ジャンプ
			YMove();	//	Ｙ軸移動有効

			//	プレイヤーからボスへのベクトル
			vec = BossPos - m_Pos;
			vec.Normalize();						//	正規化
			//	ボスへのアングル
			m_angle = atan2f( vec.x , vec.z ) + PLAYER_RIGHT_ANGLE;	//	ほぼ直角方向にセット


			//	パラメータでＹ軸移動
			if( m_lpObj->GetParam(1) == 1 )
			{
				m_Move.y = PLAYER_JUMP2D_HEIGHT;
				//SetMotion( PMOTION_FALL );
				m_step++;
			}
			break;
		case 1:		//	ジャンプ中

			YMove();	//	Ｙ軸移動有効

			//	プレイヤーからボスへのベクトル
			vec = BossPos - m_Pos;
			vec.Normalize();						//	正規化
			//	ボスへのアングル
			m_angle = atan2f( vec.x , vec.z ) + PLAYER_RIGHT_ANGLE;	//	ほぼ直角方向にセット

			//	ジャンプ中移動
			//m_Move.x = PLAYER_SPEED_2D * sinf(m_angle);
			//m_Move.z = PLAYER_SPEED_2D * cosf(m_angle);
			m_Pos.x = BossPos.x + sinf(theta) * CurrentRange;
			m_Pos.z = BossPos.z + cosf(theta) * CurrentRange;

			//	高所から落下で落下モードへ
			if( m_gravity <= GRAVITY_MAX )
			{
				ChangeState( PMODE_FALL );
			}


			//	着地時処理
			if( GroundCheck() == true )
			{
				//SetMotion( PMOTION_LAND );
				m_step++;
			}
			break;
		case 2:		//	着地モーション中

			//m_Move = Vector3(0,0,0);		//	滑り止め

			//	座標ズレを修正
			m_Pos.x = BossPos.x + sinf(theta) * idRange;
			m_Pos.z = BossPos.z + cosf(theta) * idRange;

			//	プレイヤーからボスへのベクトル
			vec = BossPos - m_Pos;
			vec.Normalize();							//	正規化
			//	ボスへのアングル
			m_angle = atan2f( vec.x , vec.z ) + PLAYER_RIGHT_ANGLE;	//	ほぼ直角方向にセット


			if( m_lpObj->GetFrame() >= 399 ){
				ChangeState( PMODE_2DMOVE );	//	通常モードへ
			}
			break;
	}
}

//----------------------------------------------------------
//	引数：なし
//	機能：落下中処理
//----------------------------------------------------------
void c_Player::Fall()
{
	static bool judge = false;

	YMove();
	//	着地時処理
	if( GroundCheck() == true )				//	着地の場合
	{
		if( !judge )
		{
			m_sound->Play( SE_DEATH );		//	死亡SE
			judge = true;					//	死亡！
			ChangeState( PMODE_DEATH );		//	死亡モードへ
		}
		else
		{
			m_Move = Vector3(0,0,0);		//	滑り止め
			CAMERA.SetMode( CMODE_NORMAL );	//	通常カメラモードへ
			SetMotion( PMOTION_DOWN );	//	倒れモーション
		}
	}
	else									//	空中の場合
	{
		InAirMove();						//	空中移動
		SetMotion( PMOTION_FALL );					//	落下モーション
	}
}

//----------------------------------------------------------
//	引数：なし
//	機能：ダメージ処理
//----------------------------------------------------------
void c_Player::Damage()
{
	static int counter = 0;
	static float speed = 1.5f;	//	飛んでいくスピード
	static float inc   = 0.0f;	//	スピード増加量

	/* 飛距離調節 */
	inc -= 0.002f;
	speed += inc;
	if( speed <= 0.0f )
	{
		inc = -1.0f;
		speed = 0.0f;
	}

	//	高所から落下で落下モードへ
	float height = collision->FloorHeight( m_Pos );
	height = m_Pos.y - height;
	if( m_gravity <= GRAVITY_MAX || height >= PLAYER_DEATH_HEIGHT )
	{
		ChangeState( PMODE_FALL );
	}
				
	/* 行動ステップ */
	switch( m_step )
	{
		case 0:
			YMove();	//	Ｙ軸移動有効

			speed = 1.5f;						//	スピードセット
			inc	  = 0.0f;						//	増加量セット
			SetMotion( PMOTION_DAMAGE );		//	やられモーション

			m_step++;
			break;
		case 1:		//	吹っ飛び
			YMove();	//	Ｙ軸移動有効

			//	地面に着地でモーションを進める
			if( GroundCheck() )
			{
				m_lpObj->SetFrame( 676 );
				m_step++;
			}
			else	//	空中
			{
				m_Move.x = sinf( m_angle + PI ) * speed;
				m_Move.z = cosf( m_angle + PI ) * speed;
				m_bMoved = true;		//	移動した！

			}

			break;
		case 2:		//	倒れ中

			YMove();	//	Ｙ軸移動有効

			m_Move.x = sinf( m_angle + PI ) * speed;
			m_Move.z = cosf( m_angle + PI ) * speed;
			m_bMoved = true;		//	移動した！


			counter++;	//	入力を待たせるカウンター

			if( counter >= 60 )
			{
				//	次のステップへ
				if( KEY(KEY_D) == 3 )
				{
					counter = 0;
					
					SetMotion( PMOTION_SITUP );
					m_step++;
				}
			}

			break;
		case 3:		//	起き上がり途中

			if( m_lpObj->GetFrame() >= 744 )
			{
				if( CAMERA.GetMode() == CMODE_GIANT_CHASE )ChangeState( PMODE_2DMOVE );			//	2D移動モードへ
				else if( CAMERA.GetMode() == CMODE_GIANT_2D )ChangeState( PMODE_2DMOVE );		//	2D移動モードへ
				else if( CAMERA.GetMode() == CMODE_GIANT_FIGHT )ChangeState( PMODE_NORMAL );	//	通常モードへ
				else ChangeState( PMODE_NORMAL );	//	通常モードへ
			}

			break;
	}
}

//----------------------------------------------------------
//	引数：なし
//	機能：死亡処理
//----------------------------------------------------------
void c_Player::Death()
{
	static float speed = 1.5f;	//	飛んでいくスピード
	static float inc   = 0.0f;	//	スピード増加量

	/* 飛距離調節 */
	inc -= 0.002f;
	speed += inc;
	if( speed <= 0.0f )
	{
		inc = -1.0f;
		speed = 0.0f;
	}
				
	/* 行動ステップ */
	switch( m_step )
	{
		case 0:
			YMove();	//	Ｙ軸移動有効

			m_sound->Play( SE_DEATH );		//	死亡SE

			speed = 1.5f;						//	スピードセット
			inc	  = 0.0f;						//	増加量セット
			SetMotion( PMOTION_DAMAGE );		//	やられモーション

			m_step++;
			break;
		case 1:		//	吹っ飛び
			YMove();	//	Ｙ軸移動有効

			//	地面に着地でモーションを進める
			if( GroundCheck() )
			{
				m_Move = Vector3(0,0,0);
				m_lpObj->SetFrame( 676 );
				m_step++;
			}
			else	//	空中
			{
				m_Move.x = sinf( m_angle + PI ) * speed;
				m_Move.z = cosf( m_angle + PI ) * speed;
			}

			break;
		case 2:		//	倒れ中

			YMove();	//	Ｙ軸移動有効

			static int ret = 60*2;

			ret--;

			if( ret <= 0 )
			{
				ret = 60*2;
				bOver = true;
			}

			

			break;
	}
}

//----------------------------------------------------------
//	引数：なし
//	機能：溜め攻撃処理
//----------------------------------------------------------
void c_Player::Power()
{
	m_Move = Vector3(0,0,0);		//	滑り止め

	D3DXMATRIX mat;

	/* 行動ステップ */
	switch( m_step )
	{
		case 0:			//	溜め中

			YMove();	//	Ｙ軸移動有効
			glEffectManager->SetPos(159753,m_Pos);
			//	次のコンボへ
			if( KEY(KEY_C) == 2 )	//	Cキー離
			{
				glEffectManager->DeleteEffect(159753);
				m_sound->Stop( SE_SEIKEN1 );
				m_sound->Play( SE_SEIKEN2 );
				SetMotion( PMOTION_POWER3 );
				m_step++;
			}

			break;
		case 1:			//	正拳突き

			YMove();	//	Ｙ軸移動有効

			m_Move.x = sinf( m_angle ) * PLAYER_SPEED_POWER;
			m_Move.z = cosf( m_angle ) * PLAYER_SPEED_POWER;

			//	当たり判定
			mat = GetBoneMatrix( 5 ) * GetMatrix();
			TESTCollision->UpdateBox( m_AttackID, &mat, &Vector3( 1.0f,1.0f,3.5f ), NULL, true );

			if( m_lpObj->GetParam( 0 ) == 1 )
			{
				m_AttackID = TESTCollision->CreateCollision( new HitObjOBB( MASTERKIND_PLAYER, ATKKIND_PANCH, 6 ) );
				m_lpObj->SetParam(0, 0);
				
				glEffectManager->SetEffect(EFFECTSETKIND_SEIKEN,Vector3(m_Pos.x,m_Pos.y+2.0f,m_Pos.z),m_angle,0.033f);
			}
			//	判定消去
			if( m_lpObj->GetParam( 0 ) == 2  )
			{
				TESTCollision->DestroyHitObj( &m_AttackID );
				m_lpObj->SetParam(0, 0);
			}

			//	停止
			if( m_lpObj->GetFrame() >= 309 )
			{
				m_Move = Vector3(0,0,0);		//	滑り止め
			}

			//	通常モードへ
			if( m_lpObj->GetFrame() >= 350 )
			{
				ChangeState( PMODE_NORMAL );
			}

			break;
	}
}

//----------------------------------------------------------
//	引数：なし
//	機能：屠怒滅の一撃処理
//----------------------------------------------------------
void c_Player::Hadoo()
{
	m_Move = Vector3(0,0,0);		//	滑り止め

	Vector3 BossPos( 0,0,0 );

	Vector3 vec = BossPos - m_Pos;
	vec.Normalize();
	float angle = atan2f( vec.x,vec.z );

	SetAngle( angle );

	static int counter = 0;			//	発射カウンター

	/* 行動ステップ */
	switch( m_step )
	{
		case 0:			//	溜め中

			YMove();	//	Ｙ軸移動有効

			counter++;

			//	次のコンボへ
			if( counter >= PLAYER_SP_STORE_TIME )
			{
				glEffectManager->DeleteEffect(123456);
				glEffectManager->SetEffect(EFFECTSETKIND_GODOTOU, Vector3(m_Pos.x,m_Pos.y+2.0f,m_Pos.z),m_angle,0.01f);
				counter = 0;
				m_sound->Stop( SE_HADOO1 );
				m_sound->Play( SE_HADOO2 );
				SetMotion( PMOTION_SPECIAL3 );
				m_step++;
			}

			break;
		case 1:			//	屠怒滅の一撃

			YMove();	//	Ｙ軸移動有効

			width -= 5;
			if( width <= 0 )width = 0;

			counter++;

			//	モーション送り
			if( counter >= PLAYER_SP_RELEASE_TIME )
			{
				counter = 0;
				m_lpObj->SetFrame( 811 );

				//	クリア条件
				hp   -= 500;
				m_bClear = true;

				m_step++;
			}

			break;

		case 2:			//	通常モードへ戻る

			YMove();	//	Ｙ軸移動有効

			//	通常モードへ
			if( m_lpObj->GetFrame() >= 843 )
			{
				ChangeState( PMODE_NORMAL );
			}

			break;
	}
}
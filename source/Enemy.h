#ifndef _ENEMY_H_
#define _ENEMY_H_


#include "BaseOBJ.h"

extern bool m_bClear;
extern int hp;
extern int width;

//-----------------------------------
//	定数
//-----------------------------------
#define OBJECT_SIZE_GIANT			1.0f					//	巨人サイズ
#define OBJECT_SIZE_WARRIOR			0.015f					//	中国武人サイズ
#define OBJECT_SIZE_NINJA			0.01f					//	忍者サイズ

//	モデルファイルパス
//#define FILE_PATH_GIANT				"DATA\\CHR\\bigknight\\OldKnight\\knight.IEM"	//	巨人(前ヴァージョン)
#define FILE_PATH_GIANT				"DATA\\CHR\\bigknight\\knight\\knight.IEM"		//	巨人
#define FILE_PATH_WARRIOR			"DATA\\CHR\\Y2008\\Y2008.IEM"					//	武人
#define FILE_PATH_NINJA				"DATA\\CHR\\Y2008\\Y2008.IEM"					//	忍者

//	敵タイプ
enum
{
	ENEMY_TYPE_NONE = -1,	//	初期値
	ENEMY_TYPE_GIANT,		//	巨人
	ENEMY_TYPE_WARRIOR,		//	武人
	ENEMY_TYPE_NINJA,		//	忍者
};

//****************************************
//
//	巨人用定数
//
//*****************************************

#define GIANT_BONE_WEAPON			33			//	巨人の武器ボーン

//	巨人行動モード
enum
{
	GMODE_NORMAL,			//	通常モード
	GMODE_TURN,				//	旋回モード
	GMODE_LOW,				//	下段攻撃モード
	GMODE_MIDDLE,			//	中段攻撃モード
	GMODE_HIGH,				//	上段攻撃モード
	GMODE_PRESS,			//	押しつぶしモード
	GMODE_TAKE_OFF,			//	剣置きモード
	GMODE_DAMAGE,			//	ダメージモード
	GMODE_DEATH,			//	死亡モード
};

//	巨人モーション
enum
{
	GMOTION_LINEAR,			//	補間
	GMOTION_WAIT_SWORD,		//	帯剣時の待機
	GMOTION_WAIT_FIGHT,		//	剣なしの待機
	GMOTION_ATTACK_LOW,		//	蹴り	
	GMOTION_ATTACK_MIDDLE,	//	横斬り
	GMOTION_ATTACK_HIGH,	//	殴り
	GMOTION_HAND_PRESS1,	//	手で潰す攻撃１
	GMOTION_HAND_PRESS2,	//	手で潰す攻撃２
	GMOTION_HAND_PRESS3,	//	手で潰す攻撃３
	GMOTION_DAMAGE,			//	怯み
	GMOTION_DEATH,			//	死亡
	GMOTION_TAKE_OFF_SWORD,	//	剣を置く
	GMOTION_EVENT,			//	イベント用( 構え〜待機 )
	GMOTION_TURN_L_S,		//	左旋回(帯剣時)
	GMOTION_TURN_R_S,		//	右旋回(帯剣時)
	GMOTION_TURN_L_N,		//	左旋回(非帯剣時)
	GMOTION_TURN_R_N,		//	右旋回(非帯剣時)
};

//*****************************************************************************************************************************
//
//		エネミー基本クラス
//
//*****************************************************************************************************************************
class c_Enemy : public BaseOBJ
{
protected:
	int hit_result;		//	判定結果
private:
	
public:
	c_Enemy( char* name );							//	引数付きコンストラクタ(name:ファイル名)

	virtual void Update( Vector3& ppos ) = 0;		//	更新(ppos:プレイヤー座標)
	virtual void Render() = 0;						//	描画
	virtual void Render2D() = 0;					//	描画
};

//*****************************************************************************************************************************
//
//		巨人クラス
//
//*****************************************************************************************************************************
class c_Giant : public c_Enemy
{
private:
	iex2DObj* EnvTex;				//	環境マップテクスチャ
	iex3DObj* m_lpHair;				//	ファサファサメッシュ
	iex3DObj* m_lpWeapon;			//	武器メッシュ
	bool m_bSword;					//	帯剣フラグ
	Vector3 m_SwordPos;				//	武器座標
	int count;
	iex2DObj* m_Clear;
	iex2DObj* m_Clear2;
public:
	c_Giant( char* name );			//	引数付きコンストラクタ(name:ファイル名)
	~c_Giant();						//	デストラクタ

	void Update( Vector3& ppos );	//	更新(ppos:プレイヤー座標)
	void Render();					//	描画
	void Render2D();				//	2D描画

	void SetMotion( int n );		//	モーション設定
	void ChangeState( int state );	//	モード切り替え

	//	行動モード
	void Wait();					//	待機
	void Turn( Vector3& ppos );		//	旋回(ppos:プレイヤー座標)
	void Attack_Low();				//	下段攻撃
	void Attack_Middle();			//	中段攻撃
	void Attack_High();				//	上段攻撃
	void Attack_Press();			//	押しつぶし攻撃
	void TakeOff();					//	剣を置く
	void Damage();					//	被ダメージ
	void Death();					//	死亡

	//	セッター
	void SetSwordFlag( bool f ){ m_bSword = f; }	//	帯剣フラグ
	//	ゲッター
	bool GetSwordFlag(){ return m_bSword; }			//	帯剣フラグ
};

//*****************************************************************************************************************************
//
//		武人クラス
//
//*****************************************************************************************************************************
class c_Warrior : public c_Enemy
{
private:

public:
	c_Warrior( char* name );		//	引数付きコンストラクタ(name:ファイル名)
	~c_Warrior();					//	デストラクタ

	void Update( Vector3& ppos );	//	更新(ppos:プレイヤー座標)
	void Render();					//	描画
};

//*****************************************************************************************************************************
//
//		忍者クラス
//
//*****************************************************************************************************************************
class c_Ninja : public c_Enemy
{
private:

public:
	c_Ninja( char* name );			//	引数付きコンストラクタ(name:ファイル名)
	~c_Ninja();						//	デストラクタ

	void Update( Vector3& ppos );	//	更新(ppos:プレイヤー座標)
	void Render();					//	描画
};

//*****************************************************************************************************************************
//
//		エネミー管理クラス
//
//*****************************************************************************************************************************
class c_EnemyManager
{
private:
	int m_EnemyType;				//	敵のタイプ(-1:初期値,0:巨人,1:武人,2:忍者)
	c_Enemy *m_obj;					//	敵基本クラスの実体
public:
	c_EnemyManager();				//	コンストラクタ
	~c_EnemyManager();				//	デストラクタ

	void Update( Vector3& ppos );	//	更新(ppos:プレイヤー座標)
	void Render();					//	描画
	void Render2D();					//	描画
};

#endif	//	_ENEMY_H_
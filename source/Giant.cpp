#include	"iextreme.h"
#include	"system/system.h"
#include	"Enemy.h"
#include	"TEST\\TESTCollision.h"


//*****************************************************************************************************************************
//
//
//
//*****************************************************************************************************************************



//*****************************************************************************************************************************
//
//		巨人クラス
//
//*****************************************************************************************************************************

bool m_bClear;
int interval;
bool clear;
int hp;

//*****************************************************************************************************************************
//
//
//
//*****************************************************************************************************************************
//--------------------------------------------
//	コンストラクタ
//--------------------------------------------
c_Giant::c_Giant(char *name) : c_Enemy(name)
{
	m_Pos = Vector3( 0, -8, 0 );
	m_angle = 0.0f;
	m_scale = OBJECT_SIZE_GIANT;
	
	m_state = GMODE_NORMAL;
	m_step	= 0;

	m_HP	= 500;		//	巨人の体力
	hp		= 500;
	m_AttackID = -1;
	m_DamageID = -1;
	hit_result = -1;


	//	環境マップ設定
	EnvTex = new iex2DObj( "DATA\\Env.png" );
	shader->SetValue("EnvMap", EnvTex);
	shader->SetValue("EnvParam", 0.2f);

	//	武器読み込み
	m_lpWeapon = new iex3DObj( "DATA\\CHR\\bigknight\\sword\\sword.IEM"  );

	m_bSword = true;	//	最初は帯剣
	m_SwordPos = Vector3( 0,0,0 );

	//	羽毛メッシュ読み込み
	m_lpHair = new iex3DObj( "DATA\\CHR\\bigknight\\hair\\hair.IEM" );

	count = 60*1;
	interval = 5*1;
	clear = false;

	m_Clear = new iex2DObj( "DATA\\Clear.png" );
	m_Clear2 = new iex2DObj( "DATA\\Clear_Screen.png" );
	m_bClear = false;

}

//--------------------------------------------
//	デストラクタ
//--------------------------------------------
c_Giant::~c_Giant()
{
	DEL( m_lpObj );
	DEL( EnvTex );
	DEL( m_lpWeapon );
	DEL( m_lpHair );
	DEL( m_Clear );
	DEL( m_Clear2 );
}

//*****************************************************************************************************************************
//
//
//
//*****************************************************************************************************************************


//*****************************************************************************************************************************
//
//		主処理
//
//*****************************************************************************************************************************
void c_Giant::Update( Vector3& ppos )
{
	m_Move = Vector3(0,0,0);		//	滑り止め

	//	行動モード
	switch( m_state )
	{
		case GMODE_NORMAL:	//	通常モード
			Wait();
			break;
		case GMODE_TURN:	//	旋回モード
			Turn( ppos );
			break;
		case GMODE_LOW:		//	下段攻撃モード
			Attack_Low();
			break;
		case GMODE_MIDDLE:	//	中段攻撃モード
			Attack_Middle();
			break;
		case GMODE_HIGH:	//	上段攻撃モード
			Attack_High();
			break;
		case GMODE_PRESS:	//	押しつぶしモード	
			Attack_Press();
			break;
		case GMODE_TAKE_OFF://	剣置きモード
			TakeOff();
			break;
		case GMODE_DAMAGE:	//	ダメージモード
			Damage();
			break;
		case GMODE_DEATH:	//	死亡モード
			Death();
			break;
	}

	//	武器をセット
	if( GetSwordFlag() )WeaponSet( m_lpWeapon, GIANT_BONE_WEAPON );
	else
	{
		m_lpWeapon->SetPos( m_SwordPos );
		m_lpWeapon->SetAngle( PI/2, 0, 0 );
		m_lpWeapon->SetScale( OBJECT_SIZE_GIANT);
		m_lpWeapon->Update();
	}

	BaseOBJ::Update();

	//	羽毛メッシュ更新
	m_lpHair->SetPos( m_Pos );
	m_lpHair->SetAngle( m_angle );
	m_lpHair->SetScale( OBJECT_SIZE_GIANT );
	m_lpHair->Animation();
	m_lpHair->Update();

	//	帯剣時
	if( GetSwordFlag() )
	{
		if( ppos.y >= 393.0f )	//	プレイヤーが最上階
		{
			ChangeState( GMODE_TAKE_OFF );
		}
	}

	if( hp > 0 )
	{
		/* やられ判定 */
		D3DXMATRIX mat = GetBoneMatrix( 12 ) * GetMatrix();	
		TESTCollision->UpdateBox( m_DamageID, &mat , &Vector3( 50.0f,50.0f,70.0f ), NULL, false );
		TESTCollision->HitCheck();
		DamageHitResult result = TESTCollision->GetHitCheckResult( MASTERKIND_ENEMY );
		hit_result = result.GetAtkCollisionAtkKind();

		//	やられ処理
		if( hit_result == ATKKIND_PANCH )			//	プレイヤーの攻撃を受けた場合
		{
			TESTCollision->DestroyHitObj( &m_DamageID );
			width += 110*3;
			if( width >= 320 )width = 320;
			ChangeState( GMODE_DAMAGE );
			
		}
		else if( hit_result == ATKKIND_SPECIAL )
		{
			m_HP -= 500;
			m_bClear = true;
		}
	}
	else	//	死亡！
	{
		interval--;
		if( interval <= 0 )
		{
			ChangeState( GMODE_DEATH );
		}
		
	}

	static int AllOver = 60*3;

	if( clear )
	{
		AllOver--;

		if( AllOver <= 0 )
		{
			AllOver = 60*3;
			PostQuitMessage(0);
		}
	}

#ifdef _DEBUG

	if( KEY( KEY_B7 ) == 3 )	//	Qキー押
	{
		ChangeState( GMODE_TURN );
	}
	if( KEY( KEY_B8 ) == 3 )	//	Wキー押
	{
		ChangeState( GMODE_PRESS );
	}
	if( KEY( KEY_L ) == 3 )		//	Aキー押
	{
		PostQuitMessage(0);
	}
	if( KEY( KEY_R ) == 3 )		//	Sキー押
	{
		ChangeState( GMODE_TAKE_OFF );
	}
	if( KEY( KEY_ENTER ) == 3 )	//	ENTERキー押
	{
		ChangeState( GMODE_HIGH );
	}

#endif	//	_DEBUG

	/* 自動攻撃 */
	if( ppos.y >= 20.0f && ppos.y <= 21.0f )	//	下段攻撃
	{
		if( m_state == GMODE_NORMAL )ChangeState( GMODE_TURN );
	}
	if( ppos.y >= 120.0f && ppos.y <= 121.0f )	//	中段攻撃
	{
		m_Pos = Vector3( 0, 0, 0 );
		if( m_state == GMODE_NORMAL )ChangeState( GMODE_TURN );
	}

	

	//TESTCollision->DestroyHitObj( &m_DamageID );
	



}

//*****************************************************************************************************************************
//
//		描画関連
//
//*****************************************************************************************************************************
void c_Giant::Render()
{
	//	固定シェーダー描画
	//if( m_lpObj )m_lpObj->Render();
	//if( m_lpHair )m_lpHair->Render();
	//if( m_lpWeapon )m_lpWeapon->Render();

	//	指定シェーダー描画
	if( m_lpObj )m_lpObj->Render( shader, "copy_fx2" );
	if( m_lpHair )m_lpHair->Render( shader, "copy_fx2" );
	if( m_lpWeapon )m_lpWeapon->Render( shader, "copy_fx2" );


#ifdef _DEBUG
	//char	str[64];
	//wsprintf( str, "HIT %d\n", hit_result );
	//IEX_DrawText( str, 1000,40,200,20, 0xFF00FF00 );
#endif
}

void c_Giant::Render2D()
{
	if( interval <= 0 )m_Clear->Render( 0,0,1280,720,0,0,1280,740,shader, "copy" );
	if( clear )m_Clear2->Render( 0,0,1280,720,0,0,1280,720,shader, "copy" );
}

//*****************************************************************************************************************************
//
//		処理
//
//*****************************************************************************************************************************

//--------------------------------------------------------------------------------
//	引数: モーション番号
//	機能: モーションを更新する
//--------------------------------------------------------------------------------
void c_Giant::SetMotion( int n )
{
	if( m_lpObj->GetMotion() != n )
	{
		m_lpObj->SetMotion( n );
	}

	if( m_lpHair->GetMotion() != n )
	{
		m_lpHair->SetMotion( n );
	}
}

//--------------------------------------------------------------------------------
//	引数: 行動モード
//	機能: 行動モードを切り替える
//--------------------------------------------------------------------------------
void c_Giant::ChangeState( int state )
{
	BaseOBJ::ChangeState( state );	//	行動モード切り替え
}

//----------------------------------------------------------
//	引数：なし
//	機能：巨人の通常行動
//----------------------------------------------------------
void c_Giant::Wait()
{
	count--;

	if( !GetSwordFlag() )
	{
		if( count <= 0 )
		{
			count = 60*1;
			ChangeState( GMODE_TURN );
		}
		SetMotion( GMOTION_WAIT_FIGHT );		//	非帯剣時待機モーション
	}
	else
	{
		SetMotion( GMOTION_WAIT_SWORD );						//	帯剣時待機モーション
	}
}

//----------------------------------------------------------
//	引数：なし
//	機能：プレイヤー方向へ旋回させる
//----------------------------------------------------------
void c_Giant::Turn( Vector3& ppos )
{
	//	プレイヤーからボスへの方向
	float	vecX = ( m_Pos.x - ppos.x );
	float	vecZ = ( m_Pos.z - ppos.z );
	float	angle = atan2f( vecX,vecZ );

	float	x1,z1;
	if( ppos.y < 393 )	//	プレイヤーが登っている最中は先回り
	{
		//	プレイヤーへの方向( EULER*角度　角度を大きくするほど近くなる )
		x1 = ( ( ppos.x + sinf(angle + EULER*80 )*320.0f ) - m_Pos.x );
		z1 = ( ( ppos.z + cosf(angle + EULER*80 )*320.0f ) - m_Pos.z );
	}
	else	//	登りきったらプレイヤー方向
	{
		//	プレイヤーへの方向
		x1 = ( ppos.x  - m_Pos.x );
		z1 = ( ppos.z  - m_Pos.z );
	}

	//	ボスのアングル
	float	x2 = sinf( m_angle );
	float	z2 = cosf( m_angle );

	//	2次元外積算出
	float cross = x1*z2 - x2*z1;

	//　内積による角度補正
	float	dot = x1*x2 + z1*z2;
	float	len = sqrtf( x1*x1 + z1*z1 );
	dot/= len ;
	dot = 1 - dot;

	//	内積の値を限界値で止める
	if( dot >= 0.05f ) dot = 0.05f;

	//	指定角度内で次のステップ
	if( abs(dot) < 0.01f ) 
	{	
		if( ppos.y <= 70.0f )ChangeState( GMODE_LOW );	//	下段攻撃モードへ
		else if( ppos.y >= 150.0f && ppos.y <= 251.0f )ChangeState( GMODE_MIDDLE );	//	中段攻撃モードへ
		else
		{
			int rnd = rand()%100+1;
			if( rnd < 30 )ChangeState( GMODE_HIGH );
			else ChangeState( GMODE_PRESS );
			
		}
	}

	//	外積による左右判定
	if( cross < 0.0f )
	{
		//m_angle -= dot/8;
		m_angle -= (EULER*0.3f);
		//	モーション切り替え
		if( m_bSword )SetMotion( GMOTION_TURN_L_S );
		else SetMotion( GMOTION_TURN_L_N );
		
	}
	else
	{
		//m_angle += dot/8;
		m_angle += (EULER*0.3f);
		//	モーション切り替え
		if( m_bSword )SetMotion( GMOTION_TURN_R_S );
		else SetMotion( GMOTION_TURN_R_N );
	}

}

//----------------------------------------------------------
//	引数：なし
//	機能：プレイヤーへ下段攻撃させる
//----------------------------------------------------------
void c_Giant::Attack_Low()
{
	D3DXMATRIX mat;

	switch( m_step )
	{
		case 0:
			SetMotion( GMOTION_ATTACK_LOW );
			m_AttackID = TESTCollision->CreateCollision( new HitObjOBB( MASTERKIND_ENEMY, ATKKIND_PANCH, 180 ) );
			m_step++;
		case 1:

			//	当たり判定
			mat = GetBoneMatrix( GIANT_BONE_WEAPON ) * GetMatrix();	//	武器ボーン
			TESTCollision->UpdateBox( m_AttackID, &mat, &Vector3( 25.0f,10.0f,400.0f ), NULL, true );

			//	判定消去
			if( m_lpObj->GetParam( 1 ) == 2  )
			{
				TESTCollision->DestroyHitObj( &m_AttackID );
				m_lpObj->SetParam(1, 0);
			}	

			//	通常モードへ
			if( m_lpObj->GetFrame() >= 838 )
			{
				ChangeState( GMODE_NORMAL );
			}
			break;
	}
	
}

//----------------------------------------------------------
//	引数：なし
//	機能：プレイヤーへ中段攻撃させる
//----------------------------------------------------------
void c_Giant::Attack_Middle()
{
	D3DXMATRIX mat;

	if( GetSwordFlag() )	//	帯剣時
	{
		switch( m_step )
		{
			case 0:
				SetMotion( GMOTION_ATTACK_MIDDLE );
				m_AttackID = TESTCollision->CreateCollision( new HitObjOBB( MASTERKIND_ENEMY, ATKKIND_PANCH, 120 ) );
				m_step++;
				break;
			case 1:

				//	当たり判定
				mat = GetBoneMatrix( GIANT_BONE_WEAPON ) * GetMatrix();	//	武器ボーン
				TESTCollision->UpdateBox( m_AttackID, &mat, &Vector3( 10.0f,30.0f,500.0f ), NULL, true );

				//	判定消去
				if( m_lpObj->GetParam( 1 ) == 2  )
				{
					TESTCollision->DestroyHitObj( &m_AttackID );
					m_lpObj->SetParam(1, 0);
				}

				//	通常モードへ
				if( m_lpObj->GetFrame() >= 2459 )
				{
					ChangeState( GMODE_NORMAL );
				}
				break;
		}
	}
}

//----------------------------------------------------------
//	引数：なし
//	機能：プレイヤーへ上段攻撃させる
//----------------------------------------------------------
void c_Giant::Attack_High()
{
	D3DXMATRIX mat;

	if( !GetSwordFlag() )	//	帯剣していない時
	{
		switch( m_step )
		{
			case 0:
				SetMotion( GMOTION_ATTACK_HIGH );
				m_step++;
				break;
			case 1:

				//	当たり判定
				if( m_lpObj->GetParam( 1 ) == 1 )
				{
					m_AttackID = TESTCollision->CreateCollision( new HitObjOBB( MASTERKIND_ENEMY, ATKKIND_PANCH, 6 ) );
					m_lpObj->SetParam(1, 0);
				}
				
				//	判定更新
				mat = GetBoneMatrix( 32 ) * GetMatrix();
				TESTCollision->UpdateBox( m_AttackID, &mat, &Vector3( 150.0f,80.0f,80.0f ), NULL, true );

				//	判定消去
				if( m_lpObj->GetParam( 1 ) == 2  )
				{
					TESTCollision->DestroyHitObj( &m_AttackID );
					m_lpObj->SetParam(0, 0);
				}	

				//	通常モードへ
				if( m_lpObj->GetFrame() >= 3120 )
				{
					ChangeState( GMODE_NORMAL );
				}
				break;
		}
	}
}

//----------------------------------------------------------
//	引数：なし
//	機能：プレイヤーに押しつぶし攻撃
//----------------------------------------------------------
void c_Giant::Attack_Press()
{
	D3DXMATRIX mat;

	static int counter = 60*5;

	if( !GetSwordFlag() )	//	帯剣していない時
	{
		switch( m_step )
		{
			case 0:
				counter = 60*5;
				SetMotion( GMOTION_HAND_PRESS1 );
				m_step++;
				break;
			case 1:
				//	当たり判定
				if( m_lpObj->GetParam( 1 ) == 1 )
				{
					m_DamageID = TESTCollision->CreateCollision( new HitObjOBB( MASTERKIND_ENEMY, DAMAGECOLLISION ) );
					m_AttackID = TESTCollision->CreateCollision( new HitObjOBB( MASTERKIND_ENEMY, ATKKIND_PANCH, 6 ) );
					m_lpObj->SetParam(1, 0);
				}
				
				//	判定更新
				mat = GetBoneMatrix( 12 ) * GetMatrix();
				TESTCollision->UpdateBox( m_AttackID, &mat, &Vector3( 10.0f,30.0f,30.0f ), NULL, true );

				//	判定消去
				if( m_lpObj->GetParam( 1 ) == 2  )
				{
					TESTCollision->DestroyHitObj( &m_DamageID );
					TESTCollision->DestroyHitObj( &m_AttackID );
					m_lpObj->SetParam(1, 0);
				}

				//	次の攻撃へ
				if( m_lpObj->GetFrame() >= 3572 )
				{
					counter--;
					if( counter <= 0 )
					{
						TESTCollision->DestroyHitObj( &m_DamageID );
						TESTCollision->DestroyHitObj( &m_AttackID );
						counter = 0;
						SetMotion( GMOTION_HAND_PRESS3 );
						m_step++;
					}
				}

				break;
			case 2:

				//	当たり判定
				if( m_lpObj->GetParam( 1 ) == 1 )
				{
					m_AttackID = TESTCollision->CreateCollision( new HitObjOBB( MASTERKIND_ENEMY, ATKKIND_PANCH, 6 ) );
					m_lpObj->SetParam(1, 0);
				}
				
				//	判定更新
				mat = GetBoneMatrix( 32 ) * GetMatrix();
				TESTCollision->UpdateBox( m_AttackID, &mat, &Vector3( 150.0f,80.0f,80.0f ), NULL, true );

				//	判定消去
				if( m_lpObj->GetParam( 1 ) == 2  )
				{
					TESTCollision->DestroyHitObj( &m_AttackID );
					m_lpObj->SetParam(1, 0);
				}

				//	通常モードへ
				if( m_lpObj->GetFrame() >= 4533 )
				{
					TESTCollision->DestroyHitObj( &m_AttackID );
					ChangeState( GMODE_NORMAL );
				}


				break;
		}
	}	
}

//----------------------------------------------------------
//	引数：なし
//	機能：剣を床に刺して置かせる
//----------------------------------------------------------
void c_Giant::TakeOff()
{
	if( GetSwordFlag() )	//	帯剣時
	{
		switch( m_step )
		{
			case 0:
				SetMotion( GMOTION_TAKE_OFF_SWORD );
				m_step++;
				break;
			case 1:
				if( m_lpObj->GetParam(2) == 1 )
				{
					SetSwordFlag( false );		//	剣を床に置いた
					m_SwordPos = GetBonePos( GIANT_BONE_WEAPON, 50 );
					m_lpObj->SetParam( 2, 0 );
				}
				break;
		}
	}
	else
	{
		//	通常モードへ
		if( m_lpObj->GetFrame() >= 5906 )
		{	
			
			ChangeState( GMODE_TURN );
		}
	}
}

//----------------------------------------------------------
//	引数：なし
//	機能：ダメージ処理
//----------------------------------------------------------
void c_Giant::Damage()
{
	switch( m_step )
	{
		case 0:
			SetMotion( GMOTION_DAMAGE );
			m_step++;
			break;
		case 1:
			//	通常モードへ
			if( m_lpObj->GetFrame() >= 4894 )
			{	
				
				ChangeState( GMODE_NORMAL );
			}
			break;
	}
}

//----------------------------------------------------------
//	引数：なし
//	機能：死亡処理
//----------------------------------------------------------
void c_Giant::Death()
{
	static int end = 60*3;

	switch( m_step )
	{
		case 0:
			SetMotion( GMOTION_DEATH );
			m_step++;
			break;
		case 1:
			if( m_lpObj->GetFrame() >= 5585 )
			{	
				end--;
				if( end <= 0 )clear = true;
			}

			break;
	}
}

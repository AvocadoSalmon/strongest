#ifndef _SCENE_TITLE_H_
#define _SCENE_TITLE_H_

#include	"Camera.h"
#include	"Player.h"
#include	"Enemy.h"
#include	"Collision.h"
#include	"Fade.h"
#include	"TEST\\EffectManager.h"

//*****************************************************************************************************************************
//
//	メインモードクラス
//
//*****************************************************************************************************************************

class	sceneTitle : public Scene
{
private:
	c_Player *m_player;			//	プレイヤークラス
	c_Fade *fade;				//	フェードIOクラス
	bool m_bchange;				//	シーン移行フラグ
	bool m_bshowTitle;			
	int m_timer;
	LPIEXMESH	lpPortal;
	LPIEXMESH	lpStage;
	EffectManager* m_effectManager;
	IEX2DOBJ*	m_titleRogo;

	int			m_bgm;
public:
	sceneTitle();				//	コンストラクタ
	~sceneTitle();				//	デストラクタ

	bool Initialize();			//	初期化
	void Update();				//	更新
	void Render();				//	描画

};

#endif	//	_SCENE_TITLE_H_


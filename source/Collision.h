#ifndef _COLLISION_H_
#define _COLLISION_H_

//-------------------------------------
//	定数定義
//-------------------------------------
#define COLLISION_FLOOR_BIAS		Vector3( 0, 3 ,0 )	//	床判定の調整値
#define COLLISION_FLOOR_RAY_DIST	300.0f				//	床判定のレイ距離
#define COLLISION_ERROR_DIST		10000.0f			//	壁判定エラー値
#define COLLISION_MODEL_SIZE		1.7f				//	コリジョンモデルサイズ

//-----------------------------------
//	当たり判定クラス
//-----------------------------------
class c_Collision
{
private:
	iexMesh*	m_CollisionModel;						//	コリジョンメッシュ
public:
	c_Collision();										//	コンストラクタ
	~c_Collision();										//	デストラクタ

	void	Load( char* name );							//	コリジョンメッシュのロード(name:ファイル名)

	float FloorHeight( Vector3& pos );					//	引数の座標から床への高さを返す(pos:キャラ座標)
	float Wall( Vector3& pos, Vector3& v );				//	壁とのキョリを返す(pos:キャラ座標,v:キャラの向いている方向)
	Vector3 CheckMove( Vector3& pos, Vector3& move );	//	最終的な移動結果座標を返す(pos:キャラ座標,move:キャラの移動量)

	void	Render();

	////	位置チェック
	//Vector3 CheckPos( BaseOBJ* actorA, BaseOBJ* actorB );
	//Vector3 CheckPos( Vector3& PosA, float size, BaseOBJ* actorB );
};

extern	c_Collision* collision;							//	c_Collisionクラスのextern宣言


#endif	//	_COLLISION_H_